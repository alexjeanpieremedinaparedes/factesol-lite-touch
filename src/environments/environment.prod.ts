export const environment = {
  production: true,
  urlApi: 'https://factedsol.innovated.xyz/',
  urlApiFile: 'https://innovated.xyz/seeted/api/',
  urlImageInvalid : 'assets/img/default/fotouser.svg',
  urllogoInvalid : 'assets/img/default/logopordefault.jpg',
  urlApiRecursos : 'https://innovated-recursos.innovated.xyz/api/',
  urlApiRecMasivos  : 'https://factedsol-recursos.innovated.xyz/api/'
  // urlApiRecMasivos  : 'http://factedsol-recursos.innovated.xyz/api/',
};
