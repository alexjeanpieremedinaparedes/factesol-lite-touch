// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlApi: 'https://factedsol.innovated.xyz/',
  urlApiFile: 'https://innovated.xyz/seeted/api/',
  urlImageInvalid : 'assets/img/default/fotouser.svg',
  urllogoInvalid : 'assets/img/default/logopordefault.jpg',
  urlApiRecursos : 'https://innovated-recursos.innovated.xyz/api/',
  urlApiRecMasivos  : 'https://factedsol-recursos.innovated.xyz/api/'
  // urlApiRecMasivos  : 'http://factedsol-recursos.innovated.xyz/api/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
