import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, map } from "rxjs/operators";
import { UpdatedatadowloandfilesService } from './services/updatedatadowloandfiles.service';
import { IProcesoMasivo } from './models/proccessMassive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  panelOpenState = false;
  title = 'Fact';
  IProcess = new IProcesoMasivo();

  constructor( 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private sProcess: UpdatedatadowloandfilesService ) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let child = this.activatedRoute.firstChild;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
          } else if (child.snapshot.data && child.snapshot.data['title']) {
            return child.snapshot.data['title'];
          } else {
            return null;
          }
        }
        return null;
      })
    ).subscribe((data: any) => {
      if (data) {
        this.titleService.setTitle(data);
      }
    });
  }

  ngOnInit(): void {
    this.sProcess.proccess$.subscribe(process => this.IProcess = process)
  }

  @HostListener('window:beforeunload', ['$event'])
  doSomething($event) {

    const validate = !this.IProcess.completed
    if(validate){
      $event.returnValue='Your data will be lost!';
    }
    
  }

}
