import { Injectable } from '@angular/core';
import *  as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
const EXCEL_EXT = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  constructor() { }

  exportToExcel(json: any, excelFileName: string): void {

    const workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workBook: XLSX.WorkBook = { Sheets: { 'data': workSheet }, SheetNames: ['data'] };
    const excelbuffer: any = XLSX.write(workBook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcel(excelbuffer, excelFileName);

  }

  private saveAsExcel(buffer: any, FileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, FileName + EXCEL_EXT);
  }

  fecha_actual(): string {
    let fecha: string;
    const dat = new Date();
    const dia = dat.getDate();
    const mes = dat.getMonth() + 1;
    const año = dat.getFullYear();

    if (mes < 10) fecha = `${año}-0${mes}-${dia}`;
    else fecha = `${año}-${mes}-${dia}`;
    return fecha;
  }

  convert_fecha(fecha: string) {

    const dat = new Date(fecha);
    const dia = dat.getDate();
    const mes = dat.getMonth() + 1;
    const año = dat.getFullYear();

    if (mes < 10) fecha = `${año}-0${mes}-${dia}`;
    else fecha = `${año}-${mes}-${dia}`
    return fecha;
  }

  base64toBlob(base64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
      var begin = sliceIndex * sliceSize;
      var end = Math.min(begin + sliceSize, bytesLength);

      var bytes = new Array(end - begin);
      for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
        bytes[i] = byteCharacters[offset].charCodeAt(0);
      }
      byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  downloadFile(blob, nameArchivo) {
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob);
    }
    const linkElement = document.createElement('a');
    const url = window.URL.createObjectURL(blob);
    linkElement.setAttribute('href', url);
    linkElement.setAttribute('download', nameArchivo);
    const clickEvent = new MouseEvent('click', {
      'view': window,
      'bubbles': true,
      'cancelable': false
    });
    linkElement.dispatchEvent(clickEvent);
  }

  printDocument( bs64File : any ){

    let byteCharacters = atob(bs64File);
    let byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    let byteArray = new Uint8Array(byteNumbers);
    let file = new Blob([byteArray], {type: 'application/pdf;base64'});
    let fileURL = URL.createObjectURL(file)
    const iframe = document.createElement('iframe');
    iframe.style.display = 'none';
    iframe.src = fileURL;
    document.body.appendChild(iframe);
    iframe.contentWindow.print();

  }

  KeyCabecera(arreglo: []): any {

    let object = { columns: [], displayedColumns: [] };
    const columns = arreglo;
    const keys = Object.keys(columns);

    keys.forEach((el, i) => {

      object.columns.push({ titulo: el });
      object.displayedColumns.push(el);

    });

    return object;

  }

  _search(arreglo: any[], text_busqueda, texto: string): any[] {

    if (!texto) return arreglo;
    return arreglo.filter(user => String(user[text_busqueda]).toUpperCase().includes(texto.toUpperCase()));
  }

  _headersApi(): any {
    const token = JSON.parse(localStorage.getItem('_token_login')).token;
    return { 'Authorization': `bearer ${token}` }
  }

  _convertbs64ForSrc(bs64img): string {
    return 'data:image/jpeg;base64,' + bs64img;
  }

  _convertbs64ForPdf(bs64File): string {
    return 'data:application/pdf;base64,' + bs64File;
  }

  capitalice(string): string {
    return string.charAt(0).toUpperCase() + string.slice(1).toLocaleLowerCase();
  }

  startTime() {

    let today = new Date();
    let hr = today.getHours();
    let min = today.getMinutes();
    let sec = today.getSeconds();
    const ap = (hr < 12) ? "<span>AM</span>" : "<span>PM</span>";
    hr = (hr == 0) ? 12 : hr;
    hr = (hr > 12) ? hr - 12 : hr;
    //Add a zero in front of numbers<10
    hr = this.checkTime(hr);
    min = this.checkTime(min);
    sec = this.checkTime(sec);

    const clock = document.getElementById("clock");
    if( clock !== null ){

      clock.innerHTML = hr + " : " + min + " : " + sec + " " + ap;
      setTimeout( ()=> {
        this.startTime()
      }, 500);

    }

  }

  checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  _RoundDecimal(numero : number, digitos : number ) : string {

    const base = Math.pow(10, digitos);
    const entero = Math.round(numero * base);
    const valor = (entero / base).toFixed( digitos );
    return valor;
  }

  onFocus( focus:string ){
    document.getElementById(focus).focus();
  }

  SoloNumeros( $event ){

    const KEY_ALPHABET = [ 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90 ];
    const KEY_ARITMETICA = [ 189, 109, 107 ];
    const KEYS = KEY_ALPHABET.concat( KEY_ARITMETICA );

    if( KEYS.includes($event.keyCode) ){
      return false;
    }

    return true;
  }

  _convertToNameMes( cod : string ) : string {

    const meses = [
      { codigo: '01', mes: 'Enero' },
      { codigo: '02', mes: 'Febrero' },
      { codigo: '03', mes: 'Marzo' },
      { codigo: '04', mes: 'Abril' },
      { codigo: '05', mes: 'Mayo' },
      { codigo: '06', mes: 'Junio' },
      { codigo: '07', mes: 'Julio' },
      { codigo: '08', mes: 'Agosto' },
      { codigo: '09', mes: 'Septiembre' },
      { codigo: '10', mes: 'Octubre' },
      { codigo: '11', mes: 'Noviembre' },
      { codigo: '12', mes: 'Diciembre' }
    ]

    return meses.find(x=>x.codigo === cod ).mes;
  }

  _timerestant( time : number ) : any {

    let hrs = ~~(time / 3600);
    let mins = ~~((time % 3600) / 60);
    let secs = ~~time % 60;
    let days = Math.floor(secs / (3600*24));

    const msjdays = days > 0 ? `${days} dias`    :'';
    const msjhrs  = hrs  > 0 ? `${hrs} horas`    :'';
    const msjmins = mins > 0 ? `${mins} minutos` :'';
    const msjsecs = secs > 0 ? `${secs} segundos`:'';

    const ret = `${msjdays} ${msjhrs} ${msjmins} ${msjsecs}`.trim().replace('dias', 'dias,').replace('horas', 'horas,').replace('minutos', 'minutos y ').trim();
    return ret;

  }



}
