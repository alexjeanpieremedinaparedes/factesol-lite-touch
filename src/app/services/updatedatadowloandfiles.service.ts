import { EventEmitter, Injectable } from '@angular/core';
import { IProcesoMasivo } from '../models/proccessMassive';

@Injectable({
  providedIn: 'root'
})
export class UpdatedatadowloandfilesService {

  proccess$ = new EventEmitter<IProcesoMasivo>()
  constructor() { }
}
