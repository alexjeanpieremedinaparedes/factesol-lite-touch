import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FunctionsService } from './functions.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  
  private url : string ;
  constructor( private http:HttpClient, private sfunction : FunctionsService ) {
    this.url = 'api/cliente/';
  }

  listClient(): Observable<any>{

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}clientelistar`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  listDocumento(): Observable<any>{
    
    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}clientelistartipodocumento`, { headers }).pipe( map( (result : any ) => result.result ) )
  }

  filter_ClientDoc( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}clientelistarnumerodocumento`, body, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  add_removeEmail( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}clientecorreogestion`, body, { headers } )

  }

  SaveClient( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}clienteagregar`, body, { headers })

  }

  deleteClient( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}clienteeliminar`, body, { headers } );

  }

}
