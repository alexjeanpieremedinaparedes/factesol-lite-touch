import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FunctionsService } from './functions.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  private url : string ;
  constructor( private http:HttpClient, private sfunction : FunctionsService ) {
    this.url = 'api/empresa/';
  }

  listSucursal(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}sucursallistar`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  listTalonario(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}talonariolistar`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  SaveSucursal( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}sucursalagregar`, body, { headers } )
  }

  SaveTalonario( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}talonarioagregar`, body, { headers })

  }

  TalonarioAsignarSucursal( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}talonarioasignarsucursal`, body, { headers })

  }

  TalonarioAsignarUsuario( body: any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}talonarioasignarusuario`, body, { headers })

  }

  DeleteTalonario( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}talonarioeliminar`, body, { headers })

  }

  DeleteSucursal( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}sucursaleliminar`, body, { headers })
  }
  
  changeLogo( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}cambiarlogo`, body, { headers })

  }

  listPermisoUsuario( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}listarpermisosusuario`, body, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  AgregarPermisoUsuario( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}agregarpermisosusuario`, body, { headers })

  }

  AgregarConfiguracion( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}configuracionweb`, body, { headers })

  }

  listConfiguracion(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}configuracionweblistar`, { headers }).pipe( map( (result : any ) => result.result ) )
  }

  listPlantillas(){
    const url = 'https://innovated-recursos.innovated.xyz/vista/pdf/listarplantilla';
    return this.http.get(url);
  }

}
