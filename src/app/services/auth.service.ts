import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { mdUser, mdUseradd } from '../models/user.model';
import { FunctionsService } from './functions.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private url:string;
  constructor( private http:HttpClient, private route:Router,private sfunction : FunctionsService ) {
    this.url = 'api/usuario/';
  }

  login( body:mdUser ) {
    return this.http.post(`${environment.urlApi}${this.url}iniciosesion`, body );
  }

  logout(){
    localStorage.removeItem('_token_login');
    this.route.navigateByUrl('/inicio')
  }

  changePassword( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}cambiarpassword`, body, { headers });

  }

  listUsers(){
    
    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}usuariolistar`, { headers }).pipe( map( (result : any) => result.result ) );

  }

  saveUser( body: mdUseradd ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}usuarioagregar`, body, { headers } )
    
  }

  deleteUser( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}usuarioeliminar`, body, { headers } )

  }

}
