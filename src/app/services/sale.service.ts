import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { typeNota } from '../interfaces/typeNota.interface';
import { FunctionsService } from './functions.service';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  private url : string ;
  private urlPrintSale : string;
  private urlPrintSaleReimprimir
  private headers : any;

  constructor( private http:HttpClient, private sfunction : FunctionsService ) {
    this.url = 'api/venta/';
    this.urlPrintSale = 'pdf/generarpdfcomprobante';
    this.urlPrintSaleReimprimir = 'pdf/generarpdf';
    this.headers = { 'Authorization' : 'INNOVATED' }

  }

  ValidarCPE( body:any )
  {
    return this.http.post( `${environment.urlApiFile}ValidarCpe`, body )
  }

  list_saleAccountant( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}listarventacontable`, body, {headers});

  }

  list_saleAdmin( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}listarventaadmin`, body, { headers })

  }

  listTipoComprobante(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}tipocomprobantelistar`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  listTipopago(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}tipopagolistar`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  listMoneda(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}monedalistar`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  listMedioPago(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}mediopagolistar`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  _vObtnerCliente( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}obtenercliente`, body, { headers })

  }

  listTipocambio ( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}tipocambiolistar`, body, { headers })

  }

  SaveTipoCambio ( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}tipocambioagregar`, body, { headers })

  }

  SaveSale ( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}ventaagregar`, body, { headers })

  }

  ObtenerVenta ( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}ventaobtener`, body, { headers }).pipe( map( (result : any ) => result.result) )

  }

  listTipocambioSunat ( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}tipocambiosunat`, body, { headers })

  }


  obtenerTalonarioUsuario( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}obtenertalonariousuario`, body, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  pdf( body:any ){
    return this.http.post( `${environment.urlApiFile}pdf`, body)
  }

  xml( body:any ){
    return this.http.post(`${environment.urlApiFile}ListarXmlCdr?tipoArchivo=xml`, body)
  }

  cdr( body:any ){
    return this.http.post(`${environment.urlApiFile}ListarXmlCdr?tipoArchivo=cdr`, body);
  }

  Constancia( body:any ){
    return this.http.post(`${environment.urlApiFile}ListarXmlCdr?tipoArchivo=constancia`, body);
  }

  listDetailscpe( body:any ){
    return this.http.post(`${environment.urlApiFile}cpefilterManual`, body).pipe( map( (result : any ) => result.result[0] ) )
  }

  send_sunat( body:any ){
    return this.http.post( `${environment.urlApiFile}ProcesoCompleto`, body )
  }

  send_email( body:any ){
    return this.http.post(`${environment.urlApiFile}EnviarCorreo`, body);
  }

  _printSale( body:any ){

    return this.http.post( `${environment.urlApiRecursos}${this.urlPrintSale}`, body, { headers : this.headers } )
  }

  ReimprimirSale( body:any ){

    return this.http.post( `${environment.urlApiRecursos}${this.urlPrintSaleReimprimir}`, body, { headers : this.headers } )
  }

  AnularCpe( body : any ){
    return this.http.post( `${environment.urlApiFile}anular`, body, { headers : this.headers } )
  }

  GenerateFileMassive( body: any ){
    return this.http.post(`${environment.urlApiRecMasivos}comprobante/generararchivos`, body )
  }

  list_saledetails( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}ventaobtenerdetalle`, body, { headers })

  }
  
  getTypeNotaCredito(){
    return this.http.get<typeNota[]>('assets/json/Notacredito.json')
  }

  getTypeNotaDebito(){
    return this.http.get<typeNota[]>('assets/json/Notadebito.json')
  }

  GenerateFileRAR( body: any ){
    return this.http.post(`${environment.urlApiRecMasivos}comprobante/generarrar`, body )
  }

}
