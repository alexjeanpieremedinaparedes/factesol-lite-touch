import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FunctionsService } from './functions.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private url : string ;
  constructor( private http:HttpClient, private sfunction : FunctionsService ) {
    this.url = 'api/producto/';
  }

  listProduct(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}listarproducto`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  listPresentation(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}listarpresentacion`, { headers }).pipe( map( (result : any ) => result.result ) )

  }

  SaveProduct( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}productoagregar`, body, { headers })

  }

  deleteProduct( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}productoeliminar`, body, { headers })

  }

  listUnidadMedida(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}unidadmedidalistar`, { headers }).pipe( map( (result : any ) => result.result ) );

  }

  SaveUnidadMedida( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}unidadmedidaagregar`, body, { headers })

  }

  lisCategoria(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}categorialistar`, { headers }).pipe( map( (result : any ) => result.result ) );

  }

  SaveCategoria( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}categoriaagregar`, body, { headers })

  }

  listSubCategoria(){

    const headers = this.sfunction._headersApi();
    return this.http.get(`${environment.urlApi}${this.url}subcategorialistar`, { headers }).pipe( map( (result : any ) => result.result ) );

  }

  SaveSubCategoria( body:any ){

    const headers = this.sfunction._headersApi();
    return this.http.post(`${environment.urlApi}${this.url}subcategoriaagregar`, body, { headers })

  }

}