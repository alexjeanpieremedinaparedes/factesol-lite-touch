import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//rutas de configuraciones
import { configuracionesRoutingModule } from './pages/configuraciones/configuraciones.routing';
//rutas de dasboard
import { dashboardRoutingModule } from './pages/dashboard/dashboard.routing';
//rutas de inicio
import { inicioRoutingModule } from './pages/inicio/inicio.routing';

import { NopagesfoundComponent } from './nopagesfound/nopagesfound.component';

const routes: Routes = [

  {path: '', redirectTo: '/inicio' , pathMatch: 'full'},
  {path: '**' , component:NopagesfoundComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,{ useHash: true }),
    configuracionesRoutingModule,
    dashboardRoutingModule,
    inicioRoutingModule

  ]
  ,
  exports: [RouterModule]
})
export class AppRoutingModule { }
