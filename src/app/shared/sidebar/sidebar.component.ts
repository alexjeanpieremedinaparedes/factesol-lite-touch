
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IPermissions } from '../../interfaces/permissions';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  
  error : boolean;
  message : string;

  permiso = {} as IPermissions;
  step = 0;
  setStep(index: number) {
    this.step = index;
  }

  // sidebar
  tab: any = 'tab0';
  tab1: any
  tab2: any
  tab3: any
  Clicked: boolean

  acordion: boolean = true;
  accordionsidebar() {
    this.acordion = !this.acordion;
  }

  onClick(check) {
    //    console.log(check);
    if (check == 0){
      this.tab = 'tab0';
    }else if (check == 1) {
      this.tab = 'tab1';
    } else if (check == 2) {
      this.tab = 'tab2';
    } else {
      this.tab = 'tab3';
    }

  }

  onSwitch(check) {

    switch (check) {
      case 0:
        return 'tab0';
      case 1:
        return 'tab1';
      case 2:
        return 'tab2';
      case 3:
        return 'tab3';
    }

  }


  openTab0: boolean = false;
  openTab1: boolean = false;
  openTab2: boolean = false;
  openTab3: boolean = false;
  openTab4: boolean = false;
  openTab5: boolean = false;
  openTab6: boolean = false;
  openTab7: boolean = false;
  openTab8: boolean = false;

  changetabs() {
    this.openTab0 = false;
    this.openTab1 = false;
    this.openTab2 = false;
    this.openTab3 = false;
    this.openTab4 = false;
    this.openTab5 = false;
    this.openTab6 = false;
    this.openTab7 = false;
    this.openTab8 = false;

  }

  toggleTabs($tabNumber: number) {
    this.changetabs();
    if ($tabNumber === 0) this.openTab0 = true;
    else if ($tabNumber === 1) this.openTab1 = true;
    else if ($tabNumber == 2) this.openTab2 = true;
    else if ($tabNumber == 3) this.openTab3 = true;
    else if ($tabNumber == 4) this.openTab4 = true;
    else if ($tabNumber == 5) this.openTab5 = true;
    else if ($tabNumber == 6) this.openTab6 = true;
    else if ($tabNumber == 7) this.openTab7 = true;
    else if ($tabNumber == 8) this.openTab8 = true;

  }

  constructor(private router: Router) {
    const ruta = this.router.url;
    if (ruta.includes('bienvenido')) this.openTab0 = true;
    if (ruta.includes('ventarapida')) this.openTab1 = true;
    else if (ruta.includes('ventanormal')) this.openTab2 = true;
    else if (ruta.includes('notacredito')) this.openTab3 = true;
    else if (ruta.includes('notadebito')) this.openTab4 = true;
    else if (ruta.includes('productoservicios')) this.openTab5 = true;
    else if (ruta.includes('clientes')) this.openTab6 = true;


    else if (ruta.includes('reportesadministrativos')) this.openTab7 = true;
    else if (ruta.includes('reportecontable')) this.openTab8 = true;

    this._aplicarPermisos();
  }

  ngOnInit(): void { }

  _aplicarPermisos() {

    this.error = false;
    this.message = null;
    const auth = localStorage.getItem('_token_login');

    if (auth) {

      const permissions = JSON.parse(auth).permissions;

      if( permissions === undefined || permissions === null ){
        this.error = true;
        this.message = 'Falta agregar permisos';
      }
      else{

        const permisos = JSON.parse(permissions);
  
        this.permiso.ventarapida = false;
        this.permiso.ventanormal = false;
        this.permiso.notacredito = false;
        this.permiso.notadebito = false;
        this.permiso.productoservicio = false;
        this.permiso.cliente = false;
        this.permiso.repadministrativo = false;
        this.permiso.repcontable = false;
  
        const keys = Object.keys(this.permiso);
        keys.forEach(el => {
  
          permisos.filter(x => x.CodigoWeb === el).map(response => {
            this.permiso[el] = response.Seleccionar;
  
          })
  
        });

        this.permiso.operacion = this.permiso.ventarapida || this.permiso.ventanormal || this.permiso.notacredito || this.permiso.notadebito;
        this.permiso.mantenimiento = this.permiso.productoservicio || this.permiso.cliente;
        this.permiso.reportes = this.permiso.repadministrativo || this.permiso.repcontable;
        
      }

    }
  }
}
