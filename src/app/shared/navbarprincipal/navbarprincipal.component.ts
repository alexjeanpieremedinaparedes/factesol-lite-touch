
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FunctionsService } from '../../services/functions.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbarprincipal',
  templateUrl: './navbarprincipal.component.html'
})
export class NavbarprincipalComponent implements OnInit {
  data = {} as Data;

  tab1: any;
  tab2: any;
  
  constructor( public auth:AuthService,
              sfunction : FunctionsService,
              private router: Router ) {

    const ruta = this.router.url;
    if (ruta.includes('ventanormal')) this.openTab1 = true;
    else if (ruta.includes('ventanormaltouch')) this.openTab2 = true;

    const _logindata = localStorage.getItem('_token_login');
    if( _logindata ){

      const user  = JSON.parse(_logindata);
      this.data.user = user.user;
      this.data.bs64logo = sfunction._convertbs64ForSrc(user.logo);
      this.data.name = user.Nombre;
      this.data.lastname = user.Apellidos;
      this.data.empresa = user.empresa;
      this.data.ruc = user.ruc;
    }
  }

  onClick(check) {
    //    console.log(check);
    if (check == 1){
      this.tab1 = 'tab1';
    }else if (check == 2) {
      this.tab2 = 'tab2';
    } 
  }

  onSwitch(check) {
    switch (check) {
      case 1:
        return 'tab0';
      case 2:
        return 'tab1';
    }
  }

  changetabs() {
    this.openTab1 = false;
    this.openTab2 = false;
  }


  openTab1: boolean = false;
  openTab2: boolean = false;

  toggleTabs($tabNumber: number) {
    this.changetabs();
    if ($tabNumber === 1) this.openTab1 = true;
    else if ($tabNumber === 2) this.openTab1 = true;
   
  }

  ngOnInit(): void {}

}

export interface Data{
  bs64logo  : string;
  user      : string;
  name      : string;
  lastname  : string;
  empresa   : string;
  ruc       : string;
}
