import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarprincipalComponent } from './navbarprincipal/navbarprincipal.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DemoMaterialModule } from '../material-module';
import { LoadingComponent } from './loading/loading.component';
import { SidebarconfigurationComponent } from './sidebarconfiguration/sidebarconfiguration.component';
import { TableDynamicComponent } from './table-dynamic/table-dynamic.component';
import { NotImageDirective } from '../directive/not-image.directive';
import { ComponentsModule } from '../components/components.module';
import { VentaNotasComponent } from './venta-notas/venta-notas.component';
import { PipesModule } from '../pipes/pipes.module';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';


@NgModule({
  declarations: [
    NavbarprincipalComponent,
    SidebarComponent,
    FooterComponent,
    LoadingComponent,
    SidebarconfigurationComponent,
    TableDynamicComponent,
    NotImageDirective,
    VentaNotasComponent,
  ],
  exports: [
    NavbarprincipalComponent,
    SidebarComponent,
    LoadingComponent,
    FooterComponent,
    SidebarconfigurationComponent,
    TableDynamicComponent,
    VentaNotasComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    DemoMaterialModule,
    CommonModule,
    ComponentsModule,
    PipesModule,
    TrimValueAccessorModule

  ]
})
export class SharedModule { }
