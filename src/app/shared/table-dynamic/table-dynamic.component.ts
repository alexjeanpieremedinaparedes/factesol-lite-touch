import { TemplatePortal } from '@angular/cdk/portal';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { fromEvent, Subscription } from 'rxjs';
import { take, filter } from 'rxjs/operators';
import { Component, Input, OnInit, ViewChild, TemplateRef, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";

@Component({
  selector: 'app-table-dynamic',
  templateUrl: './table-dynamic.component.html',
  styleUrls: ['./table-dynamic.component.css']
})
export class TableDynamicComponent implements OnInit {

  data:any;
  selectedRow:any;

  @Input() dataTheDB : boolean = true;
  @Input() img:string = null;
  @Input() columns = [];
  @Input() displayedColumns  : string[] = [];
  @Input() dataSource = new MatTableDataSource([]);
  @Input() verSeleccionado:boolean = false;
  @Input() clickDerecho:boolean = false;
  @Input() clickDerecho2:boolean = false;
  @Input() txtClicDerecho2 : string = null;
  @Input() txtClicDerecho: string = null;

  @Output() sendRow = new EventEmitter<void>();
  @Output() sendOperation = new EventEmitter<{ titulo : string, id : number }>();
  @Output() sendSegundaopcion  = new EventEmitter<any>();
  @Output() sendAgregar = new EventEmitter<number>();
  @Output() lista:any = new EventEmitter<any>();

  @ViewChild(MatSort) sort: MatSort;

  sub: Subscription;
  @ViewChild('userMenu') userMenu: TemplateRef<any>;
  @ViewChild( MatTable ) table : MatTable<any>;

  overlayRef: OverlayRef | null;

  constructor(  public overlay          : Overlay,
                public viewContainerRef : ViewContainerRef ) { }

  ngOnInit(): void {}

  open( { x, y }: MouseEvent, row ) {

    this.selectedRow = row;
    if( !this.clickDerecho ) return;

    this.close();
    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo({ x, y })
      .withPositions([
        {
          originX: 'end',
          originY: 'bottom',
          overlayX: 'end',
          overlayY: 'top',
        }
      ]);

    this.overlayRef = this.overlay.create({
      positionStrategy,
      scrollStrategy: this.overlay.scrollStrategies.close()
    });

    this.overlayRef.attach(new TemplatePortal(this.userMenu, this.viewContainerRef, {
      $implicit: row
    }));

    this.sub = fromEvent<MouseEvent>(document, 'click')
      .pipe(
        filter(event => {
          const clickTarget = event.target as HTMLElement;
          return !!this.overlayRef && !this.overlayRef.overlayElement.contains(clickTarget);
        }),
        take(1)
      ).subscribe(() => this.close())

      this.showDetails( row );
  }

  close() {
    this.sub && this.sub.unsubscribe();
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }

  sortData( $event ){
    this.dataSource.sort = this.sort;
  }

  sendDetails(){
    this.sendRow.emit( this.data )
  }

  _sendSegundaopcion(){
    this.sendSegundaopcion.emit( this.data );
  }

  showDetails( $event ){
    this.data = $event;
  }

  enventOperation( titulo:string, id:number ){
    this.table.renderRows();
    this.sendOperation.emit( { titulo : titulo, id : id } );
  }

  _EventAgregar( $event ){
    this.sendAgregar.emit( $event );
  }


  MarcarDescargar($event)
  {
    this.dataSource.filteredData.filter((e)=> { e.Marcar = $event.checked});
    const a = this.dataSource.filteredData.filter((e)=> { if(e.Marcar) return e;});
    this.lista.emit(a);
  }

  Prueba($event)
  {
    const a = this.dataSource.filteredData.filter((e)=> { if(e.Marcar) return e;});
    this.lista.emit(a);
  }

}
