import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html'
})
export class LoadingComponent implements OnInit {

  constructor(public  spinner:NgxSpinnerService) { }

  ngOnInit(): void {
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
  }
}
