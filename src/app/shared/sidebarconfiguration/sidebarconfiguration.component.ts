import { Component, DebugElement, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IPermissions } from '../../interfaces/permissions';

@Component({
  selector: 'app-sidebarconfiguration',
  templateUrl: './sidebarconfiguration.component.html'
})
export class SidebarconfigurationComponent implements OnInit {

  error : boolean;
  message : string;

  permiso = {} as IPermissions;

  openTab1: boolean = false;
  openTab2: boolean = false;
  openTab3: boolean = false;
  openTab4: boolean = false;
  openTab5: boolean = false;
  openTab6: boolean = false;

  changetabs() {
    this.openTab1 = false;
    this.openTab2 = false;
    this.openTab3 = false;
    this.openTab4 = false;
    this.openTab5 = false;
    this.openTab6 = false;

  }

  toggleTabs($tabNumber: number) {
    this.changetabs();
    if ($tabNumber === 1) this.openTab1 = true;
    else if ($tabNumber == 2) this.openTab2 = true;
    else if ($tabNumber == 3) this.openTab3 = true;
    else if ($tabNumber == 4) this.openTab4 = true;
    else if ($tabNumber == 5) this.openTab5 = true;
    else if ($tabNumber == 6) this.openTab6 = true;
  }

  constructor(private router: Router) {

    const ruta = this.router.url;
    if (ruta.includes('micuenta')) this.openTab1 = true;
    else if (ruta.includes('usuario')) this.openTab2 = true;
    else if (ruta.includes('correos')) this.openTab3 = true;
    else if (ruta.includes('impresion')) this.openTab4 = true;
    else if (ruta.includes('venta')) this.openTab5 = true;
    else if (ruta.includes('facturacionsunat')) this.openTab6 = true;

    this._aplicarPermisos();

  }

  ngOnInit(): void { }

  _aplicarPermisos(){

    this.error = false;
    this.message = null;
    const auth = localStorage.getItem('_token_login');

    if (auth){

      const permissions = JSON.parse(auth).permissions;

      if( permissions === undefined || permissions === null ){
        this.error = true;
        this.message = 'Falta agregar permisos';
      }
      else {

        const permisos = JSON.parse(permissions);
        this.permiso.usuario     = false;
        this.permiso.impresion   = false;
        this.permiso.venta       = false;
        this.permiso.facturacionsunat = false;
        
        const keys = Object.keys(this.permiso);
        keys.forEach( el =>{
    
          permisos.filter( x=>x.CodigoWeb === el ).map( response => {
    
            this.permiso[el] = response.Seleccionar;
    
          })
    
        });
      }

    }

  }

}
