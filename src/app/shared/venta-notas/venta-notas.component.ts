import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IComprobante } from 'src/app/interfaces/cpe.interface';
import { typeNota } from 'src/app/interfaces/typeNota.interface';
import { VercomprobanteComponent } from 'src/app/pages/dashboard/reportes/repadministrativo/vercomprobante/vercomprobante.component';
import { SaleService } from 'src/app/services/sale.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { FunctionsService } from '../../services/functions.service';

@Component({
  selector: 'app-venta-notas',
  templateUrl: './venta-notas.component.html',
  styleUrls: ['./venta-notas.component.css']
})
export class VentaNotasComponent implements OnInit {

  @Input() TipoNota : string;
  @Input() TypeNota : Observable<typeNota[]>;

  cpeGeneral = {} as IComprobante;
  cpe        = {} as IComprobante;
  nota       = {} as Nota;
  misDatos   = {} as MisDatos;

  openTab         : number = 1;
  bloqAll         : boolean = true;
  notdata         : boolean = true;
  success         : boolean;
  cnn_expi        : boolean;
  error           : boolean;
  message         : string;
  listcpe         : any[] = [];
  showModal       : boolean = false;
  ModalContinuar  : boolean = false;
  showDetailsVenta: boolean = true;
  dateStart       : any = new Date();

  listTalGeneral  : any[] = [];
  listTalonario   : any[] = [];
  
  form            : FormGroup;
  formSeccionOne  : FormGroup;
  configuration   : any[] = [];
  pipedecimal     : number = 0;
  typeCoin        : any = 'S/';
  TotalVenta      : number;
  mismaAfectacion : boolean;

  _token_login = JSON.parse(localStorage.getItem('_token_login'));

  constructor(  public dialog: MatDialog,
                private fb: FormBuilder,
                private svalidator : ValidatorsService,
                private ssale : SaleService,
                private spinner : NgxSpinnerService,
                public sfuntion : FunctionsService ) {

    this.getTalonarioUser();
    this._create_form();
    this._create_formSeccionOne();
    this.listTipocpe();
    this._init_amountDetails();

    
    this.configuration = JSON.parse(JSON.parse(localStorage.getItem('_token_login')).configuration);
    this.configuration.filter( x=>x.Operacion === 'formatoREDdetalle' ).map( data =>{
      const decimales = Number(data.valor);
      this.pipedecimal = decimales;
    });

  }

  ngOnInit(): void {}

  get serieInvalid(){
    return this.svalidator.control_invalid('serie', this.form);
  }

  get numeroInvalid(){
    return this.svalidator.control_invalid('numero', this.form);
  }

  get codigocomprobanteInvalid(){
    return this.svalidator.control_invalid('codigocomprobante', this.form);
  }

  _create_form(){
    this.form = this.fb.group({
      serie   : [ '', [ Validators.required, Validators.minLength(4), Validators.maxLength(4) ] ],
      numero  : [ '', [ Validators.required, Validators.min(0) ] ],
      codigocomprobante : [ '', [ Validators.required ] ]
    });
  }


  get invalidSerie(){
    return this.svalidator.control_invalid('serie', this.formSeccionOne);
  }

  get invalidNumber(){
    return this.svalidator.control_invalid('number', this.formSeccionOne);
  }

  get aliasSerieInvalid(){
    return this.svalidator.control_invalid('aliasSerie', this.formSeccionOne);
  }

  get tipoNotaInvalid(){
    return this.svalidator.control_invalid('tipoNota', this.formSeccionOne);
  }

  get docAfectadoInvalid(){
    return this.svalidator.control_invalid('docAfectado', this.formSeccionOne);
  }

  get observacionInvalid(){
    return this.svalidator.control_invalid('observacion', this.formSeccionOne);
  }

  _create_formSeccionOne(){
    this.formSeccionOne = this.fb.group({
      serie           : [ '', [ Validators.required, Validators.minLength(4), Validators.maxLength(4) ] ],
      number          : [ '', [ Validators.required ] ],
      aliasSerie      : [ '', [ Validators.required ] ],
      tipoNota        : [ '', [ Validators.required ] ],
      docAfectado     : [ '', [ Validators.required ] ],
      observacion     : [ '', [ Validators.required ] ],
      tipoimpresion   : [ '' ]

    });

  }
  

  toggleTabs($tabNumber: number){

    this.openTab = $tabNumber;
  } 

  
  listTipocpe(){

    this.initializeMessages();
    this.ssale.listTipoComprobante().subscribe( response =>{
      
      this.listcpe = response;
      this.listcpe = this.listcpe.filter( x=>x.NumeroSunat === '01' || x.NumeroSunat === '03' );
      this.spinner.hide();

    }, (error) => this._exeception( error ) );
  }

  getTalonarioUser(){

    const body = {
      idusuario : JSON.parse(localStorage.getItem('_token_login')).clave
    };

    this.ssale.obtenerTalonarioUsuario( body ).subscribe( response => {

      this.listTalGeneral = response;
      this.listTalGeneral = this.listTalGeneral.filter( x=>x.NumeroSunat === this.TipoNota );
      this.listTalonario = this.listTalGeneral;

      this.spinner.hide();
    }, ( error ) => this._exeception( error ) )

  }

  _sendData(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data(this.form);
    }

    this.initializeAll();
    const body = {
      ... this.form.value
    };

    this.ssale.ObtenerVenta( body ).subscribe( response =>{

      if( response.length > 0 ){

        const result = response[0];
        
        if( result.Anulada ){
          this.error = true;
          this.message = 'No se puede aplicar una nota de Crédito aun comprobante anulado';
        }
        else{

          this.cpeGeneral = result;
          this.notdata = Object.entries( this.cpeGeneral ).length === 0;
          this.TotalVenta = this.cpeGeneral.Total;
  
          this.misDatos.Moneda = result.Moneda;
          this.misDatos.TipoPago = result.TipoPago;
          this.misDatos.Descuento = result.Descuento;
          this.misDatos.Exonerada = result.Exonerada;
          this.misDatos.Afecta = result.Afecta;
          this.misDatos.Inafecta = result.Inafecta;
          this.misDatos.IGV = result.IGV;
          this.misDatos.Total = result.Total;
          this.misDatos.detalle = JSON.stringify(this.cpeGeneral.Detalle);
          this.typeCoin = result.MonedaSimbolo;

          let afectaciones = [];

          this.cpeGeneral.Detalle.forEach(element => {
            let exists = afectaciones.filter(s => s.includes(element.Afectacion));

            if(exists.length == 0)
            {
              afectaciones.push(element.Afectacion)
            }
          });

          this.mismaAfectacion = afectaciones.length == 1;
        }

        this.TypeNota = this.TypeNota.pipe( map( 
          result => result.filter( item =>{

            if(this.mismaAfectacion) return item;
            else{
              if(item.name !== 'Descuento Global') return item;
            }

          })  ) );

      }
      else {
        
        this.error = true;
        this.message = 'No se encontro el comprobante';
      }

      this.spinner.hide();
    }, (error) => this._exeception(error) );
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }


  initializeMessages(){
    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
    this.TotalVenta = 0;
  }

  initializeAll() {
    this.initializeMessages();
    this.notdata    = true;
    this.cpeGeneral = {} as IComprobante;
    this.cpe        = {} as IComprobante;
    this.nota       = {} as Nota;
    this.misDatos   = {} as MisDatos;
    this.openTab    = 1;
    this._init_amountDetails();
  }
  
  _vercpe(){

    this.error = false;
    this.message = null;
    
    if( this.cpeGeneral.Detalle === undefined || Object.entries( this.cpeGeneral.Detalle ).length === 0 ){
      this.error = true;
      this.message = 'no existen datos para mostrar';
      return;
    }

    const CodSunat = this.cpeGeneral.TipoComprobante === 'BOLETA' ? '03' : '01';
    this.dialog.open(VercomprobanteComponent, {
      data: {
        ruc : this._token_login.ruc,
        serie : this.cpeGeneral.Serie,
        numero : this.cpeGeneral.Numero,
        codigoComprobante : CodSunat
      }
    });

  }

  onChangeTipoNota( $event : string ){

    //Nota de credito
    switch( $event ){

      case 'Anulacion en la Operación':
        this.bloqAll = true;
        break;

      case 'Devolución por Item':
        this.bloqAll = false;
        break;

      case 'Descuento por Item':
        this.bloqAll = false;
        break;
      case 'Aumento en el Valor':
        this.bloqAll = false;
    }

    //Nota de debito
    const minimizar = ['Intereses por Mora', 'Penalidades', 'Descuento Global']
    this.showDetailsVenta = minimizar.includes($event) ? false : true;

    if(this.showDetailsVenta){

      const detalle = JSON.parse(this.misDatos.detalle);
      this.cpeGeneral.Detalle = detalle;
      this.cpe = this.cpeGeneral;
      this.typeCoin = this.cpe.MonedaSimbolo;
    }
    else{

      debugger
      
      const det = JSON.parse(this.misDatos.detalle);
      const afectacion = $event === "Descuento Global" ? det[0].Afectacion : "INAFECTO";
      let detalleS = JSON.parse('[{ "Afectacion" : "'+afectacion+'", "Descripcion" : "", "UnidadMedida" : "UNIDAD", "Cantidad" : 1, "Precio" : 0, "Importe" : 0  }]');
      this.cpeGeneral.Detalle = detalleS;
      this.cpe = this.cpeGeneral;
      // this.typeCoin = this.cpe.MonedaSimbolo;
    }

    const updateDescripcion = this.nota.tipoNota === 'Correccion por error en la descripcion';
    this.cpe.Afecta = updateDescripcion ? 0  : this.misDatos.Afecta;
    this.cpe.Gratuita = 0;
    this.cpe.Exonerada = updateDescripcion ? 0  : this.misDatos.Exonerada;
    this.cpe.Inafecta = updateDescripcion ? 0  : this.misDatos.Inafecta;
    this.cpe.IGV = updateDescripcion ? 0  : this.misDatos.IGV;
    this.cpe.Total = updateDescripcion ? 0  : this.misDatos.Total;
    
  }
  
  continuePrinciapal(){

    if(!this.ValidatebtnContinue()) return;
    this.showModal = !this.showModal;
    this.ModalContinuar = !this.ModalContinuar;
    this.dialog.closeAll();

    const loadTypecpeOld = this.cpe.Serie.substring(0, 1);
    this.listTalGeneral = this.listTalonario;
    this.listTalGeneral = this.listTalGeneral.filter( x=>String(x.NumeroSunat) === this.TipoNota && String(x.Serie).substring(0,1) === loadTypecpeOld )

    this.onChangeAlias('', true);
    this.formSeccionOne.patchValue({
      tipoNota      : this.nota.tipoNota,
      docAfectado   : this.cpe.IDComprobante,
      observacion   : this.nota.observacion,
    });

  }

  ValidatebtnContinue() : boolean {

    let detailInvalid = false;
    this.error = false;
    this.message = null;

    if( this.cpe.Detalle === undefined || Object.entries( this.cpe.Detalle ).length === 0 ){
      this.error = true;
      this.message = 'Agregue el detalle para continuar';
      return false;
    }

    this.cpe.Detalle.forEach( el =>{
      if(!el.Descripcion) detailInvalid = true;
    })

    if(detailInvalid){

      this.error = true;
      this.message = 'Verifique la descripcion del detalle no puede ir vacio';
      return false;
    }

    const totales = this.cpe.Detalle.map( x=>x.Importe ).reduce( ( t, a ) => t + a, 0 );

    if( totales <= 0 ){

      this.error = true;
        this.message = 'El total no debe ir con monto 0';
        return false;
    }
    
    if( this.TipoNota === "07" ){

      const totalDetalle = this.sfuntion._RoundDecimal( totales, 2 );
      const totalVenta = this.sfuntion._RoundDecimal( this.TotalVenta, 2 );
  
      if( Number(totalDetalle) > Number(totalVenta) ){
  
        this.error = true;
        this.message = `El total de la nota de crédito no debe superar el importe de la ${ this.cpeGeneral.TipoComprobante }`
        return false;
      }
    }

    return true;
  }

  _init_amountDetails(){

    this.cpe.Gratuita = 0;
    this.cpe.Exonerada = 0;
    this.cpe.Afecta = 0;
    this.cpe.Inafecta = 0;
    this.cpe.IGV = 0;
    this.cpe.Total = 0;

    this.cpeGeneral.TipoComprobante = 'BOLETA';

  }

  quitarItem( index ){

    this.cpe.Detalle.splice( index, 1 )
    if( this.cpe.Detalle.length  <= 0 ){

      this.form.reset();
      this.notdata = true;
      this.cpeGeneral = {} as IComprobante;
      this.cpe        = {} as IComprobante;
      this.nota       = {} as Nota;
      this.misDatos   = {} as MisDatos;
      this._init_amountDetails();

    }
    
    this._refreshTotales();

  }

  onChangeAlias( descripcion : string, init : boolean = false ){

    const loadTypecpeOld = this.cpe.Serie.substring(0, 1);
    const toSelect = init ? this.listTalGeneral.find( x=>String(x.NumeroSunat) === this.TipoNota && String(x.Serie).substring(0,1) === loadTypecpeOld ) :
                            this.listTalGeneral.find( x=>String(x.NumeroSunat) === this.TipoNota && x.Descripcion === descripcion && this.TipoNota && String(x.Serie).substring(0,1) === loadTypecpeOld );
    if( toSelect ){

      this.formSeccionOne.patchValue({
        aliasSerie  : toSelect.Descripcion,
        codSunat    : toSelect.NumeroSunat,
        serie       : toSelect.Serie,
        number      : toSelect.Numero,
        tipoimpresion : toSelect.Tamaño
      });
    }
  }

  onChangeObservacion(){
    this.nota.observacion = this.formSeccionOne.value.observacion;
  }

  getdateSelect( $event ){
    this.dateStart = $event;
  }

  validateKey(event, limite ) {

    if(!this.svalidator.validateKey( event, limite ))return false;
    return true;

  }

  returndata1( data : string ) : string {
    return this.formSeccionOne.get(data).value;
  }

  SaveNota(){

    if( this.formSeccionOne.invalid ){
      return this.svalidator.Empty_data(this.formSeccionOne);
    }

    this.initializeMessages();
    let detalle = [];
    
    const number = String(this.formSeccionOne.value.number).padStart(8, '0');
    const serie = this.returndata1('serie');
    const correlativo = `${serie}-${number}`;
    const codSunat = this.cpe.TipoComprobante === 'BOLETA' ? '03' : '01';
    const tipocpe = this.TipoNota === '07' ? 'NOTA DE CREDITO ELECTRÓNICA' : 'NOTA DE DEBITO ELECTRÓNICA';
    const ModificarDescripcion = this.nota.tipoNota === 'Correccion por error en la descripcion';
    
    const moneda = this.cpe.Moneda;
    const formaPago = this.cpe.TipoPago;

    this.cpe.Detalle.forEach( el => {

      let descripcion;
      if(!el.ModificaDescrip) descripcion = el.Descripcion;
      else if( ModificarDescripcion ) descripcion = el.ModificaDescrip
      else descripcion = el.Descripcion;

      const details = {

        idcomprobante     : correlativo,
        idproducto        : el.IDProducto ?? 0,
        descripcion       : descripcion,
        cantidad          : el.Cantidad,
        precio            : el.Precio,
        precioreferencial : el.Precio,
        TipoAfectacion    : el.Afectacion,
        bonificacion      : el.Bonificacion ?? false,
        unidadmedida      : el.UnidadMedida,
        docanticipo       : el.DocAnticipo ?? "",
        icbper            : el.Icbper ?? 0,
        idpresentacion    : el.IDPresentacion ?? "0",
        total             : this.sfuntion._RoundDecimal(el.Importe, this.pipedecimal)
      }

      detalle.push( details );
    });

    const body = {

      idcomprobante: correlativo,
      idusuario: JSON.parse(localStorage.getItem('_token_login')).clave,
      idtipopago: 0,
      // idtipopago: Number(this.returndata2('tipopago')),
      idmoneda: this.cpe.IDMoneda,
      idmediopago: 0,
      fecha: this.sfuntion.convert_fecha(this.dateStart),
      fechavencimiento: this.sfuntion.convert_fecha(this.dateStart),
      serie: serie,
      numero: number,
      codigocomprobante: this.TipoNota,
      ordencompra: '',
      tiponota: this.returndata1('tipoNota'),
      motivonota: this.returndata1('observacion'),
      tipocomprobanterelacionado: codSunat,
      serierelacionada: this.cpe.Serie,
      numerorelacionada: this.cpe.Numero,
      nombrecliente: this.cpe.Cliente,
      numerodocumento: this.cpe.NumeroDocumento,
      tipocambio: this.cpe.TipoCambio,
      descuentoporcentaje: 0.00,
      descuentosoles: 0.00,
      gravada:  this.sfuntion._RoundDecimal(this.cpe.Afecta, 2) ?? 0,
      gratuita: this.sfuntion._RoundDecimal(this.cpe.Gratuita, 2) ?? 0,
      exonerada: this.sfuntion._RoundDecimal(this.cpe.Exonerada, 2) ?? 0, 
      inafecta: this.sfuntion._RoundDecimal(this.cpe.Inafecta, 2) ?? 0,
      igv: this.sfuntion._RoundDecimal(this.cpe.IGV, 2) ?? 0,
      total: this.sfuntion._RoundDecimal(this.cpe.Total, 2) ?? 0,
      anticipo: 0,
      descripcion: '',
      guiaremitente: '',
      guiatransportista: '',
      placa: '',
      ordenpago: '',
      detraccionporcentaje: 0,
      detraccionmonto: 0,
      detraccionMontoAfectacion : 0,
      detraccionmontoafectado: 0,
      conimpuesto: this.cpe.ConImpuesto,
      icbper: 0.00,
      detalle: detalle,
      cuota : [],

      //print
      direccioncliente: this.cpe.Direccion ?? '',
      serienumero: correlativo,
      usuario: this._token_login.user,
      ruc: this._token_login.ruc,
      empresa: this._token_login.empresa,

      direccionempresa: this._token_login.direccion,
      direccionanexo: this.listTalGeneral.find( x=>x.Serie === serie ).Sucursal ?? '', 
      telefonoempesa: this._token_login.telefonoempresa,
      correoempresa: this._token_login.correoempresa,
      tipocomprobante: tipocpe,
      formapago: formaPago,
      moneda: moneda,
      subtotal: this.sfuntion._RoundDecimal( this.cpe.Total, 2) ?? 0,
      logo: this._token_login.logo,
      tipoimpresion : this.formSeccionOne.value.tipoimpresion ?? 'A4'
      
    };

    // debugger
    this.ssale.SaveSale( body ).subscribe( ( response : any ) => {

      if( response.message === 'exito' ){
        
        this.getTalonarioUser();
        this.ImprimirVenta(body);
        //ok
        this.success = true;
        this.showModal = false;
        this._changeExito();
      }

    }, (error) => this._exeception( error ) );

  }

  ImprimirVenta( body: any ){

    try{
      
      this.ssale._printSale( body ).subscribe( (response : any ) =>{
  
        if( response.message === 'exito' ){

          const bs64File = response.result;
          this.sfuntion.printDocument(bs64File);
        }
        else{
  
          this.error = true;
          this.message = response.message
  
        }
  
        this.spinner.hide();
  
      }, ( error ) => this._exeception( error ) );

    }catch(error){

      this._exeception( error )
    }


  }

  _changeExito() {

    setTimeout(() => {
      
      this.showModal = false;
      this.ModalContinuar= false;
      this.dialog.closeAll();

      this.success = false;
      this.cnn_expi = false;
      this.error = false;
      this.message = null;

      this.notdata = true;
      this.cpeGeneral = {} as IComprobante;
      this.cpe        = {} as IComprobante;
      this.nota       = {} as Nota;
      this.misDatos   = {} as MisDatos;
      this.openTab    = 1;
      this._init_amountDetails();
      this.form.reset();
      this.formSeccionOne.reset();
      
    }, 2000);

  }

  onChangeItemdetails( index : number ){
    
    this.calculate( index );
  }
  
  _typeAfectacion( afectacion : string ){

    const amount = this.cpe.Detalle.filter( x=> String(x.Afectacion).toUpperCase() === afectacion.toUpperCase() ).map( x=>x.Importe ).reduce( ( t, a ) => t + a, 0 );
    return amount;
  }

  calculate( index: number ){

    const quantity = this.cpe.Detalle[index].Cantidad ?? 0;
    const price = Number(this.cpe.Detalle[index].Precio) ?? 0;
    const total = price * quantity;

    this.cpe.Detalle[index].Importe = total;
    this.cpe.Detalle[index].Cantidad = quantity;
    this.cpe.Detalle[index].Precio = price;
    this._refreshTotales();

  }

  _refreshTotales(){

    if(!this.cpe.Detalle) return;
    const totales = this.cpe.Detalle.map( x=>x.Importe ).reduce( ( t, a ) => t + a, 0 );
    const montoAfecto = this._typeAfectacion('AFECTO');
    const montoInafecto = this._typeAfectacion('INAFECTO');
    const montoExonerado = this._typeAfectacion('EXONERADO');

    this.cpe.Gratuita = 0;
    this.cpe.anticipo = 0;
    this.cpe.Inafecta = montoInafecto     ?? 0;
    this.cpe.Exonerada = montoExonerado   ?? 0;

    if( this.cpe.ConImpuesto ){
      
      this.cpe.Afecta = montoAfecto / 1.18  ?? 0;
      this.cpe.IGV = this.cpe.Afecta * 0.18 ?? 0;
      this.cpe.subtotal = totales;
      this.cpe.Total = totales;
    }
    else{

      this.cpe.Afecta = montoAfecto ?? 0;
      this.cpe.IGV = this.cpe.Afecta * 0.18 ?? 0;
      this.cpe.subtotal = totales;
      this.cpe.Total = this.cpe.Afecta + this.cpe.Inafecta + this.cpe.Exonerada + this.cpe.IGV;
    }

  }

  _changeDebitoObservacion( index : number ){
    this.nota.observacion = this.cpe.Detalle[index].Descripcion;
  }

  _changeDescription( index : number ){

    const delleOrigen = JSON.parse(this.misDatos.detalle);
    const descOld = delleOrigen[index].Descripcion;
    const desnew = this.cpe.Detalle[index].Descripcion;

    const updateDescripcion = `DICE: ${descOld} DEBE DECIR:${desnew}`.toUpperCase();
    this.cpe.Detalle[index].ModificaDescrip = updateDescripcion;
  }

}

export interface Nota{
  tipoNota    : string;
  observacion : string;
}

export interface MisDatos{

  Moneda    : string;
  TipoPago  : string;
  Descuento : number;
  Exonerada : number;
  Afecta    : number;
  Inafecta  : number;
  IGV       : number;
  Total     : number;
  detalle   : string;
}