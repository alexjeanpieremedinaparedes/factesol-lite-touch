import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { BusinessService } from 'src/app/services/business.service';
import { FunctionsService } from '../../../services/functions.service';
import { PlantillaimpresionComponent } from './plantillaimpresion/plantillaimpresion.component';

@Component({
  selector: 'app-impresion',
  templateUrl: './impresion.component.html'
})
export class ImpresionComponent implements OnInit {
  
  itemsPlantilla : any;
  arrayPlantillas : any[] = [];

  data   = {} as configuration;

  ruc         : string;
  bs64logo    : any;
  delete      : boolean;
  success     : boolean;
  cnn_expi    : boolean;
  error       : boolean;
  message     : string;
  
  @ViewChild("fileInput") fileInput: ElementRef;
  constructor(  private spinner   : NgxSpinnerService,
                private sbusiness : BusinessService,
                private sfunction : FunctionsService,
                public  modal     : MatDialog   ) {

    //Listar  plantillas
    this._listPlantillas();

    const auth = JSON.parse(localStorage.getItem('_token_login'));
    this.data.ocultarvencimiento          = false;
    this.data.ocultarordencompra          = false;
    this.data.ocultarplaca                = false;
    this.data.ocultarguiatransportista    = false;
    this.data.ocultarGuiaremitente        = false;

    this.data['plantilla-ticket']        = '';
    this.data['plantilla-A4|A5']         = '';

    this.listConfig();
    this.ruc = auth.ruc;
    this.bs64logo = sfunction._convertbs64ForSrc(auth.logo);

  }

  ngOnInit(): void {}

  listConfig(){

    this.itemsPlantilla = {};
    this.initialize();
    this.sbusiness.listConfiguracion().subscribe( response => {
      
      const keys = Object.keys(this.data);
      keys.forEach( el =>{

        response.filter( x=>x.Operacion === el ).map( result =>{
          
          if(typeof this.data[el] != 'object' )this.data[el] = result.valor === 'SI' ? true : false;
          else this.data[el].forEach(e => { if(e.nombre == result.valor)  e.valor = true; });
          if( result.Operacion  === 'formatoREDdetalle' ) this.data[el] = result.valor;
          if( result.Operacion === 'plantilla-ticket' ) this.itemsPlantilla['plantilla-ticket'] = result.valor;
          if( result.Operacion === 'plantilla-A4|A5' ) this.itemsPlantilla['plantilla-A4|A5'] = result.valor;
        });

      });

      this.spinner.hide();

    }, ( error )=> this._exeception( error ) );

  }

  toggle( name : string, $event : any ){

    this.initialize();
    const valor = typeof($event) === 'boolean' && $event ? 'SI' : typeof($event) === 'boolean' && !$event ? 'NO' :  $event;

    const body = {
      operacion : name,
      value     : valor
    };

    this.sbusiness.AgregarConfiguracion( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){

        this.success = true;
        this.message = 'Operacion correcta';
        this.UpdateConfigLocalGeneral(name, $event);
        this._changeExito();

      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) );

  }
  
  elegirtipoplantilla(){

    const body = {
      plantillas : this.arrayPlantillas,
      ppredeterminadas : this.itemsPlantilla
    };

    this.modal.open(PlantillaimpresionComponent, {
      data : body,
      disableClose : true
    }).afterClosed().subscribe( response =>{
      
      const result = response.body;

      if( result.usar ){
        
        const name = result.nameconfig;
        const valor = result.valor;
        this.toggle( name, valor );
      }
    })
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {

    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }

  uploadFileEvt(imgFile: any) {

    if (imgFile.target.files && imgFile.target.files[0]) {

      let reader = new FileReader();

      reader.onload = (e: any) => {

        let image = new Image();
        image.src = e.target.result;
        image.onload = rs => {

          let imgBase64Path = e.target.result;
          const bs64split = imgBase64Path.split(',')[1];
          this._changeLogo(bs64split);
          this.bs64logo = this.sfunction._convertbs64ForSrc(bs64split);

        };
      };

      reader.readAsDataURL(imgFile.target.files[0]);
      if(this.fileInput)this.fileInput.nativeElement.value = "";
    }
  }

  _changeLogo( bs64img:any ){

    this.initialize();
    const body = {
      ruc : this.ruc,
      logobase64 : bs64img
    };

    this.sbusiness.changeLogo( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){

        this.success = true;
        this.message = 'Operacion correcta';
        this.UpdateConfigLocal(bs64img );
        this._changeExito();

      }

      this.spinner.hide();

    }, ( error ) => this._exeception( error ) );
  }

  UpdateConfigLocal( valor : any ){

    const auth = JSON.parse(localStorage.getItem('_token_login'));
    let dataUpdate = auth;
    if( dataUpdate ){
      dataUpdate.logo = valor;
      localStorage.setItem('_token_login', JSON.stringify(auth) );
      document.getElementById('logo1').setAttribute('src', this.sfunction._convertbs64ForSrc( valor) )
      document.getElementById('logo2').setAttribute('src', this.sfunction._convertbs64ForSrc( valor) )
    }

    if( !valor ){
      this.bs64logo = null;
    }
  }

  UpdateConfigLocalGeneral( name : string ,valor : any ){
    
    const auth = JSON.parse(localStorage.getItem('_token_login'));
    const _config = JSON.parse(auth.configuration);
    let dataUpdate = _config.find( x => x.Operacion === name );

    if( dataUpdate ){
      
      dataUpdate.valor = valor;
      auth.configuration = JSON.stringify(_config);
      localStorage.setItem('_token_login', JSON.stringify(auth) );
    }

    if( name === 'plantilla-ticket' )this.itemsPlantilla['plantilla-ticket'] = valor;
    else if( name === 'plantilla-A4|A5' ) this.itemsPlantilla['plantilla-A4|A5'] = valor;
    
  }

  //Listar plantillas

  _listPlantillas(){

    this.spinner.show();
    this.sbusiness.listPlantillas().subscribe( response =>{

      if( response['message'] === "exito" ){
        this.arrayPlantillas = response['result'];
      }

      this.spinner.hide();
    }, (e)=> this._exeception(e) );
  }

  _showDelete(){
    
    this.delete = !this.delete;
    this.message = '¿ Desea eliminar su logo ?';
  }

  _deleteFoto( $event : boolean ){
    if( $event ){
      this._changeLogo(null);
    }
  }

}

export interface configuration{

  ocultarvencimiento          : boolean;
  ocultarordencompra          : boolean;
  ocultarplaca                : boolean;
  ocultarguiatransportista    : boolean;
  ocultarGuiaremitente        : boolean;
  ['plantilla-ticket']        : string;
  ['plantilla-A4|A5']         : string;
}