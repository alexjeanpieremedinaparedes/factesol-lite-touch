import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { DetalleplantillaComponent } from '../detalleplantilla/detalleplantilla.component';
import { ImpresionComponent } from '../impresion.component';

@Component({
  selector: 'app-plantillaimpresion',
  templateUrl: './plantillaimpresion.component.html',
  styleUrls: ['./plantillaimpresion.component.css']
})
export class PlantillaimpresionComponent implements OnInit {

  openTab : number = 1;
  arrayPlantillas : any[] = [];

  constructor(
    public modal:MatDialog,
    private dialogRef : MatDialogRef<ImpresionComponent>,
    @Inject(MAT_DIALOG_DATA) public data : any
  ) {}

  ngOnInit(): void {
    this._listPlantillas();  
  }

  toggleTabs($tabNumber: number){
    this.openTab = $tabNumber;
    this._listPlantillas();
  }

  _listPlantillas(){
    const formato =  this.openTab === 1 ? [ 'A4', 'A5' ] : [ 'TICKET' ];
    this.arrayPlantillas = this.data.plantillas.filter((x : any )=>formato.includes(x.formato.toUpperCase()));
  }

  close(){
    
    const body = { usar : false };
    this.dialogRef.close({ body});
  }


  detalleplantilla( item : any ){

    const body = {
      plantilla : item
    };

    this.modal.open(DetalleplantillaComponent, {
      data : body,
      disableClose : true
    }).afterClosed().subscribe( response =>{
      if( response.body.usar ){
        this.dialogRef.close(response);
      }

    });
  }

}
