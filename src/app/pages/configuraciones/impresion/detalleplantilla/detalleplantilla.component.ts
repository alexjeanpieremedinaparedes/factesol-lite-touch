import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlantillaimpresionComponent } from '../plantillaimpresion/plantillaimpresion.component';

@Component({
  selector: 'app-detalleplantilla',
  templateUrl: './detalleplantilla.component.html',
  styleUrls: ['./detalleplantilla.component.css']
})
export class DetalleplantillaComponent implements OnInit {

  ticket : boolean;
  constructor(
    private dialogRef : MatDialogRef<PlantillaimpresionComponent>,
    @Inject( MAT_DIALOG_DATA ) public data : any
    ) {
    
    const formato = data.plantilla.formato;
    this.ticket = formato === 'TICKET' ? true : false;
  }

  ngOnInit(): void {
  }

  _usarDisenio( usar : boolean ){
    
    if( usar ){

      let body:any = {};
      if( this.ticket ){
        body.nameconfig = 'plantilla-ticket';
      }
      else{
        body.nameconfig = 'plantilla-A4|A5';
      }

      body.usar = usar;
      body.valor = this.data.plantilla.nombrecomprobante;
      this.dialogRef.close({ body});
    }
  }

  _testPrint( ){

    this.printIframe(this.data.plantilla.plantilla);
  }

  close(){
    
    const body = { usar : false };
    this.dialogRef.close({ body});
  }

  printIframe (url : string) {

    let proxyIframe = document.createElement ('iframe');
    document.body.appendChild (proxyIframe);
    proxyIframe.style.display = 'none';
    proxyIframe.style.width = '100%';
    proxyIframe.style.height = '100%';
    const iframe = `<iframe src = ${url} onload = "print ();" width = "800" height = "1000" frameborder = "0" marginheight = "0" marginwidth = "0">`
    let contentWindow = proxyIframe.contentWindow;
    contentWindow.document.open();
    contentWindow.document.write(iframe);
    contentWindow.document.close();
  }

}
