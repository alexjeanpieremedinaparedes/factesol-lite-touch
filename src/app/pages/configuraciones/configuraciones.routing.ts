import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { UsuariosComponent } from './usuarios/usuarios/usuarios.component';
import { VentaComponent } from './venta/venta.component';
import { ImpresionComponent } from './impresion/impresion.component';
import { MicuentaComponent } from './micuenta/micuenta.component';
import { ConfiguracionesComponent } from './configuraciones.component';
import { FacturacionSunatComponent } from './facturacion/facturacion-sunat/facturacion-sunat.component';
import { CorreosComponent } from './correos/correos.component';

const routes: Routes = [
    { path: '',
     component:ConfiguracionesComponent,

     children:[
        { path: 'micuenta', component: MicuentaComponent , data: { title: 'Mi Cuenta' } },
        { path: 'usuario', component: UsuariosComponent , data: { title: 'Usuario' }},
        { path: 'correos', component: CorreosComponent , data: { title: 'correo' }},
        { path: 'impresion', component: ImpresionComponent , data: { title: 'Impresión' }} ,
        { path: 'venta', component: VentaComponent, data: { title: 'Configuración Venta' } },
        { path: 'facturacionsunat', component: FacturacionSunatComponent, data: { title: 'Facturación Sunat' } },
        {path: '', redirectTo: '/inicio' , pathMatch: 'full'},
    ]
},

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class configuracionesRoutingModule { }
