import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
import { ValidatorsService } from 'src/app/services/validators.service';

@Component({
  selector: 'app-micuenta',
  templateUrl: './micuenta.component.html'
})
export class MicuentaComponent implements OnInit {

  success : boolean;
  cnn_expi: boolean;
  error   : boolean;
  message : string;

  user    : string;
  ruc     : string;
  idusuario: number;

  changePassword:boolean;
  name:string;
  lastname:string;
  dni : string;

  Admin:boolean;
  Reimprimir:boolean;
  CambioPrecio:boolean;

  form:FormGroup;

  constructor(  private auth      : AuthService,
                private fb        : FormBuilder,
                private svalidator: ValidatorsService,
                private spinner   : NgxSpinnerService ) {

    this.load_dataUser();
    this.create_form();

  }

  ngOnInit(): void { }

  get userInvalid(){
    return this.svalidator.control_invalid('user', this.form);
  }

  get passNowInvalid(){
    return this.svalidator.control_invalid('passNow', this.form);
  }

  get newpassInvalid(){
    return this.svalidator.control_invalid('newpass', this.form);
  }

  get repeatpassInvalid(){
    return this.svalidator.control_invalid('repeatpass', this.form);
  }

  create_form(){
    this.form = this.fb.group({
      user      : [ this.user, Validators.required ],
      passNow   : [ '', Validators.required ],
      newpass   : [ '', Validators.required ],
      repeatpass: [ '', Validators.required ]
    });
  }

  load_dataUser(){

    const _logindata = localStorage.getItem('_token_login');
    if( _logindata ){

      const user  = JSON.parse(_logindata);
      this.name = user.Nombre;
      this.lastname = user.Apellidos;
      this.dni =  user.dni;

      this.Admin = user.padmin;
      this.Reimprimir = user.preimprimir;
      this.CambioPrecio = user.changePrecio;

      this.ruc = user.ruc;
      this.user = user.user;
      
    }

  }

  change_Password(){

    if( this.form.controls.passNow.invalid ){
      this.form.controls.passNow.markAsTouched();
      return;
    }

    this.initilizar();
    const body = {
      ruc     : this.ruc,
      usuario : this.form.value.user,
      password: this.form.value.passNow
    }

    this.auth.login( body ).subscribe( (response : any ) => {

      if( response.message === 'exito' ){
        this.changePassword = true;
        this.idusuario = response.result.usuario.Clave;
      }
      else this.changePassword = false;
      this.spinner.hide();

    }, (error)=>{

      this.changePassword = false;
      this.error = true;
      this.message = error.error.message === null || error.error.message === undefined ? "Sin conexion al servidor" : 'La contraseña que ingreso es incorrecta !!!';
      this.spinner.hide();
      
    })

  }

  saveNewPass(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data(this.form);
    }

    this.initilizar();
    if( this.form.value.newpass !== this.form.value.repeatpass ){
      
      this.changePassword = true;
      this.error = true;
      this.message = 'las contraseñas no coinciden';
      this.spinner.hide();
      return;

    }

    const body = {
      idusuario : this.idusuario,
      password  : this.form.value.repeatpass
    }

    this.auth.changePassword( body ).subscribe( response =>{
      this.success = true;
      this.message = 'Operacion correcta';
      this.close_success();
      this.spinner.hide();

    }, ( error )=>{

      console.log(error)
      this.error = true;
      const cnn_expi = error.error === 'Unauthorized';
      this.cnn_expi = cnn_expi;
      this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
      this.spinner.hide();

    })
  }

  initilizar(){
    this.spinner.show();
    this.changePassword = false;
    this.error = false;
    this.cnn_expi = false;
    this.success = false;
    this.message = null;
  }

  clear_form(){
    
    this.form.patchValue({
      passNow : null,
      newpass : null,
      repeatpass: null,
    });

  }

  close_success(){

    setTimeout(()=>{
      this.success = false;
      this.message = null;
      this.clear_form();
      this.form.controls.passNow.setErrors(null);
      this.auth.logout();
    }, 3000)
  }

}
