import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
import { FunctionsService } from 'src/app/services/functions.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { AsignarPermisosUsuariosComponent } from '../modalusuario/asignar-permisos-usuarios/asignar-permisos-usuarios.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html'
})
export class UsuariosComponent implements OnInit {


  idUsuario : number;
  delete    : boolean = false;
  success   : boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;

  nuevouser: boolean = false;

  columns = [];
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource([]);
  listUser : [] = [];
  _cantItems : number;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  form:FormGroup;
  constructor(  private auth        : AuthService,
                private sfunction   : FunctionsService,
                private svalidator  : ValidatorsService,
                private spinner     : NgxSpinnerService,
                private fb          : FormBuilder,
                public modal  : MatDialog ) {

    this.create_form();
    this.listUsers('');
  }

  ngOnInit(): void {}

  get nameInvalid(){
    return this.svalidator.control_invalid('nombre', this.form);
  }

  get lastnameInvalid(){
    return this.svalidator.control_invalid('apellido', this.form);
  }

  get dniInvalid(){
    return this.svalidator.control_invalid('dni', this.form);
  }

  get userInvalid(){
    return this.svalidator.control_invalid('usuario', this.form);
  }

  get pass1Invalid(){
    return this.svalidator.control_invalid('password', this.form);
  }

  get pass2Invalid(){
    const pass1Value = this.form.value.password;
    const pass2Value = this.form.value.password2;

    return ( pass1Value === pass2Value ) ? false : true;
  }

  create_form(){
    this.form = this.fb.group({

      idusuario     : [ 0 ],
      usuario       : [ '', [ Validators.required, Validators.minLength(3) ] ],
      password      : [ '', [ Validators.required, Validators.minLength(3) ] ],
      password2     : [ '', [ Validators.required, Validators.minLength(3) ] ],
      nombre        : [ '', [ Validators.required, Validators.minLength(3) ] ],
      apellido      : [ '', [ Validators.required, Validators.minLength( 3 ) ] ],
      dni           : [ '', [ Validators.required, Validators.minLength(8), Validators.maxLength(8) ] ],
      acceso        : [ true ],
      admin         : [ false ],
      reimprimir    : [ true ],
      cambiarprecio : [ true ],
      editar        : [ false ]

    }, {
      validators : this.svalidator.passIguales('password', 'password2')
    });
  }

  // modal nuevo usuario
  modalnuevo(){
    this.nuevouser =!this.nuevouser;
    this._clear();
  }

  listUsers(text:string){

    this.initializeTable();
    this.auth.listUsers().subscribe( response => {

      if( response.length > 0 ){

        const columns = response ?? [];
        const keys = this.sfunction.KeyCabecera(columns[0]);
        this.columns = keys.columns;
        this.displayedColumns = keys.displayedColumns;
        this.dataSource = new MatTableDataSource( response );
        this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Usuario', text ) );
        this.listUser = response;
        this.dataSource.paginator = this.paginator;
        this.paginator._intl.itemsPerPageLabel = 'items por pagina';
        this._cantItems = this.dataSource.data.length;
      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) )
  }

  SaveUser(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data( this.form );
    }

    this.initialize();
    this.spinner.show();
    const body = {
      ... this.form.value
    };
    
    this.auth.saveUser( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){
        this.success = true;
        this._changeExito();
        this._clear();
        this.refresh();
        if( body.editar ) this.nuevouser = false;

      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) )

  }

  deleteUsers( $event ){

    this.initialize();
    if( !$event ) return;
    const body = {
      idusuario : this.idUsuario
    };

    this.spinner.show();
    this.auth.deleteUser( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){
        this.success = true;
        this.message = 'Registro eliminado con exito';
        this.refresh();
        this._changeExito();
      }
      this.spinner.hide();

    }, ( error )=> this._exeception( error ) )

  }

  _clear(){

    this.form.reset({

      idusuario     : 0,
      usuario       : '',
      password      : '',
      password2     : '',
      nombre        : '',
      apellido      : '',
      dni           : '',
      acceso        : true,
      admin         : false,
      reimprimir    : true,
      cambiarprecio : true,
      editar        : false

    });

  }
  
  refresh() {
    let search = (<HTMLInputElement>document.getElementById('search')).value;

    this.auth.listUsers().subscribe( response => {
      if(response.length > 0){

        this.dataSource = new MatTableDataSource<Element>(response);
        this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Usuario', search ) );
        this.dataSource.paginator = this.paginator;
        this._cantItems = this.dataSource.data.length;
        this.listUser = response;

      }

    }, (error)=>{
      this._exeception( error )
    })
  }

  initialize() {
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  initializeTable(){
    this.spinner.show();
    this.dataSource = null;
    this.columns = [];
    this.displayedColumns = [];
    this.initialize();
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  _search( text:string ){

    this.dataSource = new MatTableDataSource( this.listUser );
    this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Usuario' ,  text ) );
    this.dataSource.paginator = this.paginator;
    this._cantItems = this.dataSource.data.length;
  }

  enventOperation( $event ){

    if( $event.titulo === 'Eliminar' ){

      this.idUsuario = $event.id;
      this.message = '¿Está seguro de eliminar el usuario?';
      this.delete = !this.delete;

    }
    else if( $event.titulo === 'Editar' ){
      
      this.nuevouser = !this.nuevouser;
      this.idUsuario = $event.id;;
      this.listUser.filter( (x : any ) => x.ID === this.idUsuario ).map( (result : any ) => {

        this.form.setValue({
          idusuario     : this.idUsuario,
          usuario       : result.Usuario,
          password      : '',
          password2     : '',
          nombre        : result.NombreTrabajador,
          apellido      : result.ApellidoTrabajador,
          dni           : result.DNI,
          acceso        : true,
          admin         : result.Admin,
          reimprimir    : result.Reimprimir,
          cambiarprecio : result.CambiarPrecio,
          editar        : true
        });

      } );
    }

  }

  ShowModalPermisos( $event ){

    this.modal.open(AsignarPermisosUsuariosComponent, {
      data: {
        idusuario : $event.ID,
        trabajador : `${$event.NombreTrabajador} ${$event.ApellidoTrabajador}`
      }
    }).afterClosed().subscribe( response => {

      if( response.exito ){

        this.success = true;
        this.message = 'Permisos asignados correctamente';
        this._changeExito();

      }

    });

  }

  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }

}