import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { BusinessService } from 'src/app/services/business.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { UsuariosComponent } from '../../usuarios/usuarios.component';

@Component({
  selector: 'app-asignar-permisos-usuarios',
  templateUrl: './asignar-permisos-usuarios.component.html',
  styles: [
  ]
})
export class AsignarPermisosUsuariosComponent implements OnInit {

  success: boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;
  listPermisos : any[] = [];

  constructor(  private sbusiness: BusinessService,
                private spinner    : NgxSpinnerService,
                public dialogRef  : MatDialogRef<UsuariosComponent>,
                @Inject( MAT_DIALOG_DATA ) public data : any ) {
    this.listPermisosUser();
  }

  ngOnInit(): void {}

  listPermisosUser(){

    this.initialize();
    const body = {
      idusuario : String(this.data.idusuario)
    };

    this.sbusiness.listPermisoUsuario( body ).subscribe( response => {

      this.listPermisos = response;
      this.spinner.hide();

    }, ( error ) => this._exeception( error ) );

  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {

    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;

  }

  onChange(Description : string, $event : boolean ){

    let dataModificar = this.listPermisos.find( x=>x.DescripcionWeb === Description );
    dataModificar.Seleccionar = $event;

  }

  Save(){

    this.initialize();
    let permisos = [];
    
    this.listPermisos.forEach(el =>{

      const body = {
        idformulario  : el.Clave,
        idusuario     : this.data.idusuario,
        permiso       : el.Seleccionar
      }
      permisos.push(body);

    });
    
    const body = { permisos : permisos };
    this.sbusiness.AgregarPermisoUsuario( body ).subscribe( resposne => {

      this.success = true;
      this.dialogRef.close( { exito: true } );
      this.spinner.hide();

    }, (error) => this._exeception( error ) );

  }

}
