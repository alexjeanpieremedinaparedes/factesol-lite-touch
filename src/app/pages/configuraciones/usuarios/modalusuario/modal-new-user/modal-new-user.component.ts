import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
import { FunctionsService } from 'src/app/services/functions.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { AsignarnuevousuarioComponent } from '../../../facturacion/modalfacturacion/asignarnuevousuario/asignarnuevousuario.component';
import { AsignarPermisosUsuariosComponent } from '../asignar-permisos-usuarios/asignar-permisos-usuarios.component';

@Component({
  selector: 'app-modal-new-user',
  templateUrl: './modal-new-user.component.html',
  styleUrls: ['./modal-new-user.component.css']
})
export class ModalNewUserComponent implements OnInit {

  idUsuario : number;
  delete    : boolean = false;
  success   : boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;
  form:FormGroup;
  constructor(  private auth        : AuthService,
                private sfunction   : FunctionsService,
                private svalidator  : ValidatorsService,
                private spinner     : NgxSpinnerService,
                private fb          : FormBuilder,
                public modal        : MatDialog,
                public dialogRef    : MatDialogRef<AsignarnuevousuarioComponent> ) {
    this.create_form();
  }

  ngOnInit(): void {}

  asignarpermisos(){
    this.modal.open(AsignarPermisosUsuariosComponent);
  }

  get nameInvalid(){
    return this.svalidator.control_invalid('nombre', this.form);
  }

  get lastnameInvalid(){
    return this.svalidator.control_invalid('apellido', this.form);
  }

  get dniInvalid(){
    return this.svalidator.control_invalid('dni', this.form);
  }

  get userInvalid(){
    return this.svalidator.control_invalid('usuario', this.form);
  }

  get pass1Invalid(){
    return this.svalidator.control_invalid('password', this.form);
  }

  get pass2Invalid(){
    const pass1Value = this.form.value.password;
    const pass2Value = this.form.value.password2;

    return ( pass1Value === pass2Value ) ? false : true;
  }

  create_form(){
    this.form = this.fb.group({

      idusuario     : [ 0 ],
      usuario       : [ '', [ Validators.required, Validators.minLength(3) ] ],
      password      : [ '', [ Validators.required, Validators.minLength(3) ] ],
      password2     : [ '', [ Validators.required, Validators.minLength(3) ] ],
      nombre        : [ '', [ Validators.required, Validators.minLength(3) ] ],
      apellido      : [ '', [ Validators.required, Validators.minLength( 3 ) ] ],
      dni           : [ '', [ Validators.required, Validators.minLength(8), Validators.maxLength(8) ] ],
      acceso        : [ true ],
      admin         : [ false ],
      reimprimir    : [ true ],
      cambiarprecio : [ true ],
      editar        : [ false ]

    }, {
      validators : this.svalidator.passIguales('password', 'password2')
    });
  }


  SaveUser(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data( this.form );
    }

    this.initialize();
    this.spinner.show();
    const body = {
      ... this.form.value
    };
    
    this.auth.saveUser( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){
        
        this.success = true;
        this._clear();
        this.dialogRef.close({ exito: true });

      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) )

  }

  _clear(){

    this.form.reset({

      idusuario     : 0,
      usuario       : '',
      password      : '',
      password2     : '',
      nombre        : '',
      apellido      : '',
      dni           : '',
      acceso        : true,
      admin         : false,
      reimprimir    : true,
      cambiarprecio : true,
      editar        : false

    });

  }

  initialize() {
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

}
