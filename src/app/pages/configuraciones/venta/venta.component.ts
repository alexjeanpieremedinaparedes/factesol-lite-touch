import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { BusinessService } from '../../../services/business.service';
import { map } from 'rxjs/operators';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html'
})
export class VentaComponent implements OnInit {

  data   = {} as configuration;
  listPresent : any[] = [];

  success     : boolean;
  cnn_expi    : boolean;
  error       : boolean;
  message     : string;

  constructor(  private spinner   : NgxSpinnerService,
                private sbusiness : BusinessService,
                private sproduct  : ProductsService ) {

    this.data.moneda = [ {valor: false,nombre:'PEN'}, {valor: false, nombre:'USD'} ];
    this.data.tipopago = [ {valor: false,nombre:'contado'}, {valor: false, nombre:'credito'} ];
    this.data.otro = [ {valor: false,nombre:'conigv'}, {valor: false, nombre:'sinigv'} ];
    this.data.comprobante = [ {valor: false,nombre:'03'}, {valor: false, nombre:'01'},{valor: false, nombre:'00'} ];
    this.data.tipoafectacion = [ {valor: false,nombre:'afecto'}, {valor: false, nombre:'inafecto'},{valor: false, nombre:'exonerado'} ];

    this.data.presentacion                = '';
    this.data.afecto                  = false;
    this.data.verDetraccion           = false;
    this.data.cambiarfecha            = false;
    this.data.descuentos              = false;
    this.data.guiasYotros             = false;

    this.data.productoMasPresentacion = false;
    this.data.formatoREDdetalle       = 0;
    this.data.importeMinCredito       = 0;

    this.data.servicio                = false;
    this.data.contado                 = false;
    this.data.credito                 = false;

    this.data.boleta                  = false;
    this.data.factura                 = false;
    this.data.tickect                 = false;

    this.data.tppagoAfecto            = false;
    this.data.tppagoInafecto          = false;
    this.data.tppagoExonerada         = false;

    this.data.mSoles                  = false;
    this.data.mDolares                = false;

    this.data.conIgv                  = false;
    this.data.sinIgv                  = false;

    this._lisprodct();
    this.listConfig();

  }

  ngOnInit(): void {}

  listConfig(){

    this.initialize();
    this.sbusiness.listConfiguracion().subscribe( response => {

      const keys = Object.keys(this.data);
      keys.forEach( el =>{

        response.filter( x=>x.Operacion === el ).map( result =>{

          if(typeof this.data[el] != 'object' )this.data[el] = result.valor === 'SI' ? true : false;
          else this.data[el].forEach(e => { if(e.nombre == result.valor)  e.valor = true; });
          if( result.Operacion  === 'formatoREDdetalle' || result.Operacion  === 'importeMinCredito'  ) this.data[el] = result.valor;
          if( result.Operacion === 'presentacion' ) this.data[el] = Number(result.valor);
        });

      });

      this.spinner.hide();

    }, ( error )=> this._exeception( error ) );

  }

  _lisprodct(){

    this.initialize();
    this.sproduct.listPresentation().subscribe( response => {
    
      this.listPresent = response.filter(x=>x.VentaRapida === true);
      this.spinner.hide();

    }, (error) => this._exeception(error) );
  }

  toggle( name : string, $event : any ){

    this.initialize();
    $event = name === 'presentacion' ? String($event) : $event;
    const valor = typeof($event) === 'boolean' && $event ? 'SI' : typeof($event) === 'boolean' && !$event ? 'NO' :  $event;
    const body = {
      operacion : name,
      value     : valor
    };

    this.sbusiness.AgregarConfiguracion( body ).subscribe( (response : any ) =>{
      
      if( response.message === 'exito' ){

        this.success = true;
        this.message = 'Operacion correcta';
        this._changeExito();

        //Update localStorage
        this.UpdateConfigLocal( name, valor)

      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) );

  }


  GuardarPredeterminado(nombre: string, name : string, $event : any ){

    this.initialize();
    const valor = typeof($event) === 'boolean' && $event ? 'SI' : typeof($event) === 'boolean' && !$event ? 'NO' :  $event;
    this.data[nombre].forEach(e => { if(e.nombre != name)e.valor = false; });
    const body = {
      operacion : nombre,
      value     : name
    };

    this.sbusiness.AgregarConfiguracion( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){

        this.success = true;
        this.message = 'Operacion correcta';
        this._changeExito();

        //Update localStorage
        this.UpdateConfigLocal( nombre, name)

      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) );

  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {

    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;

  }

  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }

  UpdateConfigLocal( name : string ,valor : any ){
    
    const auth = JSON.parse(localStorage.getItem('_token_login'));
    const _config = JSON.parse(auth.configuration);
    let dataUpdate = _config.find( x => x.Operacion === name );

    if( dataUpdate ){
      
      dataUpdate.valor = valor;
      auth.configuration = JSON.stringify(_config);
      localStorage.setItem('_token_login', JSON.stringify(auth) );
    }
    
  }

}

export interface configuration{

  afecto                  : boolean;
  verDetraccion           : boolean;
  cambiarfecha            : boolean;
  descuentos              : boolean;
  guiasYotros             : boolean;

  productoMasPresentacion : boolean;
  formatoREDdetalle       : number;
  importeMinCredito       : number;

  servicio                : boolean;
  contado                 : boolean;
  credito                 : boolean;

  boleta                  : boolean;
  factura                 : boolean;
  tickect                 : boolean;

  tppagoAfecto            : boolean;
  tppagoInafecto          : boolean;
  tppagoExonerada         : boolean;

  mSoles                  : boolean;
  mDolares                : boolean;

  conIgv                  : boolean;
  sinIgv                  : boolean;

  presentacion            : string;
  comprobante             : any[];
  moneda                  : any[];
  tipopago                : any[];
  tipoafectacion          : any[];
  otro                    : any[];
}
