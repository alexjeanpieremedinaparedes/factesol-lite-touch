import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { BusinessService } from 'src/app/services/business.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { AsignarComponent } from '../asignar/asignar.component';

@Component({
  selector: 'app-nuevoanexo',
  templateUrl: './nuevoanexo.component.html',
  styles: [
  ]
})
export class NuevoanexoComponent implements OnInit {

  success   : boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;

  form : FormGroup;
  constructor(  private sbussines : BusinessService,
                private spinner   : NgxSpinnerService,
                private fb        : FormBuilder,
                private svalidator: ValidatorsService,
                public dialogRef    : MatDialogRef<AsignarComponent>,
                @Optional() @Inject( MAT_DIALOG_DATA ) private data : any ) {
                  
    this.create_form();
    if( data ){
      this.loadForm();
    }
  }

  ngOnInit(): void {}

  get codAnexoInvalid(){
    return this.svalidator.control_invalid('codigo', this.form);
  }

  get direccionInvalid(){
    return this.svalidator.control_invalid('direccion', this.form);
  }

  get sucursalInvalid(){
    return this.svalidator.control_invalid('denominacion', this.form);
  }

  create_form(){

    this.form = this.fb.group({
      idsucursal    : [ 0 ],
      codigo        : [ '', [ Validators.required, Validators.minLength(4), Validators.maxLength(4) ] ],
      direccion     : [ '', [ Validators.required, Validators.minLength(3) ] ],
      denominacion  : [ '', [ Validators.required, Validators.minLength(3) ] ],
      editar        : [ false ]
    });

  }

  loadForm(){

    this.form.patchValue({
      idsucursal    : this.data.id ,
      codigo        : this.data.codAnexo,
      direccion     : this.data.Direccion,
      denominacion  : this.data.denominacion,
      editar        : true
    });

  }

  SaveOffice(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data( this.form );
    }

    this.spinner.show();
    const body = {
      ... this.form.value
    };

    this.sbussines.SaveSucursal( body ).subscribe( (response : any ) => {

      if( response.message === 'exito' ){
        this.success = true;
        this._clear();
        this.dialogRef.close({ exito: true });
      }

      this.spinner.hide();

    }, ( error )=> this._exeception( error ) );

  }


  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }


  _clear(){

    this.form.reset({

      idsucursal    : 0,
      codigo        : '',
      direccion     : '',
      denominacion  : '',
      editar        : false

    });

  }

  

}
