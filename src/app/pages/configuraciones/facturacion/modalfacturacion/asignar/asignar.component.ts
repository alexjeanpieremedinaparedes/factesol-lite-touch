import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { NuevoanexoComponent } from '../nuevoanexo/nuevoanexo.component';
import { BusinessService } from 'src/app/services/business.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FacturacionSunatComponent } from '../../facturacion-sunat/facturacion-sunat.component';
import { FunctionsService } from 'src/app/services/functions.service';

@Component({
  selector: 'app-asignar',
  templateUrl: './asignar.component.html',
  styles: [
  ]
})
export class AsignarComponent implements OnInit {

  idDelete    : number;
  idSucursal  : number;
  delete      : boolean;
  success     : boolean;
  cnn_expi    : boolean;
  error       : boolean;
  message     : string;
  listoffice  : any[] = [];
  listAlt     : any[] = [];

  constructor(  public dialog     : MatDialog,
                private sbusiness : BusinessService,
                private spinner   : NgxSpinnerService,
                private sfunction : FunctionsService ,
                public dialogRef  : MatDialogRef<FacturacionSunatComponent>,
                @Inject( MAT_DIALOG_DATA ) public data : any ) {
    this.idSucursal = data.idSucursal;
    this.listOffices();
  }

  ngOnInit(): void {}

  listOffices(){

    this.initialize();
    this.sbusiness.listSucursal().subscribe( response => {

      this.listoffice = response;
      this.listAlt = response;
      this.spinner.hide();

    }, (error) =>  this._exeception( error ));

  }

  refresh() {
    this.sbusiness.listSucursal().subscribe( response => {
      this.listoffice = response;
      this.listAlt = response;
      this.spinner.hide();
    }, (error) => this._exeception( error ) );
  }

  initialize() {
    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  _AgregarAnexo( edit : boolean = false, id : number = 0 ){

    let body;
    this.listAlt.filter( x=>x.IDSucursal === id ).map( response => {
      body = {
        denominacion : response.Denominacion,
        codAnexo     : response.CodigoSucursal,
        Direccion    : response.Direccion,
        id           : response.IDSucursal
      }

    });

    this.dialog.open(NuevoanexoComponent, {
      data : body
    })
    .afterClosed()
    .subscribe( response =>{
      if( response.exito ){
        this.success = true;
        if( edit ) this.message = 'El registro se modifico de manera correcta';
        this._changeExito();
        this.refresh();
      }
    });

  }

  showdelete( idSucursal : number ){

    this.idDelete = idSucursal;
    this.delete = !this.delete;
    this.message = '¿Eliminar Sucursal?';

  }

  deleteAnexo( $event ){

    this.initialize();
    if( !$event ) return;
    const body = {
      idsucursal : this.idDelete
    };

    this.sbusiness.DeleteSucursal( body ).subscribe( (response : any) =>{

      if( response.message === 'exito' ){
        this.success = true;
        this.message = 'Registro eliminado con exito';
        this.refresh();
        this._changeExito();
      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) );

  }

  _changeItem( idSucursal : number ){
    this.idSucursal = idSucursal;
  }

  SaveAnexoSucursal(){

    this.initialize();

    const validator = [ undefined, null, 0 ];

    if( validator.includes(this.data.idTalonario) || validator.includes( this.idSucursal ) ){
      this.error = true;
      this.message = 'Operacion incorrecta';
      return;
    }

    const body = {
      idtalonario: this.data.idTalonario,
      idsucursal: this.idSucursal
    }

    this.sbusiness.TalonarioAsignarSucursal( body ).subscribe( (response : any ) => {
      
      if( response.message === 'exito' ){
        this.success = true;
        this.dialogRef.close({ exito: true });
      }
      
      this.spinner.hide();

    }, ( error ) => this._exeception( error ) )

  }

  _search( text:string ){

    this.listoffice = this.listAlt;
    this.listoffice = this.sfunction._search( this.listoffice, 'Denominacion', text );
    
  }

  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }
}