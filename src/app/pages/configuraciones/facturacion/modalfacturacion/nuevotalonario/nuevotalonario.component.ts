import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { BusinessService } from 'src/app/services/business.service';
import { SaleService } from 'src/app/services/sale.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { FacturacionSunatComponent } from '../../facturacion-sunat/facturacion-sunat.component';

@Component({
  selector: 'app-nuevotalonario',
  templateUrl: './nuevotalonario.component.html',
  styles: [
  ]
})
export class NuevotalonarioComponent implements OnInit {

  listTipoComprobante : any [] = [];
  idTalonario : number;
  delete      : boolean = false;
  success     : boolean;
  cnn_expi    : boolean;
  error       : boolean;
  message     : string;

  form : FormGroup;
  constructor(  private sbusiness   : BusinessService,
                private ssale       : SaleService,
                private fb          : FormBuilder,
                private svalidator  : ValidatorsService,
                private spinner     : NgxSpinnerService,
                public dialogRef    : MatDialogRef<FacturacionSunatComponent>,
                @Optional() @Inject( MAT_DIALOG_DATA ) public data : any ) {

    this.create_form();
    this._listTipoComprobante();

  }

  ngOnInit(): void {}

  get idtipocomprobanteInvalid(){
    return this.svalidator.control_invalid('idtipocomprobante', this.form);
  }

  get serieInvalid(){
    return this.svalidator.control_invalid('serie', this.form);
  }

  get numberInvalid(){
    return this.svalidator.control_invalid('numero', this.form);
  }

  get descriptionInvalid(){
    return this.svalidator.control_invalid('descripcion', this.form);
  }

  get tipoimpresionInvalid(){
    return this.svalidator.control_invalid('tipoimpresion', this.form);
  }

  get tamanioInvalid(){
    return this.svalidator.control_invalid('tamanio', this.form);
  }
  

  create_form(){

    this.form = this.fb.group({

      idtalonario       : [ 0 ],
      idtipocomprobante : [ '', [ Validators.required ] ],
      serie             : [ '', [ Validators.required, Validators.minLength(4), Validators.maxLength(4) ] ],
      numero            : [ '', [ Validators.required ] ],
      descripcion       : [ '', [ Validators.required ] ],
      tipoimpresion     : [ '', [ Validators.required ] ],
      tamanio           : [ '', [ Validators.required ] ],
      idsucursal        : [ 0 ],
      editar            : [ false ]

    });
  }

  loadForm( idtipocomprobante : number ){

    this.form.patchValue({

      idtalonario       :  this.data.idtalonario,
      idtipocomprobante :  String(idtipocomprobante),
      serie             :  this.data.Serie,
      numero            :  this.data.Numero,
      descripcion       :  this.data.Alias,
      tipoimpresion     :  this.data.TipoTamanio,
      tamanio           :  this.data.Tamanio,
      idsucursal        :  this.data.IDSucursal,
      editar            :  true

    });

  }

  _listTipoComprobante(){

    this.initialize();
    this.ssale.listTipoComprobante().subscribe( response => {

      this.listTipoComprobante = response;

      if(this.data){

        if( this.data.editTal ){
          const idtipocomprobante = this.listTipoComprobante.find( (x : any)=>x.NombreComprobante === this.data.TipoComprobante).Clave ?? 0;
          this.loadForm(idtipocomprobante);
        }
        
      }

      this.spinner.hide();

    }, (error) => this._exeception( error ) );

  }

  SaveTalonario(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data( this.form );
    }

    this.initialize();

    const body = {
      ... this.form.value
    };

    body.numero = String(body.numero);
    this.sbusiness.SaveTalonario( body ).subscribe( (response : any ) => {

      if( response.message === 'exito' ){
        this.success = true;
        this._clear();
        this.dialogRef.close({ exito: true });
      }
      
      this.spinner.hide();

    }, (error)=> this._exeception( error ) );

  }


  _clear(){

    this.form.reset({

      idtalonario       : 0,
      idtipocomprobante : '',
      serie             : '',
      numero            : '',
      descripcion       : '',
      tipoimpresion     : '',
      tamanio           : '',
      idsucursal        : 0,
      editar            : false

    });

  }

  initialize() {
    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _exeception( error:any ){
    
    console.log(error);
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  validateKey(event, limite ) {
    
    if(!this.svalidator.validateKey( event, limite ))return false;
    return true;

  }

}
