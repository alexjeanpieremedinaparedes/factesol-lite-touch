import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
import { BusinessService } from 'src/app/services/business.service';
import { FunctionsService } from 'src/app/services/functions.service';
import { ModalNewUserComponent } from '../../../usuarios/modalusuario/modal-new-user/modal-new-user.component';
import { FacturacionSunatComponent } from '../../facturacion-sunat/facturacion-sunat.component';

@Component({
  selector: 'app-asignarnuevousuario',
  templateUrl: './asignarnuevousuario.component.html',
  styles: [
  ]
})
export class AsignarnuevousuarioComponent implements OnInit {

  idsUsers    : any[]  = [];
  idUser      : number;
  success     : boolean;
  cnn_expi    : boolean;
  error       : boolean;
  message     : string;
  listuser    : any[] = [];
  listAsignada: any[]  = [];
  listAlt     : any[] = [];

  constructor(  public dialog     : MatDialog,
                private sbusiness : BusinessService,
                private auth      : AuthService,
                private spinner   : NgxSpinnerService,
                private sfunction : FunctionsService,
                public dialogRef  : MatDialogRef<FacturacionSunatComponent>,
                @Inject( MAT_DIALOG_DATA ) public data : any ) {
    this.idsUsers = data.idusuario
    this.listAsignada = this.idsUsers;
    this.listUsers();
  }

  ngOnInit(): void {}

  listUsers(){

    this.initialize();
    this.auth.listUsers().subscribe( response => {
      
      this.listuser = response;
      this.listAlt = response;
      this.spinner.hide();
      
    }, (error)=> this._exeception( error ));
  }

  AsignarUsuarioTalonario(){

    this.initialize();
    const validator = [ undefined, null, 0 ];

    if( validator.includes(this.data.idTalonario) || this.listAsignada.length === 0 ){
      this.error = true;
      this.message = 'Operacion incorrecta';
      return;
    }
    let data : any[] = [];

    this.listAsignada.forEach( el =>{

      const body = {
        idtalonario  : this.data.idTalonario,
        idusuario    : el
      }
      data.push( body );
    } )

    const body = {
      datos: data
    };

    this.sbusiness.TalonarioAsignarUsuario( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){
        this.success = true;
        this.dialogRef.close({ exito: true });
      }
      
      this.spinner.hide();

    }, ( error ) => this._exeception( error ) )

  }

  initialize() {
    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  onChange(value: number, checked: boolean, i:number) {

    if (checked) this.listAsignada.push( value );
    else{
      let index = this.listAsignada.indexOf( value );
      this.listAsignada.splice( index, 1 );
    }
  }

  _search( text:string ){

    this.listuser = this.listAlt;
    this.listuser = this.sfunction._search( this.listuser, 'Usuario', text ); 
  }

  _newuser(){
    this.dialog.open( ModalNewUserComponent )
                .afterClosed()
                .subscribe( response =>{

                  if( response.exito ){

                    this.success = true;
                    this._changeExito();
                    this.refresh();
                  }

                } )
  }

  refresh() {
    this.auth.listUsers().subscribe( response => {
      this.listuser = response;
      this.listAlt = response;
      this.spinner.hide();
    }, (error) => this._exeception( error ) );
  }

  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }


}
