import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AsignarComponent } from '../modalfacturacion/asignar/asignar.component';
import { AsignarnuevousuarioComponent } from '../modalfacturacion/asignarnuevousuario/asignarnuevousuario.component';
import { NuevotalonarioComponent } from '../modalfacturacion/nuevotalonario/nuevotalonario.component';
import { BusinessService } from '../../../../services/business.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-facturacion-sunat',
  templateUrl: './facturacion-sunat.component.html'
})
export class FacturacionSunatComponent implements OnInit {

  dateCertificado : string;
  idTalonario : number;
  delete: boolean;
  success   : boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;
  list: any[] = [];
  idUsuario : number;
  listTalonarioUsuario: any[] = [];

  constructor(  public dialog     : MatDialog,
                private sbusiness : BusinessService,
                private spinner   : NgxSpinnerService ) {

    this.listTalonario();
    const fecha = JSON.parse(localStorage.getItem('_token_login')).datecertificado;
    this.idUsuario = JSON.parse(localStorage.getItem('_token_login')).clave;
    this.dateCertificado = fecha;

  }

  listTalonario(){

    this.initialize();
    this.sbusiness.listTalonario().subscribe( response => {
      
      this.list = response;
      this.list.forEach(element =>{

        element.Usuarios.forEach(eU => {

          if(eU.IDUsuario == this.idUsuario){
            this.listTalonarioUsuario.push(element.IDTalonario);
          }

        });

      });
      
      this.spinner.hide();

    }, (error) => this._exeception( error ) );

  }

  refresh() {

    this.spinner.show();
    this.sbusiness.listTalonario().subscribe( response => {

      this.list = response;

      this.list.forEach(element =>{

        element.Usuarios.forEach(eU => {

          if(eU.IDUsuario == this.idUsuario){

            this.listTalonarioUsuario.push(element.IDTalonario);
          }
        });
      });

      this.spinner.hide();

    }, (error) => this._exeception( error ) );
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  initialize() {
    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  ngOnInit(): void {}

  _AgregarTalonario( edit : boolean = false, id : number = 0 ){

    let body;
    this.list.filter( x=>x.IDTalonario === id ).map( response =>{
      body = {
        editTal         : true,
        IDSucursal      : response.IDSucursal,
        idtalonario     : response.IDTalonario ,
        TipoComprobante : response.NombreComprobante,
        Alias           : response.Descripcion,
        Serie           : response.Serie,
        Numero          : response.Numero,
        Tamanio         : response.Tamanio,
        TipoTamanio     : response.Tipo
      }

    });


    this.dialog.open(NuevotalonarioComponent, {
      data: body
    })
    .afterClosed()
    .subscribe( (response) => {

      if( response.exito ){

        this.success = true;
        if( edit ) this.message = 'El registro se modifico de manera correcta';
        this._changeExito();
        this.refresh();
      }
    });

  }

  _Asignarsucursal( idTalonario : number, IDSucursal : number ){
    this.dialog.open(AsignarComponent, {
      data : {
        idTalonario : idTalonario,
        idSucursal : IDSucursal
      }
    })
    .afterClosed()
    .subscribe( response =>{

      if( response.exito ){
        this.success = true;
        this.message = 'Sucursal asignada correctamente';
        this._changeExito();
        this.refresh();
      }

    })
  }

  _AsignarUserTalonario( idTalonario : number, idsUser :any[]  ){

    this.dialog.open(AsignarnuevousuarioComponent, {
      data : {
        idTalonario : idTalonario,
        idusuario : idsUser.map( x=>x.IDUsuario ),
      }
    })
    .afterClosed()
    .subscribe( response =>{

      if( response.exito ){

        this.listTalonarioUsuario.includes( idTalonario );
        this.success = true;
        this.message = 'Usuario asignado correctamente';
        this._changeExito();
        this.refresh();
        
      }

    })
  }

  showdelete( idTalonario : number ){

    this.idTalonario = idTalonario;
    this.delete = !this.delete;
    this.message = '¿Eliminar talonario?';

  }

  deleteTalonario( $event ){

    this.initialize();
    if( !$event ) return;
    const body = {
      idtalonario : this.idTalonario
    }

    this.sbusiness.DeleteTalonario( body ).subscribe( (response : any) =>{

      if( response.message === 'exito' ){
        this.success = true;
        this.message = 'Registro eliminado con exito';
        this.refresh();
        this._changeExito();
      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) );

  }
  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }

}