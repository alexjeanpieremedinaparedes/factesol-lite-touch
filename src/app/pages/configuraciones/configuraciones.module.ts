
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DemoMaterialModule } from 'src/app/material-module';
import { MicuentaComponent } from './micuenta/micuenta.component';
import { UsuariosComponent } from './usuarios/usuarios/usuarios.component';
import { ImpresionComponent } from './impresion/impresion.component';
import { VentaComponent } from './venta/venta.component';
import { ConfiguracionesComponent } from './configuraciones.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { FacturacionSunatComponent } from './facturacion/facturacion-sunat/facturacion-sunat.component';
import { AsignarComponent } from './facturacion/modalfacturacion/asignar/asignar.component';
import { NuevotalonarioComponent } from './facturacion/modalfacturacion/nuevotalonario/nuevotalonario.component';
import { NuevoanexoComponent } from './facturacion/modalfacturacion/nuevoanexo/nuevoanexo.component';
import { AsignarnuevousuarioComponent } from './facturacion/modalfacturacion/asignarnuevousuario/asignarnuevousuario.component';
import { AsignarPermisosUsuariosComponent } from './usuarios/modalusuario/asignar-permisos-usuarios/asignar-permisos-usuarios.component';
import { ModalNewUserComponent } from './usuarios/modalusuario/modal-new-user/modal-new-user.component';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { CorreosComponent } from './correos/correos.component';
import { DetalleplantillaComponent } from './impresion/detalleplantilla/detalleplantilla.component';
import { PlantillaimpresionComponent } from './impresion/plantillaimpresion/plantillaimpresion.component';
import { NotlogoDirective } from 'src/app/directive/notlogo.directive';

@NgModule({
  declarations: [
    MicuentaComponent,
    UsuariosComponent,
    ImpresionComponent,
    VentaComponent,
    FacturacionSunatComponent,
    ConfiguracionesComponent,
    AsignarComponent,
    NuevotalonarioComponent,
    NuevoanexoComponent,
    AsignarnuevousuarioComponent,
    AsignarPermisosUsuariosComponent,
    ModalNewUserComponent,
    CorreosComponent,
    DetalleplantillaComponent,
    PlantillaimpresionComponent,
    NotlogoDirective
  ],

  exports: [
    MicuentaComponent,
    UsuariosComponent,
    ImpresionComponent,
    VentaComponent,
    ConfiguracionesComponent,
  ],

  imports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    DemoMaterialModule,
    SharedModule,
    ComponentsModule,
    PipesModule
  ]
})
export class ConfiguracionesModule { }
