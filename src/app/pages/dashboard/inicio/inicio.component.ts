import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  usuario:any= [];
  alertaCertificado:boolean = false;

  constructor() 
  { 
    this.usuario = JSON.parse(localStorage.getItem('_token_login')); 

    let fecha = new Date(this.usuario.datecertificado);
    const fechaActual = new Date(); 
    fecha.setDate(-15);

    if(fechaActual > fecha) this.alertaCertificado = true;
  }

  ngOnInit(): void {
  }

}
