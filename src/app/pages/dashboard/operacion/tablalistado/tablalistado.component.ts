import { Component, OnInit, Input, Output, EventEmitter, NgZone, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';

import { ModalTipocambioComponent } from '../modalsoperacion/modal-tipocambio/modal-tipocambio.component';
import { ValidatorsService } from 'src/app/services/validators.service';
import { SaleService } from 'src/app/services/sale.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ClientesComponent } from '../../mantenimiento/clientes/clientes.component';
import { FunctionsService } from 'src/app/services/functions.service';
import { IComprobante } from 'src/app/interfaces/cpe.interface';
import { take } from 'rxjs/operators';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

@Component({
  selector: 'app-tablalistado',
  templateUrl: './tablalistado.component.html',
  styleUrls: ['./tablalistado.component.css']
})
export class TablalistadoComponent implements OnInit {

  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  @Input() listTable: any[] = [];
  @Input() listUnidadM : any = [];
  @Input() totales : string;
  @Output() sendListTable = new EventEmitter<any>();
  @Output() sendClearTotal = new EventEmitter<string>();


  cpe = {} as IComprobante;

  minDate = new Date();
  typeCoin : any = 'PEN';
  importeMinCredito : number = 0;
  pipedecimal   : number = 0;
  ModalContinuar: boolean = false;  // modal continuar 01
  showModal     : boolean = false;
  irAtras       : boolean = false;
  txtBtnTotal   : string  = '+ ver mas';
  tpPago        : string;

  itemdelete    : number;
  cuotaRestante : number = 0;
  openTab       : number = 1;
  vermas        : boolean = false;
  delete        : boolean = false;
  success       : boolean = false;
  cnn_expi      : boolean;
  message       : string;
  error         : boolean;
  codSunat      : string;
  dateStart     : any = new Date();

  tabBoleta     : boolean = false;
  tabFactura    : boolean = false;
  tabNotaVenta  : boolean = false;

  guiasYotros   : boolean;
  verDetraccion : boolean;
  verDetraccionCopy : boolean;

  listTalGeneral: any[] = [];
  listTalonario : any[] = [];
  listTipoPago  : any[] = [];
  listMoneda    : any[] = [];
  listMediopago : any[] = [];
  configuration : any[] = [];


  formSeccionOne : FormGroup;
  formSecciontwo : FormGroup;


  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA ];

  listGuiaRemitente: string[] = [];

  _token_login = JSON.parse(localStorage.getItem('_token_login'));

  constructor(  public Dialog       : MatDialog,
                private fb          : FormBuilder,
                private svalidator  : ValidatorsService,
                private ssale       : SaleService,
                private spinner     : NgxSpinnerService,
                public  sfuntion    : FunctionsService,
                private _ngZone: NgZone ) {

    this.create_formSeccionOne();
    this.create_formseccionTwo();

    this.getTalonarioUser();
    this.getTipopago();
    this.getMoneda();
    this.getMedioPago();

    this.configuration = JSON.parse(JSON.parse(localStorage.getItem('_token_login')).configuration);
    this.configuration.filter( x=>x.Operacion === 'comprobante' ).map( data => {
      this.codSunat = data.valor;
    });

    this.configuration.filter( x=>x.Operacion === 'guiasYotros' ).map( data => {
      this.guiasYotros = data.valor === 'NO' ? false : true;
    });

    this.configuration.filter( x=>x.Operacion === 'verDetraccion' ).map( data => {
      this.verDetraccion = data.valor === 'NO' ? false : true;
      this.verDetraccionCopy = this.verDetraccion;
    });

    this.configuration.filter( x=>x.Operacion === 'formatoREDdetalle' ).map( data =>{
      const decimales = Number(data.valor);
      this.pipedecimal = decimales;
    });

    this.configuration.filter( x=>x.Operacion === 'importeMinCredito' ).map( data =>{
      this.importeMinCredito = Number(data.valor);
    });

    this.configuration.filter( x=>x.Operacion === 'otro' ).map( data =>{
      const value = data.valor === 'sinigv' ? false : true;
      this.formSecciontwo.patchValue({ conimpuesto : value });
    });

  }
  ngOnInit(): void {
    this.tabBoleta = true;
  }

  get serieInvalid(){
    return this.svalidator.control_invalid('serie', this.formSeccionOne);
  }

  get numberInvalid(){
    return this.svalidator.control_invalid('number', this.formSeccionOne);
  }

  get aliasSerieInvalid(){
    return this.svalidator.control_invalid('aliasSerie', this.formSeccionOne);
  }

  get nDocumentoInvalid(){
    return this.svalidator.control_invalid('nDocumento', this.formSeccionOne);
  }

  get nameClienteInvalid(){
    return this.svalidator.control_invalid('nameCliente', this.formSeccionOne);
  }

  create_formSeccionOne(){
    this.formSeccionOne = this.fb.group({

      serie           : [ '', [ Validators.required, Validators.minLength(4), Validators.maxLength(4) ] ],
      number          : [ '', [ Validators.required ] ],
      aliasSerie      : [ '', [ Validators.required ] ],
      codSunat        : [ '', [ Validators.required ] ],
      nDocumento      : [ '', [ Validators.required, Validators.minLength(8) ] ],
      nameCliente     : [ '', [ Validators.required, Validators.minLength(3) ] ],
      addresClient    : [ '' ],
      tipoimpresion   : [ '' ],

    });
    
  }

  get tipopagoInvalid(){
    return this.svalidator.control_invalid('tipopago', this.formSecciontwo);
  }

  get tipomonedaInvalid(){
    return this.svalidator.control_invalid('tipomoneda', this.formSecciontwo);
  }

  get tipocambioInvalid(){
    return this.svalidator.control_invalid('tipocambio', this.formSecciontwo);
  }

  get medioPagoInvalid(){
    return this.svalidator.control_invalid('medioPago', this.formSecciontwo);
  }
  get cuotas(){
    return this.formSecciontwo.get('cuotas') as FormArray;
  }

  create_formseccionTwo(){

    this.formSecciontwo = this.fb.group({
      tipopago    : [ '', [ Validators.required ] ],
      tipomoneda  : [ '', [ Validators.required ] ],
      tipocambio  : [ '', [ Validators.required ] ],
      ordenCompra : [ '' ],
      guiaTransp  : [ '' ],
      placa       : [ '' ],
      observacion : [ '' ],
      medioPago   : [ 1, [ Validators.required ] ],
      porcentaje  : [ '' ],
      montoDetrac : [ '' ],
      idtipomoneda: [ 0 ],
      conimpuesto : [ true ],
      cuotas      : this.fb.array([])

    });

  }

  newCuota(): FormGroup {

    return this.fb.group({
      fecha   : [ '', [ Validators.required ] ],
      monto   : [ '', [ Validators.required ] ]
    });

  }

  AgregarCuotas(){

    this.error = false;
    this.message = null;

    const totalCuota = this.returnMontoCuota();
    if( totalCuota < Number(this.cpe.total) ){
      this.cuotas.push( this.newCuota() );
    }
    else {
      this.error = true;
      this.message = 'no puede superar al monto total';
    }

  }

  quitarCuota(i:number){

    if( this.cuotas.length > 1 ){
      this.cuotas.removeAt(i);
    }

    const totalCuota = this.returnMontoCuota();
    this.cuotaRestante = totalCuota > Number(this.cpe.total) ? 0 : Number(this.cpe.total) - totalCuota;
    
  }

  getTalonarioUser(){

    const body = {
      idusuario : JSON.parse(localStorage.getItem('_token_login')).clave
    };

    this.ssale.obtenerTalonarioUsuario( body ).subscribe( response => {

      this.listTalGeneral = response;
      this.spinner.hide();

    }, ( error ) => this._exeception( error ) )

  }

  getTipopago(){

    this.ssale.listTipopago().subscribe( response =>{

      this.listTipoPago = response;
      this.spinner.hide();

    }, ( error ) => this._exeception( error ) );

  }

  getMoneda(){
    this.ssale.listMoneda().subscribe( response =>{

      this.listMoneda = response;
      this.spinner.hide();

    }, ( error ) => this._exeception( error ) );
  }

  getMedioPago(){
    this.ssale.listMedioPago().subscribe( response =>{

      this.listMediopago = response;
      this.spinner.hide();

    }, ( error ) => this._exeception( error ) );
  }

  getTipoCambioSunat(){

    const codMoneda = this.formSecciontwo.value.tipomoneda;
    this.formSecciontwo.patchValue({ tipocambio : '' });
    this.loadIdTipoCambio(codMoneda);

    const dateStart = this.sfuntion.convert_fecha( this.dateStart );
    this.initialize();
    const body = {
      fecha       : dateStart,
      aliasmoneda : codMoneda
    };

    this.ssale.listTipocambioSunat( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){

        this.formSecciontwo.patchValue({ tipocambio : response.result });
        if( this.formSecciontwo.value.porcentaje ) this.calculatePorcentaje();
      }
      else{

        this.error = true;
        this.message = response.message
      }

      this.spinner.hide();

    }, ( error ) => this._exeception( error ) );

  }

  loadIdTipoCambio( codMoneda : string ){

    this.listMoneda.filter( x=>x.Codigo === codMoneda ).map( result =>{
      this.formSecciontwo.patchValue({ idtipomoneda : result.Clave });
    })
  }

  _exeception( error:any ){
    
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  QuantityPlus(quantity: number, nameProd : string ) {
    quantity++;
    this.Modify( quantity, nameProd );
  }

  QuantityMinus(quantity: number, nameProd : string ) {
    quantity--;
    if (quantity > 0) this.Modify( quantity, nameProd );
  }

  Modify( quantity : number, nameProd:string ){

    const price =  this.listTable.find( x=> x.nameProduct === nameProd ).price;
    let dataMoficar = this.listTable.find( x=> x.nameProduct === nameProd );
    dataMoficar.quantity = quantity;
    dataMoficar.total = quantity * price;
    this.TotalGeneral();

  }

  showModalDelete( index : number ){

    this.itemdelete = index;
    this.delete = !this.delete;
    this.message = '¿ Desea quitar el item ?';
  }

  deleteItemProd( $event ){

    if( $event ){

      this.listTable.splice( this.itemdelete, 1 );
      if( this.listTable.length === 0 ) this.totales = '0.00';
      else this.TotalGeneral();

    }

  }

  TotalGeneral(){

    this.totales = this.listTable.map( x=>x.total ).reduce( ( t, a )=> t + a ) ?? 0;
    this.cpe.total = Number(this.totales) ?? 0;
  }

  continuePrinciapal(){

    if( !this.ValidatePrincipal() ) return;
    this.showModal = !this.showModal;
    this.ModalContinuar = !this.ModalContinuar;
    this.Dialog.closeAll();
    
    this.onChangeAlias( this.codSunat, true );
    const conimpuesto =  this.formSecciontwo.value.conimpuesto;
    this.amount_calculate( conimpuesto );
    if( this.formSecciontwo.value.porcentaje ) this.calculatePorcentaje();

    //loadData TipoPago
    this.loadTipopago();
  }

  irAtrasSeccion02(): void {
    this.irAtras = !this.irAtras;
  }

  onShowHide(value) {
    this.showModal = value;
    this.irAtras = !this.irAtras;
    this.ModalContinuar = !this.ModalContinuar;
  }

  continueToSeccion2( $tabNumber ){

    if( this.formSeccionOne.invalid ){
      return this.svalidator.Empty_data( this.formSeccionOne );
    }
    
    const value       = this.formSeccionOne.value.codSunat;
    const codSunat    = value === '' ? this.codSunat : value;
    if(! this.ValidateForTypeCpe( codSunat ) ) return;
    
    this.configuration.filter( x=>x.Operacion === 'moneda' ).map( data => {
      this.formSecciontwo.patchValue({ tipomoneda : data.valor });
    });
    
    //Validate Ver detraccion solo para factura
    if( this.codSunat != '01' ) this.verDetraccion = false;
    else this.verDetraccion = this.verDetraccionCopy;
    //loadData TipoPago
    this.loadTipopago();
    const moneda = String(this.formSecciontwo.value.tipomoneda).toUpperCase();
    this.onChangeMoneda(moneda);
    this.openTab = $tabNumber;
    this.irAtrasSeccion02();
  }

  ValidatePrincipal() : boolean {
    
    let detailInvalid = false;
    let quantityInvalid = false;
    if( this.listTable.length === 0 ){
      this.error = true;
      this.message = 'Llene su lista de productos';
      return false;
    }

    this.listTable.forEach(el =>{
      if(!el.nameProduct || !el.price || el.price < 0 ) detailInvalid = true;
    })

    if(detailInvalid){

      this.error = true;
      this.message = 'Verifique que los campos del detalle esten llenados correctamente';
      return false;
    }

    //ValidarCantidad
    this.listTable.forEach( el =>{
      if( !el.quantity || el.quantity <= 0 ) quantityInvalid = true;
    })

    if( quantityInvalid ){

      this.error = true;
      this.message = 'la(s) cantidad(es) no puede(n) ir en 0';
      return false;
    }

    return true;
  }

  onChangeAlias( descripcion : string, init : boolean ){

    if( init ){
      this.onClick(descripcion);
    }
    else{
      this.listTalGeneral.filter( x=>x.Descripcion === descripcion ).map( result =>{
        const codSunat = String(result.NumeroSunat)
        this.onClick(codSunat, descripcion);
      });
    }

  }

  onChangeMoneda( codMoneda : string = 'PEN' ){

    if( codMoneda === 'PEN' ){

      this.listMoneda.filter( x=>x.Codigo === codMoneda ).map( result =>{

        this.loadIdTipoCambio( codMoneda );
        this.formSecciontwo.patchValue({ tipocambio : 1 });
      })

      if( this.formSecciontwo.value.porcentaje ) this.calculatePorcentaje();
    }
    else{
      this.getTipoCambioSunat();
    }

    this.listMoneda.filter( x=>x.Codigo === codMoneda ).map( result =>{
      this.typeCoin = result.Abreviatura
    });

  }

  onChangeTipopago( clave: number ){

    this.listTipoPago.filter( x=>x.Clave === clave ).map( data =>{

      this.tpPago = String( data.TipoPago ).toUpperCase();
      if( this.tpPago === 'CREDITO' ){
        
        this.cuotaRestante = Number(this.cpe.total);
        if( Number(this.cpe.total) >= this.importeMinCredito  ){
          this.AgregarCuotas();
        }
        else {

          this.error = true;
          this.message = `El importe minimo de CREDITO es de ${ this.importeMinCredito } Soles`;
          this.cuotas.clear();
          this.tpPago = 'CONTADO';
          this.listTipoPago.filter( x=>String(x.TipoPago).toUpperCase() === this.tpPago ).map( result =>{

            this.formSecciontwo.patchValue({
              tipopago : Number(result.Clave)
            });
    
          });
        }

      }
      else {
        this.cuotas.clear();
      }

    });

  }

  ValidateLoadTipopago( clave: number ){

    this.listTipoPago.filter( x=>x.Clave === clave ).map( data =>{
      this.tpPago = String( data.TipoPago ).toUpperCase();

      if( this.tpPago === 'CREDITO' ){
        
        this.cuotaRestante = Number(this.cpe.total);
        if( Number(this.cpe.total) >= this.importeMinCredito  ){

          let cuotaLLenada = false
          const cuota : any[] =  this.formSecciontwo.value.cuotas;
          cuota.forEach( el =>{
            if(el.monto)cuotaLLenada = true;
          });
          if(!cuotaLLenada){
            this.AgregarCuotas();
          }

          let totalCuota = this.returnMontoCuota() ?? 0;
          this.cuotaRestante = Number(this.cpe.total) - totalCuota;
        }
        else {

          this.cuotas.clear();
          this.tpPago = 'CONTADO';
          this.listTipoPago.filter( x=>String(x.TipoPago).toUpperCase() === this.tpPago ).map( result =>{

            this.formSecciontwo.patchValue({
              tipopago : Number(result.Clave)
            });
    
          });
        }

      }
      else {
        this.cuotas.clear();
      }

    });
  }

  onClick( codSunat : string, descripcion : string = '' ){
    
    this.initializeTabs();
    switch( codSunat ){

      case '03':

        this.codSunat = codSunat;
        this.tabBoleta = true;
        this.listTalonario =  this.listTalGeneral.filter( x=>x.NumeroSunat === this.codSunat );

        if( !this.formSeccionOne.value.nDocumento ){

          this.formSeccionOne.patchValue({
            nDocumento  : '00000000',
            nameCliente : 'CLIENTES VARIOS',
          })
        }
        break;

      case '01':

        this.codSunat = codSunat;
        this.tabFactura = true;
        this.listTalonario =this.listTalGeneral.filter( x=>x.NumeroSunat === this.codSunat );
        break;

      default:

        this.codSunat = codSunat;
        this.tabNotaVenta = true;
        this.listTalonario = this.listTalGeneral.filter( x=>x.NumeroSunat === this.codSunat );
        break;
    }

    const toSelect = descripcion === '' ? this.listTalonario.find( x=>String(x.NumeroSunat) === codSunat ) : this.listTalonario.find( x=>String(x.NumeroSunat) === codSunat && x.Descripcion === descripcion );

    if( toSelect ){

       const configTicket = this.configuration.find(x=>x.Operacion === 'plantilla-ticket' );
       const configA4A5 = this.configuration.find(x=>x.Operacion === 'plantilla-A4|A5' );

      const PlantillaTicket = configTicket === null || configTicket === undefined ? toSelect.Tamaño : configTicket.valor;
      const PlantillaA4A5 = configA4A5 === null || configA4A5 === undefined ? toSelect.Tamaño : configA4A5.valor;

      this.formSeccionOne.patchValue({
        aliasSerie    : toSelect.Descripcion,
        codSunat      : toSelect.NumeroSunat,
        serie         : toSelect.Serie,
        number        : toSelect.Numero,
        tipoimpresion : String(toSelect.Tamaño).toUpperCase() === "TICKET" ? PlantillaTicket : PlantillaA4A5
      });
    }

  }

  initializeTabs(){
    this.tabBoleta = false;
    this.tabFactura = false;
    this.tabNotaVenta = false;

    this.formSeccionOne.patchValue({
      aliasSerie  : '',
      codSunat    : '',
      serie       : '',
      number      : '',
    });
  }

  initialize() {
    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _SearchClient(){

    const nDocumento = String(this.formSeccionOne.value.nDocumento) === 'null' ? '' : String(this.formSeccionOne.value.nDocumento);
    if( nDocumento === '' || nDocumento === '00000000' ){

      this.Dialog.open(ClientesComponent, {
        data: { dataTheDB : false }
      })
      .afterClosed()
      .subscribe( response =>{
        if( response ){
          const client = [ response.NumeroIdentificacion, response.Nombrecompleto, response.IDDireccion ]
          this.loadClient( client );
        }
      });

    }
    else {
      this.SearchService();
    }

  }

  SearchService(){

    const ndocumento  = this.formSeccionOne.value.nDocumento;
    const value       = this.formSeccionOne.value.codSunat;
    const codSunat    = value === '' ? this.codSunat : value;

    if( !ndocumento ) return;
    if(!this.ValidateForTypeCpe( codSunat )) return;

    this.initialize();
    const body = {
      numerodocumento : String(ndocumento)
    }
    this.ssale._vObtnerCliente( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){

        const result = response.result;
        const client = [ result.numerodocumento, result.cliente, result.direccion ];
        this.loadClient( client );

      }

      this.spinner.hide();

    }, ( error )=> this._exeception( error ) )

  }

  onBlurEvent(event: any){

    if( this.formSeccionOne.value.nDocumento !== '00000000' ){
      this.SearchService();
    }

  }

  SoloNumeros( $event ){

    const KEY_ALPHABET = [ 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90 ];

    if( KEY_ALPHABET.includes($event.keyCode) ){
      return false;
    }

    return true;

  }

  _showTipocambio(){

    const amount = this.formSecciontwo.value.tipocambio;
    this.Dialog.open(ModalTipocambioComponent, { 
      data: {
        fechaSelect : this.dateStart,
        amount      : amount
      }
     }).afterClosed().subscribe( response =>{

      if( response.exito ){
        this.formSecciontwo.patchValue({ tipocambio : response.tipocambio })
      }

    })
  }

  _changeExito() {

    setTimeout(() => {

      this.success = false;
      this.message = null;
      this.Dialog.closeAll();
      this.listGuiaRemitente = [];
      this.formSeccionOne.reset();
      this.formSecciontwo.reset();
      this.formSecciontwo.patchValue({
        medioPago   : 1,
        idtipomoneda: 0,
        conimpuesto : true
      });

      this.cuotas.clear();
      this.ModalContinuar= false;

    }, 2000);

  }

  add(event: MatChipInputEvent): void {

    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) this.listGuiaRemitente.push(value.trim());
    if (input) input.value = '';

  }

  remove(guia: string): void {

    const index = this.listGuiaRemitente .indexOf(guia);
    if (index >= 0) this.listGuiaRemitente.splice(index, 1);
  }

  validateKey(event, limite ) {

    if(!this.svalidator.validateKey( event, limite ))return false;
    return true;

  }

  loadClient( client : any[] ){

    this.formSeccionOne.patchValue({
      nDocumento  : client[0],
      nameCliente : client[1],
      addresClient: client[2]
    });

  }

  ValidateForTypeCpe( codSunat : string ) : boolean {

    const ndocumento  = String(this.formSeccionOne.value.nDocumento).trim();
    const clientNul = ['', '', ''];

    if( codSunat === '01' && ndocumento.length !== 11 && ndocumento.length > 0 ){

      this.error = true;
      this.message = 'ha seleccionado factura, por favor ingresar un ruc valido';
      this.loadClient(clientNul);
      return false;

    }

    if( codSunat === '03' && ndocumento.length !== 8 && ndocumento.length > 0 ){

      this.error = true;
      this.message = 'ha seleccionado boleta, por favor ingresar un dni valido';
      this.loadClient(clientNul);
      return false;

    }

    if( codSunat === '00' && ndocumento.length !== 8 && ndocumento.length > 0 ){

      this.error = true;
      this.message = 'ha seleccionado nota de venta, por favor ingresar un documento valido';
      this.loadClient(clientNul);
      return false;
    }

    return true;

  }

  amount_calculate( $event:boolean ){

    const conImpuesto = $event;
    const afecto = this.returnAmount('Afecto');
    const total = Number(this.totales);

    this.cpe.anticipo = 0;
    this.cpe.Gratuita = 0;
    this.cpe.inafecto  = this.returnAmount('Inafecto') ?? 0;
    this.cpe.exonetada = this.returnAmount('Exonerado') ?? 0;

    if( conImpuesto ){
      
      this.cpe.afecto = afecto / 1.18 ?? 0;
      this.cpe.igv = this.cpe.afecto * 0.18 ?? 0;
      this.cpe.subtotal = total;
      this.cpe.total = total;
    }
    else{

      this.cpe.afecto = afecto;
      this.cpe.igv = this.cpe.afecto * 0.18 ?? 0;
      this.cpe.subtotal = total;
      this.cpe.total = this.cpe.afecto + this.cpe.inafecto + this.cpe.exonetada + this.cpe.igv + this.cpe.anticipo;
    }

    //Calcular cuotas
    const tipopago = this.returnTipopago();
    if( tipopago === 'CREDITO' ){
      
      const totalCuota = this.returnMontoCuota();
      this.cuotaRestante = totalCuota > Number(this.cpe.total) ? 0 : Number(this.cpe.total) - totalCuota;
    }

    //Calcular porcentaje
    this.calculatePorcentaje();

  }

  returnAmount( operation : string ) : number {

    return this.listTable.filter( x=>String(x.concept).toUpperCase() === operation.toUpperCase() )
                        .map( x=>x.total )
                        .reduce( ( t, a ) => t + a, 0 ) ?? 0;
  }

  resumenvermas() {

    this.vermas = !this.vermas;
    this.txtBtnTotal = !this.vermas ? ' + ver más' : ' - ver menos';
    
  }

  returndata1( data : string ) : string {
    return this.formSeccionOne.get(data).value;
  }

  returndata2( data : string ){
    return this.formSecciontwo.get(data).value;
  }

  getdateSelect( $event ){
    this.dateStart = $event;
    this.minDate = $event;
    const moneda = String(this.formSecciontwo.value.tipomoneda).toUpperCase();
    if( moneda === 'USD' ){
      this.onChangeMoneda( moneda );
    }
  }

  returnTipopago() : string {

    const tipopago = this.listTipoPago.filter( x=>Number(x.Clave) === Number(this.formSecciontwo.value.tipopago) )
                        .map( x=>x.TipoPago );
    return tipopago === undefined || tipopago === null ? '' : String(tipopago).toUpperCase();
  }

  returnMontoCuota() : number {
    const totalcuota = this.cuotas.value.map( x=>x.monto).reduce( ( t, a ) => Number(t) + Number(a) , 0 );
    return Number( totalcuota );
  }

  SaveSale(){

    const tipopago = this.returnTipopago();
    let totalCuota =  this.returnMontoCuota();
    this.error = false;
    this.message = null;

    if( this.formSecciontwo.controls.cuotas.invalid ){
      if( tipopago === 'CREDITO' ){
        this.error = true;
        this.message = 'Verificar el ingreso de los montos y la seleccion de fecha en sus cuotas';
        return;
      }
    }

    if( this.formSecciontwo.invalid ){
      return this.svalidator.Empty_data( this.formSecciontwo );
    }

    totalCuota = Number(this.sfuntion._RoundDecimal( totalCuota, 2));
    const total = Number(this.sfuntion._RoundDecimal( this.cpe.total, 2));

    if( totalCuota !== total && tipopago === 'CREDITO' ){
      this.error = true;
      this.message = 'El total de la cuota no coincide con el total de la venta';
      return;
    }

    if( this.formSecciontwo.value.porcentaje ){

      if( this.formSecciontwo.value.medioPago === 1 ){
        this.error = true;
        this.message = 'Seleccione medio de pago para el monto detracción';
        return;
      }

    }
    
    this.initialize();
    let detalle = [];
    let cuotas  = [];
    
    const number = String(this.formSeccionOne.value.number).padStart(8, '0');
    const correlativo = `${this.returndata1('serie')}-${number}`;
    const tipocpe = this.codSunat === '01' ? 'FACTURA ELECTRÓNICA' 
                  : this.codSunat === '03' ? 'BOLETA DE VENTA ELECTRÓNICA'
                  : 'NOTA DE VENTA' ;
    
    const moneda = this.listMoneda.find( x=>x.Codigo === this.formSecciontwo.value.tipomoneda ).NombreMoneda;
    const formaPago = this.listTipoPago.find( x=>x.Clave === this.formSecciontwo.value.tipopago ).TipoPago;
    
    this.listTable.forEach( el => {

      const details = {

        idcomprobante     : correlativo,
        idproducto        : 0,
        descripcion       : el.nameProduct,
        cantidad          : el.quantity,
        precio            : el.price,
        precioreferencial : 0.00,
        TipoAfectacion    : el.concept,
        bonificacion      : false,
        unidadmedida      : el.unidadM,
        docanticipo       : "",
        icbper            : 0.00,
        idpresentacion    : el.product,

        total             : this.sfuntion._RoundDecimal(el.total, this.pipedecimal)

      }

      detalle.push( details );
    });

    const cuota : any[] =  this.formSecciontwo.value.cuotas;
    cuota.forEach( (el , i) =>{

      const cuota = {

        idcomprobante : correlativo,
        fecha         : this.sfuntion.convert_fecha(el.fecha),
        monto         : this.sfuntion._RoundDecimal(el.monto, 2),

        numerocuota   : i+1,
        moneda        : moneda,

      };

      cuotas.push( cuota );
    })
    
    const body = {

      idcomprobante: correlativo,
      idusuario: JSON.parse(localStorage.getItem('_token_login')).clave,
      idtipopago: Number(this.returndata2('tipopago')),
      idmoneda: Number(this.returndata2('idtipomoneda')),
      idmediopago: Number(this.returndata2('medioPago')),
      fecha: this.sfuntion.convert_fecha(this.dateStart),
      fechavencimiento: this.sfuntion.convert_fecha(this.dateStart),
      serie: this.returndata1('serie'),
      numero: number,
      codigocomprobante: this.returndata1('codSunat'),
      ordencompra: this.returndata2('ordenCompra') ?? '',
      tiponota: '',
      motivonota: '',
      tipocomprobanterelacionado: '',
      serierelacionada: '',
      numerorelacionada: '',
      nombrecliente: this.returndata1('nameCliente'),
      numerodocumento: this.returndata1('nDocumento'),
      tipocambio: Number(this.returndata2('tipocambio')) ?? 0,
      descuentoporcentaje: 0,
      descuentosoles: 0,
      anticipo: this.sfuntion._RoundDecimal( this.cpe.anticipo, 2)    ?? 0,
      gravada: this.sfuntion._RoundDecimal( this.cpe.afecto, 2)       ?? 0,
      gratuita: this.sfuntion._RoundDecimal( this.cpe.Gratuita, 2)    ?? 0,
      exonerada: this.sfuntion._RoundDecimal( this.cpe.exonetada, 2)  ?? 0,
      inafecta: this.sfuntion._RoundDecimal(this.cpe.inafecto, 2)     ?? 0,
      igv: this.sfuntion._RoundDecimal(this.cpe.igv, 2)               ?? 0,
      total: this.sfuntion._RoundDecimal(this.cpe.total, 2)           ?? 0,
      descripcion: this.returndata2('observacion') ?? '',
      guiaremitente: this.listGuiaRemitente.join(','),
      guiatransportista: this.returndata2('guiaTransp') ?? '',
      placa: this.returndata2('placa') ?? '',
      ordenpago: '',
      detraccionporcentaje: Number(this.returndata2('porcentaje')) ?? 0,
      detraccionmonto: Number(this.returndata2('montoDetrac')) ?? 0,
      detraccionMontoAfectacion : this.cpe.total, //Este agrege porque marco me dijo en las tardes de trello
      detraccionmontoafectado: this.cpe.total, //Preguntar a marco
      conimpuesto: Boolean(this.returndata2('conimpuesto')), //Con igv true, y sin igv false
      icbper: 0,
      detalle: detalle,
      cuota : cuotas,

      //print
      direccioncliente: this.returndata1('addresClient') ?? '',
      serienumero: correlativo,
      usuario: this._token_login.user,
      ruc: this._token_login.ruc,
      empresa: this._token_login.empresa,

      direccionempresa: this._token_login.direccion,
      direccionanexo: this.listTalGeneral.find( x=>x.Serie === this.returndata1('serie') ).Sucursal ?? '', 
      telefonoempesa: this._token_login.telefonoempresa,
      correoempresa: this._token_login.correoempresa,
      tipocomprobante: tipocpe,
      formapago: formaPago,
      moneda: moneda,
      subtotal: this.sfuntion._RoundDecimal( this.cpe.subtotal, 2) ?? 0,
      logo: this._token_login.logo,
      tipoimpresion : this.returndata1('tipoimpresion') ?? 'A4',
      detraccionnumerocuenta : this._token_login.numeroCuenta ?? '',
    };

    this.ssale.SaveSale( body ).subscribe( ( response : any ) => {

      if( response.message === 'exito' ){
        
        this.getTalonarioUser();
        this.ImprimirVenta(body);
        //Limpiar
        this.success = true;
        this.showModal = false;
        this.ModalContinuar = false;
        this.openTab = 1;
        this.Dialog.closeAll();
        this.listTable = [];
        this.totales = '0';
        this.sendListTable.emit(this.listTable);
        this.sendClearTotal.emit(this.totales);
        this._changeExito();
      }

    }, (error) => this._exeception( error ) );
  }

  ImprimirVenta( body: any ){

    try {

      this.ssale._printSale( body ).subscribe( (response : any ) =>{
        
        if( response.message === 'exito' ){
  
          const bs64File = response.result;
          this.sfuntion.printDocument(bs64File);
        }
        this.spinner.hide();
  
      }, ( error ) => this._exeception( error ) );

    } catch (error) {

      this._exeception( error );
    }


  }

  onBlurCuotaRestante( index : number ){

    let totalCuota = this.returnMontoCuota();
    if( totalCuota > Number(this.cpe.total) ){

      if( this.cuotas.length === 1 ){
        this.cuotas.at(index).get('monto').setValue( this.sfuntion._RoundDecimal( Number(this.cpe.total), 2));
      }
      else {

        this.cuotas.at(index).get('monto').setValue(0);
        const totalCuota = this.returnMontoCuota();
        const monto = this.sfuntion._RoundDecimal(Number(this.cpe.total) - totalCuota, 2);
        this.cuotas.at(index).get('monto').setValue(monto);
      }

      this.cuotaRestante = totalCuota > Number(this.cpe.total) ? 0 : Number(this.cpe.total) - totalCuota;
    }
    else this.cuotaRestante = Number(this.cpe.total) - totalCuota;

  }

  onfocus( string : string ) {
    this.sfuntion.onFocus( string );
  }

  calculatePorcentaje( ){

    const PorcentajeDetracion = Number(this.formSecciontwo.value.porcentaje) ?? 0;
    const tipoCambio = Number(this.formSecciontwo.value.tipocambio) ?? 0;
    const TotalVenta = Number(this.cpe.total);
    const montoDetraccion = (TotalVenta  * PorcentajeDetracion / 100 *  tipoCambio).toFixed(2);

    this.formSecciontwo.patchValue({
      montoDetrac : montoDetraccion
    });
    //monto = TotalVenta * PorcentajeDetracion / 100 * tipoCambio
  }

  loadTipopago(){

    const tPagoToselect = this.formSecciontwo.value.tipopago

    if( !tPagoToselect ){

      this.configuration.filter( x=>x.Operacion === 'tipopago' ).map( data => {

        let cuotaLLenada = false
        const cuota : any[] =  this.formSecciontwo.value.cuotas;
        cuota.forEach( el =>{
          if(el.monto) cuotaLLenada = true;
        })
  
        if(!cuotaLLenada) this.cuotas.clear();
        const valortipopago = String(data.valor) === 'contado' ? 1 : 2;
        this.tpPago = String(data.valor).toUpperCase();
        this.formSecciontwo.patchValue({ tipopago : valortipopago });
        this.ValidateLoadTipopago(valortipopago);
      });
    }
    
  }

  onChangeDescriptionDetails( description : string, index : number ){
    this.listTable[index].nameProduct = description.trim();
  }

  onChangePriceDetails( price : number, index ){
    
    const quantity = this.listTable[index].quantity;
    this.listTable[index].price = price;
    this.listTable[index].total = price * quantity;
    if( price < 0 ) this.listTable[index].total = 0;
    this.TotalGeneral();
  }

  onchangeConcepto( concepto : string, index ){
    this.listTable[index].concept = concepto;
  }

  onchangeUnidadMedida( concepto : string, index ){
    this.listTable[index].unidadM = concepto;
  }

  triggerResize() {
    this._ngZone.onStable.pipe(take(1)).subscribe(() => this.autosize.resizeToFitContent(true));
  }

  _changeQuantity( quantity : number, index : number ){
    
    this.listTable[index].quantity = quantity;
    const nameProduct = this.listTable[index].nameProduct;
    this.Modify( quantity, nameProduct );
  }

}