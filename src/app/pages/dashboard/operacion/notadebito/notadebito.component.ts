import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { typeNota } from 'src/app/interfaces/typeNota.interface';
import { SaleService } from 'src/app/services/sale.service';

@Component({
  selector: 'app-notadebito',
  templateUrl: './notadebito.component.html'
})
export class NotadebitoComponent implements OnInit {

  listType : Observable<typeNota[]>;
  constructor( ssale : SaleService ) {
    this.listType = ssale.getTypeNotaDebito();
  }

  ngOnInit(): void {
  }

}
