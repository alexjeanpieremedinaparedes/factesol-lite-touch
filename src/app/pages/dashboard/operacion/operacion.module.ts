import { ModalClientesComponent } from './modalsoperacion/modal-clientes/modal-clientes.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VentanormalComponent } from './ventanormal/ventanormal.component';
import { VentarapidaComponent } from './ventarapida/ventarapida.component';
import { ModalAgregaranticipoComponent } from './modalsoperacion/modal-agregaranticipo/modal-agregaranticipo.component';
import { ModalTipocambioComponent } from './modalsoperacion/modal-tipocambio/modal-tipocambio.component';
import { ModalsNuevoclienteComponent } from './modalsoperacion/modals-nuevocliente/modals-nuevocliente.component';
import {
  NotacreditoComponent,
  modalvermasinformacion,
} from './notacredito/notacredito.component';
import { NotadebitoComponent } from './notadebito/notadebito.component';
import { TablalistadoComponent } from './tablalistado/tablalistado.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DemoMaterialModule } from 'src/app/material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { VentanormaltouchComponent } from './ventanormaltouch/ventanormaltouch.component';

@NgModule({
  declarations: [
    VentanormalComponent,
    VentarapidaComponent,
    ModalClientesComponent,
    ModalAgregaranticipoComponent,
    ModalTipocambioComponent,
    ModalsNuevoclienteComponent,
    NotacreditoComponent,
    modalvermasinformacion,
    NotadebitoComponent,
    TablalistadoComponent,
    VentanormaltouchComponent
  ],

  imports: [
    CommonModule,
    ComponentsModule,
    SharedModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    TrimValueAccessorModule,
    ScrollingModule 
  ],
})
export class OperacionModule {}
