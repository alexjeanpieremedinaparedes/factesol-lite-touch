import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { Component, OnInit,NgZone, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/services/functions.service';
import { ProductsService } from 'src/app/services/products.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-ventarapida',
  templateUrl: './ventarapida.component.html',
  styleUrls: ['./ventarapida.component.css']
})
export class VentarapidaComponent implements OnInit {
  
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  success   : boolean;
  cnn_expi  : boolean;
  error     : boolean;
  message   : string;

  configuration : any[] = [];
  listPresent   : any[] = [];//
  listUnidadM   : any[] = [];
  listTable     : any[] = [];
  totales       : string = '0';

  formPrincipal : FormGroup;

  constructor(  private sproduct  : ProductsService,
                private spinner   : NgxSpinnerService,
                private fb        : FormBuilder,
                private svalidator: ValidatorsService,
                private sfunction : FunctionsService,
                private _ngZone: NgZone ) {
    this._listPresentations();
    this._listUnidadMedida();
    this.create_form();

    this.configuration = JSON.parse(JSON.parse(localStorage.getItem('_token_login')).configuration);
    this.configuration.filter( x=>x.Operacion === 'tipoafectacion' ).map( data => {
      this.formPrincipal.patchValue({ concept : sfunction.capitalice(data.valor)});
    } )
    
  }

  ngOnInit(): void {}

  get productInvalid(){
    return this.svalidator.control_invalid('product', this.formPrincipal)
  }

  get nameproductInvalid(){
    return this.svalidator.control_invalid('nameProduct', this.formPrincipal)
  }

  get unidadMInvalid(){
    return this.svalidator.control_invalid('unidadM', this.formPrincipal)
  }

  get quantityInvalid(){
    return this.svalidator.control_invalid('quantity', this.formPrincipal)
  }

  get priceInvalid(){
    return this.svalidator.control_invalid('price', this.formPrincipal)
  }

  get conceptInvalid(){
    return this.svalidator.control_invalid('concept', this.formPrincipal)
  }

  create_form(){

    this.formPrincipal = this.fb.group({
      product     : [ '', [ Validators.required ] ],
      nameProduct : [ '', [ Validators.required, Validators.minLength(3) ] ],
      unidadM     : [ '', [ Validators.required ] ],
      quantity    : [ '', [ Validators.required, Validators.min(0) ] ],
      price       : [ '', [ Validators.required, Validators.min(0) ] ],
      concept     : [ '', [ Validators.required ] ]
    });

  }

  _listPresentations(){
    this.sproduct.listPresentation().subscribe( response => {
      
      this.listPresent = response.filter(x=>x.VentaRapida === true);
      this.configuration.filter( x=>x.Operacion === 'presentacion' ).map( data =>  {
        const id = this.listPresent.find( x=>x.ID === Number(data.valor) );
        this.formPrincipal.patchValue({ product : id === undefined ? '' : id.ID });
      })
      this.spinner.hide();

    }, ( error )=> this._exeception( error ))
  }

  _listUnidadMedida(){
    this.sproduct.listUnidadMedida().subscribe( response =>{

      this.listUnidadM = response;
      this.loadUidadMedida();
      this.spinner.hide();
      
    }, ( error ) => this._exeception( error ) )
  }

  loadUidadMedida(){
    const Abreviatura = this.listUnidadM.map( x=>x.Abreviatura );
    if( Abreviatura ) this.formPrincipal.patchValue( { unidadM : Abreviatura[0] } )
  }

  AddItem(){
    
    if( this.formPrincipal.invalid ){
      return this.svalidator.Empty_data( this.formPrincipal )
    }
    
    const quantity = Number( this.formPrincipal.value.quantity)
    const price = Number(this.formPrincipal.value.price);
    const total = quantity * price;

    const item = {
      ... this.formPrincipal.value,
      total   : total
    };

    item.nameProduct = String(item.nameProduct).trim();
    this.listTable.push(item);
    const totales : number = this.listTable.map( x=>x.total).reduce( ( t, a ) => t + a , 0 );
    this.totales = this.sfunction._RoundDecimal( totales , 2 );
    this.onFocus('btnSubmitFocus');
    this._clear();

  }


  _clear(){
    
    this.formPrincipal.patchValue({

      nameProduct : '',
      unidadM     : '',
      quantity    : '',
      price       : ''

    });
    
    this.loadUidadMedida();
    this.formPrincipal.markAsPristine();
    this.formPrincipal.markAsUntouched();
    this.formPrincipal.updateValueAndValidity();

  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  validateKey(event, limite ) {

    if(!this.svalidator.validateKey( event, limite ))return false;
    return true;

  }

  onFocus( focus:string ){
    document.getElementById(focus).focus();
  }

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }

}