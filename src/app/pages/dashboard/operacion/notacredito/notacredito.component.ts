import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { IComprobante } from 'src/app/interfaces/cpe.interface';
import { typeNota } from 'src/app/interfaces/typeNota.interface';
import { SaleService } from 'src/app/services/sale.service';

@Component({
  selector: 'app-notacredito',
  templateUrl: './notacredito.component.html'
})
export class NotacreditoComponent implements OnInit {

  listType : Observable<typeNota[]>;
  constructor( ssale : SaleService ) {
    this.listType = ssale.getTypeNotaCredito();
  }
  ngOnInit(): void { }

}


@Component({
  selector: 'app-modalvermas',
  templateUrl: './modalvermasinformacion.html',
})
export class modalvermasinformacion implements OnInit {

  data = {} as IComprobante;
  constructor( @Inject( MAT_DIALOG_DATA ) datos : any ){

    this.data = datos.data

    console.log(this.data)
    
  }

  ngOnInit(){}

}
