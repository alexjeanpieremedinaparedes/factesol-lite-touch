import { Component, OnInit } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-ventanormaltouch',
  templateUrl: './ventanormaltouch.component.html',
  styleUrls: ['./ventanormaltouch.component.css']
})
export class VentanormaltouchComponent implements OnInit {

  VerFiltroTresPuntos:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  listGuiaRemitente: string[] = [];
  separatorKeysCodes: number[] = [ENTER, COMMA ];
  selectable = true;
  removable = true;

  add(event: MatChipInputEvent): void {

    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) this.listGuiaRemitente.push(value.trim());
    if (input) input.value = '';

  }

  remove(fruit: string): void {
    const index = this.listGuiaRemitente.indexOf(fruit);

    if (index >= 0) {
      this.listGuiaRemitente.splice(index, 1);
    }
  }
  MostrarFiltroTresPuntos(){
    if(this.VerFiltroTresPuntos) this.VerFiltroTresPuntos = false;
    else this.VerFiltroTresPuntos = true;
  }


  // pasos 1
  public modalspasos = 1;
  toggleTabs(tabNumber: number){
    this.modalspasos = tabNumber;
  }


  // paso 2
  public pasotabsgenerales = 1;
  toggleTab( tabNumbers: number){
    this.pasotabsgenerales = tabNumbers;
  }  

}
