import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ModalsNuevoclienteComponent } from '../modals-nuevocliente/modals-nuevocliente.component';


@Component({
  selector: 'app-modal-clientes',
  templateUrl: './modal-clientes.component.html'
})
export class ModalClientesComponent implements OnInit {

  constructor(public Dialog:MatDialog) { }

  // modalnuevocliente
  nuevoCliente() {
    this.Dialog.open(ModalsNuevoclienteComponent);
  }

  ngOnInit(): void {
}


}
