import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-modals-nuevocliente',
  templateUrl: './modals-nuevocliente.component.html'
})
export class ModalsNuevoclienteComponent implements OnInit {

  success: boolean = false;
  mensaje: string = "Registro un nuevo cliente Exitosamente";

  modalguradar() {
    this.success = !this.success;
    this.Settimeout_msexito();

  }

  Settimeout_msexito() {
    setTimeout(() => {
      this.Dialog.closeAll();
      this.success = false;
    }, 2000);

  }
  
  // imput guia remitente
  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  guiaremitente = new FormControl();
  guia: string[] = ['marcoantonio@gmail.com', 'alexparedes@gmail.com'];

  @ViewChild('guiaInput') guiaInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  public cerrarFormulario: boolean;

  constructor(public Dialog: MatDialog) {}

  onShowHide(value) {
    this.cerrarFormulario = value;
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our guia
    if ((value || '').trim()) {
      this.guia.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.guiaremitente.setValue(null);
  }

  remove(guia: string): void {
    const index = this.guia.indexOf(guia);

    if (index >= 0) {
      this.guia.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.guia.push(event.option.viewValue);
    this.guiaInput.nativeElement.value = '';
    this.guiaremitente.setValue(null);
  }

  ngOnInit(): void { }

}
