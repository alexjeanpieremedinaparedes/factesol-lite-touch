import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidatorsService } from '../../../../../services/validators.service';
import { SaleService } from '../../../../../services/sale.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TablalistadoComponent } from '../../tablalistado/tablalistado.component';
import { FunctionsService } from 'src/app/services/functions.service';

import * as moment from 'moment';

@Component({
  selector: 'app-modal-tipocambio',
  templateUrl: './modal-tipocambio.component.html',
  styles: [
  ]
})
export class ModalTipocambioComponent implements OnInit {

  success: boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;

  form : FormGroup;
  constructor(  private fb : FormBuilder,
                private svalidator : ValidatorsService,
                private ssale      : SaleService,
                private spinner    : NgxSpinnerService,
                public sfunction  : FunctionsService,
                public dialogRef   : MatDialogRef<TablalistadoComponent>,
                @Inject( MAT_DIALOG_DATA ) public data : any  ) {

    this.create_form( data.fechaSelect );
    this.form.patchValue({ tipocambio : data.amount === '' ? '' : Number(data.amount) })

  }
  ngOnInit(): void {}

  get fechaInvalid(){
    return this.svalidator.control_invalid('fecha', this.form);
  }

  get tipocambioInvalid(){
    return this.svalidator.control_invalid('tipocambio', this.form);
  }

  create_form( date : string ){

    date = moment(date).format('DD/MM/YYYY');
    this.form = this.fb.group({
      fecha       : [ date, Validators.required ],
      tipocambio  : [ '' , Validators.required ]
    });

  }

  SaveTipocambio(){
    
    if( this.form.invalid ){
      return this.svalidator.Empty_data( this.form );
    }

    this.initialize();

    const body = {
      ... this.form.value
    };
    
    body.fecha = this.sfunction.convert_fecha(this.data.fechaSelect);
    this.ssale.SaveTipoCambio( body ).subscribe( (response : any ) =>{
      
      if( response.message === 'exito' ){
        this.success = true;
        this.dialogRef.close({
          exito : true,
          tipocambio : this.form.value.tipocambio
        })
      }

      this.spinner.hide();

    }, ( error )=> this._exeception( error ) );

  }

  initialize() {
    this.spinner.show();
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _exeception(error: any) {
    console.log(error);
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  validateKey(event, limite ) {

    if(!this.svalidator.validateKey( event, limite ))return false;
    return true;

  }

}
