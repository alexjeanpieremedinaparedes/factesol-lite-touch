import { Component, HostListener, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { IPresentation } from 'src/app/interfaces/presentation.interface';
import { FunctionsService } from 'src/app/services/functions.service';
import { ProductsService } from 'src/app/services/products.service';

export interface State {
  flag: string;
  name: string;
  population: string;
}

@Component({
  selector: 'app-ventanormal',
  templateUrl: './ventanormal.component.html'
})
export class VentanormalComponent implements OnInit {

  listTable    : any[]  = [];
  listUnidadM  : any[]  = [];
  totales      : string = '0';

  delete    : boolean;
  idClient  : number;
  success   : boolean;
  cnn_expi  : boolean;
  error     : boolean;
  message   : string;
  newClient : boolean;

  listadoautocompletado = false;
  listPresentation     : IPresentation [] = [];
  listPresentationCopy : IPresentation [] = [];

  constructor(  private sProduct  : ProductsService,
                private spinner   : NgxSpinnerService,
                public  sfunction : FunctionsService
                 ) {

    this._listUnidadMedida();
    this._lispresentation();
  }

  ngOnInit() {
    //Focus
    this.sfunction.onFocus("focusBuscar");
  }

  _lispresentation(){

    this.initialize();
    this.sProduct.listPresentation().subscribe( response =>{

      this.listPresentation = response;
      this.listPresentation.forEach( el =>{ el.QuantitySale = 1 } );
      this.listPresentationCopy = this.listPresentation;
      this.spinner.hide()

    }, (error) => this._exeception(error) );

  }

  _listUnidadMedida(){

    this.sProduct.listUnidadMedida().subscribe( response =>{
      this.listUnidadM = response;
    }, ( error ) => this._exeception( error ) )
  }

  onAbrirAutoCompletado() {
    this.listadoautocompletado = !this.listadoautocompletado;
  }

  QuantityPlus(quantity:number, id:number ) {
    quantity++;
    this.Modify( quantity, id );
  }

  QuantityMinus(quantity: number, id:number ) {
    quantity--;
    if (quantity > 0) this.Modify( quantity, id );
  }

  Modify( quantity : number, id:number ){

    let dataUpdate = this.listPresentation.find( x=>x.ID === id  );
    dataUpdate.QuantitySale = quantity;
  }

  _searchPresentation( search : string ){
    
    this.listPresentation = this.listPresentationCopy;
    let list = this.sfunction._search( this.listPresentation, 'Producto', search );
    if( list.length === 0 ) list = this.sfunction._search( this.listPresentation, 'Presentacion', search );
    this.listPresentation = list;

    if(search.length > 0) this.listadoautocompletado = true;
    else this.listadoautocompletado = false;

    console.log(search.length);
  }
  
  _addList( item:IPresentation ){

    //Total General
    const price = item.Precio;
    const quantity = item.QuantitySale;
    const total = quantity * price;
    const totalGeneral = Number(this.totales) + total;
    this.totales = this.sfunction._RoundDecimal(totalGeneral, 2)

    //add list table
    const body = {
      product     : item.ID,
      nameProduct : item.Producto,
      unidadM     : item.Abreviatura,
      quantity    : item.QuantitySale,
      price       : item.Precio,
      concept     : item.Afectacion,
      total       : total
    };

    this.listTable.push(body);

    //reset
    this.listPresentation.forEach(el => { el.QuantitySale = 1 })
    this.listadoautocompletado = !this.listadoautocompletado;

    //Focus
    // document.getElementById('focusBuscar');
    this.sfunction.onFocus("focusBuscar");
  }

  initialize() {

    this.spinner.show();
    this.success   = false;
    this.cnn_expi  = false;
    this.error     = false;
    this.message   = null;
    this.totales   = '0';

    this.listPresentation     = [];
    this.listPresentationCopy = [];

  }

  _exeception(error: any) {
    console.log(error);
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {

    const _VALIDATE = [ 'input', 'td', 'th', 'svg', 'rect', 'button', 'fieldset' ];
    const OpenModal = _VALIDATE.includes(event.target.localName)
    // console.log(event.target.localName)
    if( !OpenModal ) this.listadoautocompletado = false;
  }

}