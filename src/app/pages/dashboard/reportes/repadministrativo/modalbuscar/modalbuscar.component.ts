import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICondition, IStatus } from 'src/app/models/report.model';
import { RepadministrativoComponent } from '../repadministrativo.component';

@Component({
  selector: 'app-modalbuscar',
  templateUrl: './modalbuscar.component.html'
})
export class ModalbuscarComponent implements OnInit {

  icondition : ICondition;
  istatus    : IStatus;

  constructor(  private spinner   : NgxSpinnerService,
                private dialogRef : MatDialogRef<RepadministrativoComponent>,
                @Inject( MAT_DIALOG_DATA ) data : any ) {
    
    this.icondition = data.icondition;
    this.istatus = data.istatus;
  }

  ngOnInit(): void {}
  
  _changeCondition( name : string, checked ){

    this.icondition[name] = checked;
    const keys = Object.keys( this.icondition );
    const next = keys.filter( x=>x !== name )[0];
    
    if( !this.icondition.activo && !this.icondition.anulado ){
      this.icondition[next] = true;
    }

  }
  _changeStatus( name : string, checked ){
    this.istatus[name] = checked;
    const keys = Object.keys( this.istatus );
    const next = keys.filter( x=>x !== name )[0];
    
    if( !this.istatus.enviado && !this.istatus.not_enviado ){
      this.istatus[next] = true;
    }
  }

  _reestablecer(){

    this.icondition.activo    = true;
    this.icondition.anulado   = true;

    this.istatus.enviado      = true;
    this.istatus.not_enviado  = true;
  }
  
  _aplicar(){

    this.spinner.show();
    const data = Object.assign( this.icondition, this.istatus )
    this.dialogRef.close({
      data : data
    })
  }


}