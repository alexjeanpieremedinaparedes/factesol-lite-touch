import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { IComprobante } from 'src/app/interfaces/cpe.interface';
import { SaleService } from 'src/app/services/sale.service';

@Component({
  selector: 'app-vercomprobante',
  templateUrl: './vercomprobante.component.html',
  styleUrls: ['./vercomprobante.component.css']
})
export class VercomprobanteComponent implements OnInit {

  FileAsBody =  {} as IComprobante;
  bs64File : any;

  error   : boolean;
  message : string;
  success : boolean;

  constructor(  private spinner : NgxSpinnerService,
                private ssale : SaleService,
                private sanitizer: DomSanitizer,
                @Inject( MAT_DIALOG_DATA ) public data : any ) {
                  
    this.FileAsBody.ruc = data.ruc;
    this.FileAsBody.serie = data.serie;
    this.FileAsBody.numero = data.numero;
    this.FileAsBody.codigoComprobante = data.codigoComprobante;
    this.FileAsBody.tipoimpresion = data.tipoimpresion ?? 'A4'
  }

  ngOnInit(): void {
    this._showFilepdf();
  }

  _showFilepdf() {

    this.initialize();
    const body = {
      ... this.FileAsBody
    };

    this.ssale.ReimprimirSale(body).subscribe((response: any) => {
      
      if (response.message === 'exito'){
        
        const bs64 = response.result;
        this.bs64File = 'data:application/pdf;base64,' + (this.sanitizer.bypassSecurityTrustResourceUrl(bs64) as any).changingThisBreaksApplicationSecurity;  
        top.document.getElementById('ifrm').setAttribute("src", this.bs64File);

      }
      else {

        this.message = response.message ?? "Sin conexion al servidor";
        this.error = true;
      }

      this.spinner.hide();
    }, (error) => {

      this.message = error.error.message ?? "Sin conexion al servidor";
      this.error = true;
      this.spinner.hide();

    });

  }

  initialize() {
    this.spinner.show();
    this.error = false;
    this.success = false;
    this.message = null;
    this.bs64File = null;
  }

}