import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ValidatorsService } from 'src/app/services/validators.service';
import { SaleService } from 'src/app/services/sale.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FunctionsService } from 'src/app/services/functions.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { ClientService } from 'src/app/services/client.service';
import { Correo } from 'src/app/models/client.model';
import { VercomprobanteComponent } from './vercomprobante/vercomprobante.component';
import { IComprobante } from 'src/app/interfaces/cpe.interface';
import { ModalbuscarComponent } from './modalbuscar/modalbuscar.component';
import { ICondition, IStatus } from 'src/app/models/report.model';

let datosdatails: any = {};
@Component({
  selector: 'app-repadministrativo',
  templateUrl: './repadministrativo.component.html'
})
export class RepadministrativoComponent implements OnInit {


  // envio masivo prueba
  listaMigrar:any=[];
  mostrarprograsoMigrar:boolean=true;
  progreso:number = 0;
  totalEnviar:number = 0;
  totalEnviados:number=0;

  success : boolean;

  icondition  = new ICondition();
  istatus     = new IStatus();
  filterall : boolean = true;

  cnn_expi: boolean;
  error: boolean;
  message: string;
  ruc : string;

  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource([]);
  dataList : any[] = [];
  columns = [];

  dataSourceDetails = new MatTableDataSource([]);
  displayedColumnsDetails: string[] = [];
  dataListdetails : any[] = [];
  columnsDetails = [];

  form: FormGroup;
  detailsdata = {} as Details;


  FileAsBody = {} as IComprobante;
  datosDetails = {} as Details;

  ArrayFiltro:any = {
    activo: true,
    anulado: true,
    enviado: true,
    noenviado: true
  };


  @ViewChild('pgAdmin') paginatorAdmin: MatPaginator;
  @ViewChild('pgDetalle') paginatorDetalle: MatPaginator;

  constructor(public Dialog: MatDialog,
    private fb: FormBuilder,
    private svalidator: ValidatorsService,
    public sfuntion: FunctionsService,
    private spinner: NgxSpinnerService,
    private ssale: SaleService,) {

    this.initializeAmount();
    this.create_form();
    this.ruc = JSON.parse(localStorage.getItem('_token_login')).ruc

  }

  ngOnInit(): void { }

  openTab = 1;
  toggleTabs($tabNumber: number){
    this.openTab = $tabNumber;
  }

  // modal ((Ver detalle))
  VerDetalle($event) {
    datosdatails = null;
    datosdatails = $event;
    this.Dialog.open(ModalVerDetalle).afterClosed().subscribe(response => {
      if( response.error  ){

        this.error = true;
        this.message = 'No se encontro el comprobante';

      }
    });
  }

  _vercpe( $event ){

    this.Dialog.open(VercomprobanteComponent, {
      data: {
        ruc : this.ruc,
        serie : $event.Serie,
        numero : $event.Numero,
        codigoComprobante : $event.Tipo,
        tipoimpresion : $event.IDTipoImpresion
      }
    });

  }

  

  filtrar(){

    this.Dialog.open(ModalbuscarComponent, {
      data: {
        icondition  : this.icondition,
        istatus     : this.istatus
      }
    })
    .afterClosed()
    .subscribe( () =>{

      this._loadTable();
      this.spinner.hide();

    });
  }


  get datestartInvalid() {
    return this.svalidator.control_invalid('fechainicio', this.form);
  }
  get datefinInvalid() {
    return this.svalidator.control_invalid('fechafin', this.form);
  }

  create_form() {
    this.form = this.fb.group({
      fechainicio : [ new Date(), Validators.required ],
      fechafin    : [ new Date(), Validators.required ],
      serie       : [ '' ],
      numero      : [ '' ],
      cliente     : [ '' ]
    });
  }

  list() {

    if (this.form.invalid) {
      return this.svalidator.Empty_data(this.form);
    }
    this.initialize();

    const body = {
      ... this.form.value
    };
    body.fechainicio = this.sfuntion.convert_fecha(body.fechainicio);
    body.fechafin = this.sfuntion.convert_fecha(body.fechafin);
    body.numero = String(body.numero) === 'null' ? '0' : String(body.numero) ;

    this.listAdmin(body);
    this.listDetalle(body);
  }

  listAdmin(body:any){

    this.ssale.list_saleAdmin(body).subscribe((response: any) => {

      const result = response.result;
      if (result.length > 0) {

        const columns = result[0];
        const keys = this.sfuntion.KeyCabecera(columns);
        this.columns = keys.columns;
        this.displayedColumns = keys.displayedColumns;

        this.dataSource = new MatTableDataSource(result);
        this.dataList = result;
        this._loadTable();

      }

      this.spinner.hide();

    }, (error) => {

      this.error = true;
      const cnn_expi = error.error === 'Unauthorized';
      this.cnn_expi = cnn_expi;
      this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
      this.spinner.hide();

    })
  }

  listDetalle( body:any ){

    this.ssale.list_saledetails(body).subscribe((response: any) => {

      const result = response.result;
      if (result.length > 0) {

        const columns = result[0];
        const keys = this.sfuntion.KeyCabecera(columns);
        this.columnsDetails = keys.columns;
        this.displayedColumnsDetails = keys.displayedColumns;

        this.dataSourceDetails = new MatTableDataSource(result);
        this.dataListdetails = result;
        // this._loadTable();

        this.dataSourceDetails.paginator = this.paginatorDetalle;
        this.paginatorDetalle._intl.itemsPerPageLabel = 'items por pagina';
        this.detailsdata.totalDetalle = this.dataSourceDetails.data.map( x=>x.Importe ).reduce( ( t, a ) => t + a, 0 );
      }

      this.spinner.hide();

    }, (error) => {

      this.error = true;
      const cnn_expi = error.error === 'Unauthorized';
      this.cnn_expi = cnn_expi;
      this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
      this.spinner.hide();

    })

  }

  initialize() {
    this.spinner.show();
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
    this.dataSource = new MatTableDataSource([]);;
    this.columns = [];
    this.displayedColumns = [];
    
    this.dataSourceDetails = new MatTableDataSource([]);
    this.displayedColumnsDetails = []
    this.dataListdetails = [];
    this.columnsDetails = [];

    this.initializeAmount();

  }

  initializeAmount(){
    this.detailsdata.exonerada = 0;
    this.detailsdata.afecto = 0;
    this.detailsdata.inafecto = 0;
    this.detailsdata.igv = 0;
    this.detailsdata.total = 0;
  }

  exportAsExcel() {

    if( this.dataSource.data.length > 0 ){

      const ruc = JSON.parse(localStorage.getItem('_token_login')).ruc
      const name_file = `${ruc}-REPORTE ADMIN`;
      const jsonString = JSON.stringify(this.dataSource.data);
      const json = JSON.parse(jsonString);

      json.forEach(el => {

        delete el.Marcar;
        delete el.IDAnuladoEnviado;
        delete el.IDCliente;
        delete el.IDEnviado;
        delete el.IDTipoImpresion;
      });

      this.sfuntion.exportToExcel(json, name_file)
    }
    else{
      this.error = true;
      this.message = 'No existe datos para exportar en el reporte ADMIN';
    }

  }

  exportAsExcelDetails() {

    if( this.dataSourceDetails.data.length > 0 ){

      const ruc = JSON.parse(localStorage.getItem('_token_login')).ruc
      const name_file = `${ruc}-REPORTE DETALLE`;
      this.sfuntion.exportToExcel(this.dataSourceDetails.data, name_file)
    }
    else{
      this.error = true;
      this.message = 'No existe datos para exportar en el reporte DETALLE';
    }

  }

  _loadTable() : void {
    //condition
    const activo = this.icondition.activo;
    const anulado = this.icondition.anulado;

    //status
    const enviado = this.istatus.enviado;
    const not_enviado = this.istatus.not_enviado;

    this.filterall = activo && anulado && enviado && not_enviado;

    if( this.filterall ){
      this.dataSource = new MatTableDataSource( this.dataList );
    }
    else
    {

      this.dataSource = new MatTableDataSource( this.dataList.filter( result =>{

        const dbActivo = result.Cliente !== 'ANULADO';
        const dbenviado = result.IDEnviado == null ? false : result.IDEnviado;

        if( dbenviado === enviado && dbActivo === activo )
        {
          return result;
        }

        if( !dbenviado === not_enviado && !dbActivo === anulado )
        {
          return result;
        }
      }));
    }

    this.dataSource.paginator = this.paginatorAdmin;
    this.paginatorAdmin._intl.itemsPerPageLabel = 'items por pagina';
    this.detailsdata.exonerada = this.dataSource.data.map( x=>x.Exonerada ).reduce( ( t, a ) => t + a, 0 );
    this.detailsdata.afecto = this.dataSource.data.map( x=>x.Afecta ).reduce( ( t, a ) => t + a, 0 );
    this.detailsdata.inafecto = this.dataSource.data.map( x=>x.Inafecta ).reduce( ( t, a ) => t + a, 0 );
    this.detailsdata.igv = this.dataSource.data.map( x=>x.Igv ).reduce( ( t, a ) => t + a, 0 );
    this.detailsdata.total = this.dataSource.data.map( x=>x.Total ).reduce( ( t, a ) => t + a, 0 );

  }

  validateKey(event, limite ) {

    if(!this.svalidator.validateKey( event, limite ))return false;
    return true;

  }

  comprobanteSeleccionado($event)
  {
    this.listaMigrar = $event;
  }

  async MigrarLista()
  {

    this.totalEnviar = this.listaMigrar.length;
    if (this.totalEnviar == 0)
    {
      this.error = true;
      this.message = `Debe seleccionar al menos un comprobante`;
    }
    else
    {
      this.spinner.show();
      this.mostrarprograsoMigrar = false;
      const rucenvio = JSON.parse(localStorage.getItem('_token_login_m')).ruc;
      for (let i = 0; i < this.listaMigrar.length; i++)
      {
        const cuerpo = {
          serie: this.listaMigrar[i].Serie,
          numero: this.listaMigrar[i].Numero,
          codigoComprobante: this.listaMigrar[i].Tipo,
          ruc: rucenvio
        };

        const respuesta:any = await this.ssale.send_sunat(cuerpo).toPromise();
        if (respuesta.exito)
        {
          this.totalEnviados++;
          this.progreso = Math.trunc((100 / this.listaMigrar.length) * this.totalEnviados);
        }
      }

      this.spinner.hide();
      this.mostrarprograsoMigrar = true;
      this.success = true;
      this.message = `Comprobantes enviados ${this.totalEnviados} de ${this.totalEnviar}`;
      this.close_success();
      this.list();
    }
  }


  close_success()
  {

    setTimeout(()=>{
      this.success = false;
      this.message = null;
    }, 3000)
  }
}








// (modal VER--- detalle)
@Component({
  selector: 'app-modalVerDetalle',
  templateUrl: './modalVerDetalle.html',
})
export class ModalVerDetalle {

  selectedCondition : string;
  FileAsBody = {} as IComprobante;
  datosDetails = {} as Details;

  delete  : boolean;
  error   : boolean;
  message : string;
  success : boolean;
  emails  : Correo = new Correo();


  public mostrarDatos: boolean;
  enviar: boolean = false;

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  correo = new FormControl();
  correoElec:string[];
  emailInvalid:boolean;

  constructor(  private spinner   : NgxSpinnerService,
                private ssale     : SaleService,
                private sfunction : FunctionsService,
                private sclient   : ClientService,
                private dialogRef : MatDialogRef<RepadministrativoComponent> ) {

    this.spinner.show();
    this.mostrarDatos = false;
    this.FileAsBody.serie = datosdatails.Serie;
    this.FileAsBody.numero = datosdatails.Numero;
    this.FileAsBody.codigoComprobante = datosdatails.Tipo;
    this.FileAsBody.nuevoFormato = true;

    this.FileAsBody.client = datosdatails.Cliente;
    this.FileAsBody.ruc = JSON.parse(localStorage.getItem('_token_login')).ruc;
    this.list_detailsSale();
  }

  validate_email( $event ) {

    let oldval = $event.target.value;
    const val = oldval.slice(0,$event.target.selectionStart) + $event.key + oldval.slice($event.target.selectionEnd);

    this.emailInvalid = false;
    const pattern = '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$';
    if( !val.match( pattern ) ) this.emailInvalid = true;
  }

  Save_email( email:string, eliminar:boolean ){

    if( !eliminar ) {
      this.emailInvalid = false;
      const pattern = '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$';
      if( !email.match( pattern ) ){
        this.emailInvalid = true;
        return;
      }

    }

    if( this.FileAsBody.client.toUpperCase() === 'CLIENTES VARIOS' ){
      if( !eliminar ) this.correoElec.push(email);
      else{

        const index = this.correoElec.indexOf( email.trim() );
        if (index >= 0) this.correoElec.splice(index, 1);

      }
      return;
    };

    const body = {
      idcliente: this.datosDetails.idCliente,
      correo : email,
      eliminar: eliminar
    }

    this.sclient.add_removeEmail( body ).subscribe( response => {

      if( !eliminar ){
        this.correoElec.push(email);
      }
      else{

        const index = this.correoElec.indexOf( email.trim() );
        if (index >= 0) this.correoElec.splice(index, 1);
      }

    }, (error)=>{
      console.log(error)
    })


  }

  add(event: MatChipInputEvent): void {

    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) this.Save_email( value.trim(), false ); // Add our correoElec -> Save
    if (input) input.value = '';// Reset the input value
    this.correo.setValue(null);

  }

  remove(email: string): void {
    this.Save_email( email.trim(), true );
  }

  send_Sunat() {

    this.initialize();
    const body = {
      ... this.FileAsBody
    }

    body.produccion = true;

    this.ssale.send_sunat(body).subscribe((response: any) => {

      if (response.exito) {
        this.list_detailsSale();
      }
      else {

        this.message = response.message ?? "Sin conexion al servidor";
        this.error = true;

      }

      this.spinner.hide();

    }, (error) => {

      this.message = error.error.message ?? "Sin conexion al servidor";
      this.error = true;
      this.spinner.hide();

    })

  }

  send_email(){

    this.initialize();
    const body = {
      ... this.FileAsBody
    };

    body.correo = this.correoElec.join(',');
    this.ssale.send_email( body ).subscribe( (response : any ) =>{

      if( response.exito ){
        this.success = true;
        this.message = 'el correo se envio Exitosamente';
      }
      else {
        this.error = true;
        this.message = 'Operacion incorrecta';
      }

      this.close_success();
      this.spinner.hide();

    }, (error)=>{

      this.message = error.error.message ?? "Sin conexion al servidor";
      this.error = true;
      this.spinner.hide();

    });

  }

  dowloadFilepdf() {

    this.initialize();
    const body = {
      ... this.FileAsBody
    };

    this.ssale.ReimprimirSale(body).subscribe((response: any) => {

      if (response.message === 'exito') this.downloadFile( response, 'pdf' );
      else {

        this.message = response.message ?? "Sin conexion al servidor";
        this.error = true;
      }

      this.spinner.hide();

    }, (error) => {

      this.message = error.error.message ?? "Sin conexion al servidor";
      this.error = true;
      this.spinner.hide();

    })
  }

  downloadFilexml() {

    this.initialize();
    const body = {
      ... this.FileAsBody
    };

    this.ssale.xml(body).subscribe((response: any) => {

      if (response.exito) this.downloadFile( response, 'xml' );
      else {

        this.message = response.message ?? "Sin conexion al servidor";
        this.error = true;

      }

      this.spinner.hide();

    }, (error) => {

      this.message = error.error.message ?? "Sin conexion al servidor";
      this.error = true;
      this.spinner.hide();

    });

  }

  downloadFileConstancia() {

    this.initialize();
    const body = {
      ... this.FileAsBody
    };

    this.ssale.Constancia(body).subscribe((response: any) => {

      if (response.exito) this.downloadFile( response, 'pdf', true );
      else {

        this.message = response.message ?? "Sin conexion al servidor";
        this.error = true;

      }

      this.spinner.hide();

    }, (error) => {

      this.message = error.error.message ?? "Sin conexion al servidor";
      this.error = true;
      this.spinner.hide();

    });

  }

  downloadFilecdr() {

    this.initialize();
    const body = {
      ... this.FileAsBody
    };

    this.ssale.cdr(body).subscribe((response: any) => {

      if (response.exito) this.downloadFile( response, 'cdr' );
      else {

        this.message = response.message ?? "Sin conexion al servidor";
        this.error = true;

      }

      this.spinner.hide();

    }, (error) => {

      this.message = error.error.message ?? "Sin conexion al servidor";
      this.error = true;
      this.spinner.hide();

    })

  }

  private downloadFile( response : any, typeFile:string, constancia : boolean = false ){

    const extension = typeFile === 'cdr' ? 'zip' : typeFile;
    const bs64 = response.result;
    const blob = this.sfunction.base64toBlob(bs64, { type: `application/${typeFile}` });
    const name_Archivo = `${constancia ? 'C-' : '' }${this.FileAsBody.ruc}-${this.FileAsBody.codigoComprobante}-${this.FileAsBody.serie}-${this.FileAsBody.numero}.${extension}`.trim();
    this.sfunction.downloadFile(blob, name_Archivo);

  }

  initialize() {
    this.spinner.show();
    this.emails  = new Correo();
    this.error = false;
    this.success = false;
    this.message = null;
  }

  load_emails(){

    const body = {
      numerodocumento : this.datosDetails.NDocumentoCliente
    }

    this.sclient.filter_ClientDoc( body ).subscribe( response =>{

      const correos = response.Correos.map( x=>x.Correo );
      this.correoElec = correos;
      this.datosDetails.idCliente = String(response.IDCliente)

      this.spinner.hide();
    }, (error)=>{
      console.log(error);
      this.spinner.hide();
    })

  }

  list_detailsSale() {

    const body = {
      ... this.FileAsBody
    }

    this.ssale.listDetailscpe(body).subscribe(result => {

      if( result ){
        
        this.selectedCondition = result.Anulado ? 'ANULADO' : 'ACTIVO';
        this.datosDetails.detraccion = result.Detraccion ? 'SI' : 'NO';
        this.datosDetails.detraccionPorcentaje = result.DetraccionPorcentaje;
        this.datosDetails.detraccionMonto = result.DetraccionMonto;
        this.datosDetails.gRemitente = result.GuiaRemision;
        this.datosDetails.gTransportista = result.GuiaTrasportista;
        this.datosDetails.placa = result.Placa;
        this.datosDetails.ordencompra = result.OrdenCompra;
        this.datosDetails.icbper = 0,
        this.datosDetails.descuento = result.DescuentoGlobal;
        this.datosDetails.exonerada = result.Exonerada;
        this.datosDetails.gratuita = result.Gratuita;
        this.datosDetails.afecto = result.Afecto;
        this.datosDetails.inafecto = result.Inafecto;
        this.datosDetails.igv = result.Igv;
        this.datosDetails.total = result.Total
        this.datosDetails.codigoMoneda = result.MonedaCodigo;

        this.datosDetails.respuestaenvio = result.MensajeSunat;
        this.datosDetails.Resultado_Validez = result.Resultado_Validez;
        this.datosDetails.Enviado = result.Enviado;
        this.datosDetails.AnuladoEnviado = result.AnuladoEnviado;
        this.datosDetails.Anulado = result.Anulado;

        this.datosDetails.ClienteDireccion = result.ClienteDireccion;
        this.datosDetails.Descripcion = result.Descripcion;
        this.datosDetails.NDocumentoCliente = result.ClienteNumeroDocumento;

        this.enviar = (!this.datosDetails.Anulado && this.datosDetails.Enviado) || (this.datosDetails.Anulado && this.datosDetails.Enviado && this.datosDetails.AnuladoEnviado);
        this.mostrarDatos = this.enviar;

        this.datosDetails.cpeRelacionado = result.NotaSerieNumero ?? '';

        this.load_emails();
      }
      else{

        this.dialogRef.close( { error : true } );
      }

      this.spinner.hide();

    }, (error) => {

      this.spinner.hide();
      console.log(error)

    })

  }

  close_success(){

    setTimeout(()=>{
      this.success = false;
      this.message = null;
    }, 3000)
  }

  onChangeCondition( condition:string ){

    this.delete = false;
    this.message = null;

    if( condition === 'ANULADO' ){

      this.delete = true;
      this.message = '¿Desea anular el comprobante directamente en sunat?';
    }

  }

  _anular_cpe( $event ){

    if( !$event ) return;

    this.spinner.show();
    const body = {
      ... this.FileAsBody
    }
    this.ssale.AnularCpe( body ).subscribe( (response : any ) =>{

      if( response.exito ){

        const result = response.result[0];
        this.success = true;
        this.message = response.message;
        this.datosDetails.respuestaenvio = result.MensajeSunat;
        this.datosDetails.Resultado_Validez = result.Resultado_Validez;
        this.datosDetails.Anulado = result.Anulado;
        this.selectedCondition = result.Anulado ? 'ANULADO' : 'ACTIVO'
        this.enviar = (!result.Anulado && result.Enviado) || (result.Anulado && result.Enviado && result.AnuladoEnviado);
        this.mostrarDatos = this.enviar;

        this.close_success();
      }
      else{
        this.error = true;
        this.message = response.message;
      }

      this.spinner.hide();

    }, (error) => {
      console.log(error)
      this.spinner.hide();
    });
  }

  _cancelModal($event){

    this.delete = $event
    if( !$event ){
      this.selectedCondition = 'ACTIVO'
    }
  }

  validarCPEReinteto()
  {

    const body = {
      ... this.FileAsBody
    }
    
    this.spinner.show();
    this.ssale.ValidarCPE(body).subscribe((result:any) => {
      
      this.datosDetails.Resultado_Validez = result.result;
      this.spinner.hide();

    }, (error) => {

      this.spinner.hide();
      console.log(error)

    })
  }

}

export interface Details {
  detraccion: string;
  detraccionPorcentaje: string;
  detraccionMonto: string;
  gRemitente: string;
  gTransportista: string;
  placa: string;
  ordencompra: string;
  ordenPago: string;
  codigoMoneda: string;
  icbper: number;
  descuento: number;
  exonerada: number;
  gratuita: number;
  afecto: number;
  inafecto: number;
  igv: number;
  total: number;

  respuestaenvio: string;
  Resultado_Validez: string;
  Enviado: boolean;
  AnuladoEnviado: boolean;
  Anulado: boolean;

  ClienteDireccion: string;
  Descripcion: string;

  NDocumentoCliente:string;
  idCliente:string;

  cpeRelacionado:string;
  totalDetalle:number;
}
