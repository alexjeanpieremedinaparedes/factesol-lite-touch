import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { SaleService } from 'src/app/services/sale.service';
import { ValidatorsService } from 'src/app/services/validators.service';

import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from '@angular/material/paginator';
import { FunctionsService } from 'src/app/services/functions.service';
import { MatDialog } from '@angular/material/dialog';
import { ObtenerDocumentosMasivosComponent } from './obtener-documentos-masivos/obtener-documentos-masivos.component';
import { IProcesoMasivo } from 'src/app/models/proccessMassive';

@Component({
  selector: 'app-repcontable',
  templateUrl: './repcontable.component.html'
})

export class RepcontableComponent implements OnInit {

  IProcess = new IProcesoMasivo();
  totalcpe : number;

  cnn_expi: boolean;
  error: boolean;
  message: string;


  columns = [];
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource([]);
  arrayTable : any [] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  form: FormGroup;

  constructor(private ssale: SaleService,
    private fb: FormBuilder,
    private svalidator: ValidatorsService,
    private spinner: NgxSpinnerService,
    private sfunction: FunctionsService,
    private modal    : MatDialog ) {
    this.create_form();
  }

  ngOnInit(): void { }

  get yearInvalid() {
    return this.svalidator.control_invalid('anio', this.form);
  }

  get monthInvalid() {
    return this.svalidator.control_invalid('mes', this.form);
  }

  create_form() {

    this.form = this.fb.group({
      'anio': [new Date().getFullYear(), [Validators.required]],
      'mes': ['', [Validators.required]]
    })

  }

  list() {


    if (this.form.invalid) {
      return this.svalidator.Empty_data(this.form);
    }

    this.initialize();
    const body = {
      ... this.form.value
    };

    this.ssale.list_saleAccountant(body).subscribe((response: any) => {

      const result = response.result;
      if (result.length > 0) {

        const columns = result[0];
        const keys = this.sfunction.KeyCabecera(columns);
        this.columns = keys.columns;
        this.displayedColumns = keys.displayedColumns;

        this.dataSource = new MatTableDataSource(result);
        this.arrayTable = result;
        this.dataSource.paginator = this.paginator;
        this.paginator._intl.itemsPerPageLabel = 'items por pagina';

      }

      this.spinner.hide();

    }, (error) => {

      this.error = true;
      const cnn_expi = error.error === 'Unauthorized';
      this.cnn_expi = cnn_expi;
      this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
      this.spinner.hide();

    });
  }

  // obtener documntos  masivos
  obtenerDocumentMasivo( inprocess : boolean = false ){

    this.error = false;
    this.message = null;
    if (this.form.invalid) {
      return this.svalidator.Empty_data(this.form);
    }

    if( this.arrayTable.length === 0 ){
      this.error = true;
      this.message = 'para contiuar cargue el listado de comprobantes';
      return;
    }

    this.totalcpe = this.arrayTable.filter((e)=>{
      if(e.IDEnviado === true) return e;
    }).length;
    let data = {
      ... this.form.value,
      totalcpe : this.totalcpe,
      IProcess : this.IProcess,
      inprocess : inprocess,
      Datos: this.arrayTable.filter((e)=>{ if(e.IDEnviado === true ) return e; })
    };

    this.modal.open(ObtenerDocumentosMasivosComponent, {
      data : data,
      disableClose : true
    })
    .afterClosed().subscribe( result => {

      if(result.closeall){
        this._resetProcessMassive();
      }
    })

  }

  initialize() {
    this.spinner.show();
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
    this.dataSource = null;
    this.arrayTable = [];
    this.columns = [];
    this.displayedColumns = [];
  }

  _resetProcessMassive(){
    this.IProcess.init = false;
    this.IProcess.completed = true;
    this.IProcess.porcentual = 0;
    this.IProcess.i = 0;
    this.IProcess.cantTotalcpe = 0;
    this.IProcess.time = '';

    this.IProcess.anulados = true;
    this.IProcess.pdf = false;
    this.IProcess.xml = false;
    this.IProcess.cdr = false;
    this.IProcess.filedownload = null;
  }

  validateKey(event, limite) {

    if (!this.svalidator.validateKey(event, limite)) return false;
    return true;

  }

  exportAsExcel() {

    if( this.arrayTable.length > 0 ){

      if( !this.ValidateExcel() ) return;
      const ruc = JSON.parse(localStorage.getItem('_token_login')).ruc
      const anio = this.form.value.anio;
      const mes = this.form.value.mes;
      const name_file = `${ruc}-${anio}-${mes}`;
      this.sfunction.exportToExcel(this.dataSource.data, name_file)
    }
    else {
      this.error = true;
      this.message = 'No hay datos para exportar';
    }

  }

  ValidateExcel() : boolean {

    let notEnviado = 0;
    this.dataSource.data.forEach( (el, i) =>{
      if( !el.IDAnuladoEnviado && String(el['RAZÓN SOCIAL & NOMBRE']).toUpperCase() === 'ANULADO' ) notEnviado++;
    });

    if( notEnviado > 0 ){

      this.error = true;
      this.message = 'No se puede exportar las ventas hasta que todo esté enviado correctamente';
      return false;
    }

    return true;
  }

}
