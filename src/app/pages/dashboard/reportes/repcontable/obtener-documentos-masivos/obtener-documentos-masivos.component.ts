import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { IProcesoMasivo } from 'src/app/models/proccessMassive';
import { FunctionsService } from 'src/app/services/functions.service';
import { SaleService } from 'src/app/services/sale.service';
import { UpdatedatadowloandfilesService } from 'src/app/services/updatedatadowloandfiles.service';
import { RepcontableComponent } from '../repcontable.component';

@Component({
  selector: 'app-obtener-documentos-masivos',
  templateUrl: './obtener-documentos-masivos.component.html',
  styleUrls: ['./obtener-documentos-masivos.component.css']
})
export class ObtenerDocumentosMasivosComponent implements OnInit {

  question  : boolean;
  success   : boolean;
  cnn_expi  : boolean;
  error     : boolean;
  message   : string;

  openTab   : number = 1;
  form      : FormGroup;

  IProcess : IProcesoMasivo;
  timePorcentual  : any;
  datos           :any=[];

  constructor(
    public sfunction : FunctionsService ,
    private fb : FormBuilder,
    private ssale : SaleService,
    private spinner : NgxSpinnerService ,
    private dialogRef : MatDialogRef<RepcontableComponent>,
    private sProcess : UpdatedatadowloandfilesService,
    @Inject( MAT_DIALOG_DATA ) public data : any) {

      this.IProcess = data.IProcess;
      this.datos = data.Datos;
      this._create_form();
    }

  ngOnInit(): void {
    if(this.data.inprocess){
      this.openTab = 2;
    }
  }

  _create_form(){
    this.form = this.fb.group({
      ruc         : [ String(JSON.parse(localStorage.getItem('_token_login')).ruc) ],
      anio        : [ String(this.data.anio) ],
      mes         : [ String(this.data.mes) ],
      anulados    : [ this.IProcess.anulados ],
      pdf         : [ this.IProcess.pdf ],
      xml         : [ this.IProcess.xml ],
      cdr         : [ this.IProcess.cdr ]
    })
  }

  _continueTab2( $tabNumber: number ){

    if( !this._validateConitnue() ) return;
    this._obtenerFiles();
    this.IProcess.init = true;
    this.IProcess.completed = false;
    this.IProcess.cantTotalcpe = this.data.totalcpe;
    this.IProcess.anio = String(this.data.anio);
    this.IProcess.mes = String(this.data.mes);

    this.openTab = $tabNumber;
    this._generarcpe();
  }

  _validateConitnue() : boolean {

    this._initialize();
    const value = this.form.value;
    const validate = [ value.pdf, value.xml, value.cdr ]
    if( !validate.includes(true) ){
      this.error = true;
      this.message = 'Seleccione un archivo a descargar';
      return false;
    }

    return true;
  }

  async _obtenerFiles()
  {
    this.IProcess.i = 0;
    for (let i = 0; i < this.datos.length; i++)
    {
      try
      {
        const archivos = this.form.value;
        const body = {
          ruc     : String(JSON.parse(localStorage.getItem('_token_login')).ruc),
          fecha   :  this.datos[i]['FECHA EMISIÓN'],
          codigocomprobante: this.datos[i]['TIPO'],
          serie   : this.datos[i]['SERIE'],
          numero  : this.datos[i]['NÚMERO'],
          anulado : this.datos[i]['RAZÓN SOCIAL & NOMBRE'] === 'ANULADO' ? true : false,
          pdf     : archivos.pdf,
          xml     : archivos.xml,
          cdr     : archivos.cdr
        };

        const result:any = await this.ssale.GenerateFileMassive( body ).toPromise();
        if( result.status === 200 )
        {
          const porcentual = Math.round((100 / this.data.totalcpe) * this.IProcess.i);
          this.IProcess.porcentual = porcentual;
        }
        this.IProcess.i ++;
        console.log(this.IProcess.i);
      }
      catch{}
    }

    const cuerpo = {
      ruc: JSON.parse(localStorage.getItem('_token_login')).ruc,
      anio: String(this.data.anio),
      mes: String(this.data.mes)
    };
    const result:any = await this.ssale.GenerateFileRAR( cuerpo ).toPromise();

    console.log(result);
    if(result.status === 200)
    {
      this.IProcess.filedownload = result.result;
      this.IProcess.completed = true;
      this._downloadFile();
    }

    // this.IProcess.i = this.data.totalcpe;
    // this.IProcess.porcentual = 100;
    // this.IProcess.time = 'Proceso completado';

    // this.IProcess.filedownload = result.result;
    //this.IProcess.completed = true;

  }
  _generarcpe(){

    setTimeout(() => {

      const porcentual = Math.round((this.IProcess.i * 100) / this.data.totalcpe);
      this.IProcess.porcentual = porcentual;
      this._timeRemaining();
      if( porcentual < 100  ){
        this._generarcpe();
      }

    }, 2000);

  }

  _downloadFile(){
    window.open(this.IProcess.filedownload);
  }

  _minimizar(){

    this.IProcess.anulados = this.form.value.anulados;
    this.IProcess.pdf = this.form.value.pdf;
    this.IProcess.xml = this.form.value.xml;
    this.IProcess.cdr = this.form.value.cdr;
    this.dialogRef.close( { minimizar : true } );

    //prueba
    this.sProcess.proccess$.emit(  this.IProcess );
  }

  _closed(){
    this.dialogRef.close( { closeall:true } );
  }

  _questionObtnerFiles(){
    this.question = !this.question;
    this.message = '¿Volver a obtner los archivos?';
  }
  _volveraObtnerFiles( $event ){
    if(!$event) return;
    this.openTab = 1;
    this._initialize();
    this._clearform();
  }

  _initialize(){

    this.question = false;
    this.success = false;
    this.error = false;
    this.cnn_expi = false;
    this.message =  null;
    this._resetProcessMassive();

  }
  _clearform(){
    this._resetProcessMassive();
    this.form.patchValue({
      anulados    : this.IProcess.anulados,
      pdf         : this.IProcess.pdf,
      xml         : this.IProcess.xml,
      cdr         : this.IProcess.cdr
    })
  }

  _resetProcessMassive(){
    this.IProcess.init = false;
    this.IProcess.completed = true;
    this.IProcess.porcentual = 0;
    this.IProcess.i = 0;
    this.IProcess.cantTotalcpe = 0;
    this.IProcess.time = '';

    this.IProcess.anulados = true;
    this.IProcess.pdf = false;
    this.IProcess.xml = false;
    this.IProcess.cdr = false;
    this.IProcess.filedownload = null;
  }

  _timeRemaining(){

    const totalSecunds = 2 * (this.data.totalcpe - this.IProcess.i);
    const time = this.sfunction._timerestant(totalSecunds);
    if(  totalSecunds > 0 ){
      this.IProcess.time = `Tiempo estimado ${time}`;
    }

    if( this.IProcess.filedownload ){

      clearTimeout(this.timePorcentual);
      //this.IProcess.i = this.data.totalcpe;
      this.IProcess.porcentual = 100;
      this.IProcess.time = 'Proceso completado';
    }

  }

  _exeception(error: any) {

    console.log(error);
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  @HostListener('window:beforeunload', ['$event'])
  doSomething($event) {

    const validate = !this.IProcess.completed;
    if(validate){
      $event.returnValue='Your data will be lost!';
    }
  }

}
