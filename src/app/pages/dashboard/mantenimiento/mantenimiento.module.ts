import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductoserviciosComponent } from './productoservicios/productoservicios.component';
import { ModalNuevaunidmedidaComponent } from './productoservicios/modalsprodServi/modal-nuevaunidmedida/modal-nuevaunidmedida.component';
import { ModalNuevoproductoComponent } from './productoservicios/modalsprodServi/modal-nuevoproducto/modal-nuevoproducto.component';
import { ModalUnidadmedidaComponent } from './productoservicios/modalsprodServi/modal-unidadmedida/modal-unidadmedida.component';
import { ClientesComponent } from './clientes/clientes.component';
import { DemoMaterialModule } from 'src/app/material-module';
import { ComponentsModule } from 'src/app/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModaleditarpresentacionComponent } from './productoservicios/modalsprodServi/modaleditarpresentacion/modaleditarpresentacion.component';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';


@NgModule({
  declarations: [
    ProductoserviciosComponent,
    ModalNuevaunidmedidaComponent,
    ModalNuevoproductoComponent,
    ModalUnidadmedidaComponent,
    ClientesComponent,
    ModaleditarpresentacionComponent,
  ],
  exports:[
    ProductoserviciosComponent,
    ModalNuevaunidmedidaComponent,
    ModalNuevoproductoComponent,
    ModalUnidadmedidaComponent,
    ClientesComponent,
  ],

  imports: [
    CommonModule,
    DemoMaterialModule,
    ComponentsModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TrimValueAccessorModule
  ]
})
export class MantenimientoModule { }
