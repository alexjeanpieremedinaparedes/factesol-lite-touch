import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { FunctionsService } from 'src/app/services/functions.service';
import { ProductsService } from 'src/app/services/products.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { ModalUnidadmedidaComponent } from './modalsprodServi/modal-unidadmedida/modal-unidadmedida.component';
import { ModaleditarpresentacionComponent } from './modalsprodServi/modaleditarpresentacion/modaleditarpresentacion.component';

@Component({
  selector: 'app-productoservicios',
  templateUrl: './productoservicios.component.html'
})
export class ProductoserviciosComponent implements OnInit {

  idProduct: number;
  delete: boolean = false;
  success: boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;
  delPresentacion : boolean;
  presentacion    : string;
  // eliminarpresent:boolean;
  

  newProduct: boolean;

  columns = [];
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource([]);
  listProduct: [] = [];
  listPresentacion : any[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  form: FormGroup;
  formSecundario : FormGroup;

  constructor(  public dialog     : MatDialog,
                private sProduct  : ProductsService,
                private spinner   : NgxSpinnerService,
                private sfunction : FunctionsService,
                private svalidator: ValidatorsService,
                private fb        : FormBuilder ) {

    this.create_form();
    this.create_formSecundario();
    this.listProducts('');

  }
  ngOnInit(): void { }

  _newProduct(){
    this.newProduct = !this.newProduct;
    this._clear();
  }

  get nameProductInvalid(){
    return this.svalidator.control_invalid('nombreproducto', this.form);
  }

  get afectacionInvalid(){
    return this.svalidator.control_invalid('afectacion' , this.form);
  }

  get tipoProductoInvalid(){
    return this.svalidator.control_invalid('tipoproducto', this.form);
  }

  get stockInvalid(){
    return this.svalidator.control_invalid('stock', this.form);
  }

  get unidadMedidaInvalid(){
    return this.svalidator.control_invalid('unidadmedia', this.form);
  }

  get categoriaInvalid(){
    return this.svalidator.control_invalid('categoria', this.form);
  }

  get subCategoriaInvalid(){
    return this.svalidator.control_invalid('subcategoria', this.form);
  }

  get icbperInvalid(){
    return this.svalidator.control_invalid('icbper', this.form);
  }

  create_form(){

    this.form = this.fb.group({

      idproducto      : [ 0 ],
      nombreproducto  : [ '', [ Validators.required, Validators.minLength(3) ] ],
      afectacion      : [ '', [ Validators.required ] ],
      tipoproducto    : [ 'Producto', [ Validators.required ] ],
      idunidadmedida  : [ 0 ],
      idsubcategoria  : [ 0 ],
      stock           : [ '', [ Validators.required ] ],
      codigosunat     : [ '-' ],
      icbper          : [ 0, [ Validators.required ] ],
      editar          : [ false ],

      idcategoria    : [ 0 ],
      unidadmedia    : [ '', [ Validators.required ] ],
      categoria      : [ '', [ Validators.required ] ],
      subcategoria   : [ '', [ Validators.required ] ],

      ventarapida   : [ false ]

    });

  }

  listProducts(text:string){

    this.initializeTable();
    this.sProduct.listProduct().subscribe( response =>{
      
      if( response.length > 0 ){

        const columns = response ?? [];
        const keys = this.sfunction.KeyCabecera(columns[0]);
        this.columns = keys.columns;
        this.displayedColumns = keys.displayedColumns;
        this.dataSource = new MatTableDataSource( response );
        this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Producto', text ) );    
        this.listProduct = response;
        this.dataSource.paginator = this.paginator;
        this.paginator._intl.itemsPerPageLabel = 'items por pagina';
      }

      this.spinner.hide();

    }, ( error )=> this._exeception( error ) );

  }

  SaveProduct(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data(this.form);
    }
    
    if( this.listPresentacion.length === 0 ){
      
      this.error = true;
      this.message = 'Para registrar un producto, debe registrar por lo menos una presentación ';
      return;
    }
    
    this.initialize();
    this.spinner.show();
    
    const body = {
      ... this.form.value,
      detalle : this.listPresentacion
    };

    this.sProduct.SaveProduct( body ).subscribe( (response : any ) => {

      if( response.message === 'exito' ){
        this.success = true;
        this._changeExito();
        this._clear();
        this.refresh();
        if( body.editar ) this.newProduct = false;
      }

      this.spinner.hide();

    }, (error) => this._exeception( error ) )

  }

  deleteProduct( $event ){

    if( this.delPresentacion ) this.deleteItemPresentacion( this.presentacion )
    else{

      this.initialize();
      this.spinner.show();
      if( !$event ) return;
      const body = {
        idproducto : this.idProduct
      }
  
      this.sProduct.deleteProduct( body ).subscribe( (response : any ) => {
        
        if( response.message === 'exito' ){
          this.success = true;
          this.message = 'Producto eliminado con exito';
          this.refresh();
          this._changeExito();
        }
  
        this.spinner.hide();
  
      }, ( error )=> this._exeception( error ) );

    }


  }

  showDeletePresentacion( presentacion : string ){

    this.delete = !this.delete;
    this.message = '¿ Quitar item ?';
    this.delPresentacion = true;
    this.presentacion = presentacion;

  }

  deleteItemPresentacion( presentacion : string ){
    
    const index = this.listPresentacion.map( x=>x.presentacion ).indexOf( presentacion );
    this.listPresentacion.splice(index, 1);
    this.refresh();
  }

   
  editarpresentacion( presentacion : string, precio : number ){
    this.dialog.open(ModaleditarpresentacionComponent, { 
      data: {
        presentacion : presentacion,
        precio       : precio
      }
     }).afterClosed().subscribe( response => {

      let dataMoficar = this.listPresentacion.find( x=> x.presentacion === presentacion );
      dataMoficar.presentacion = response.presentacion.toUpperCase();
      dataMoficar.precio = response.precio

     })
  }


  _clear(){

    this.form.reset({

      idproducto      : 0,
      nombreproducto  : '',
      afectacion      : '',
      tipoproducto    : 'Producto',
      idunidadmedida  : 0,
      idsubcategoria  : 0,
      stock           : '',
      codigosunat     : '-',
      icbper          : 0,
      editar          : false,

      idcategoria    : 0,
      unidadmedia    : '',
      categoria      : '',
      subcategoria   : '',
      ventarapida    : false

    });

    this.formSecundario.reset();
    this.listPresentacion = [];

  }

  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }

  refresh() {
    let search = (<HTMLInputElement>document.getElementById('search')).value;

    this.sProduct.listProduct().subscribe( response => {
      if(response.length > 0){

        this.dataSource = new MatTableDataSource<Element>(response);
        this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Producto', search ) );
        this.dataSource.paginator = this.paginator;
        this.listProduct = response;

      }

    }, (error)=>{
      this._exeception( error )
    })
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  enventOperation( $event ){

    if( $event.titulo === 'Eliminar' ){

      this.idProduct = $event.id ?? 0;
      this.message = '¿Está seguro de eliminar el producto?';
      this.delete = !this.delete;

    }
    else if( $event.titulo === 'Editar' ){
    
      this.newProduct = !this.newProduct;
      this.idProduct = $event.id ?? 0;
      this.listProduct.filter( (x : any ) => x.ID === this.idProduct ).map( (result : any ) => {

        this.form.setValue({
          idproducto      : result.ID,
          nombreproducto  : result.Producto,
          afectacion      : result.Afectacion,
          tipoproducto    : result.Tipo.trim().toLowerCase().replace(/\w\S*/, (w) => (w.replace(/^\w/, (c) => c.toUpperCase()))),
          idunidadmedida  : result.IDUnidaMedida,
          idsubcategoria  : result.IDSubCategoria,
          stock           : result.Stock,
          codigosunat     : '-',
          icbper          : result.ICbper,
          editar          : true,

          idcategoria    : result.IDCategoria,
          unidadmedia    : result.NombreUnidadMedida,
          categoria      : result.NombreCategoria,
          subcategoria   : result.NombreSubcategoria,
          ventarapida    : result.VentaRapida === 'SI' ? true : false
        });

        this.listPresentacion = result.IDPresentacion;

      } );
    }

  }

  initialize() {

    this.delPresentacion = false;
    this.delete = false;
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.presentacion = null;
    this.message = null;
  }

  initializeTable(){
    this.spinner.show();
    this.dataSource = null;
    this.columns = [];
    this.displayedColumns = [];
    this.initialize();
  }

  _irModal( operation : string ){

    this.initialize();
    if( operation.toUpperCase() === 'SUB-CATEGORIA' && Number(this.form.value.idcategoria) === 0 ){

      this.error = true;
      this.message = 'Seleccione una categoria';
      return;

    }
    
    this.dialog.open( ModalUnidadmedidaComponent, {
      data : { 
        operation : operation,
        idCategoria : this.form.value.idcategoria
      }
    })
    .afterClosed()
    .subscribe( response => this.loadOperation( operation, response.name, response.id ) );
  }

  loadOperation( operation : string, name : string, id:number ){

    if( name === undefined ) return;
    const op = operation.toUpperCase();

    switch( op ){
      case 'UNIDAD DE MEDIDA':
        this.form.patchValue({ unidadmedia : name, idunidadmedida : id })
        break;
      case 'CATEGORIA':
        this.form.patchValue({ categoria : name, idcategoria : id, subcategoria : '', idsubcategoria : 0 })
        break;
      case  'SUB-CATEGORIA':
        this.form.patchValue({ subcategoria : name, idsubcategoria : id })
        break;
    }

  }

  //Formulario Secundario

  get presentacionInvalid(){
    return this.svalidator.control_invalid('presentacion', this.formSecundario);
  }

  get precioInvalid(){
    return this.svalidator.control_invalid('precio', this.formSecundario);
  }

  create_formSecundario(){

    this.formSecundario = this.fb.group({
      presentacion :  [ '', [ Validators.required, Validators.minLength(2) ] ],
      precio       :  [ '', [ Validators.required ] ]
    });

  }

  addItem(){

    if( this.formSecundario.invalid ){
      return this.svalidator.Empty_data(this.formSecundario);
    }
    
    this.listPresentacion.push( {
      idpresentacion : '0',
      idproducto : '0',
      presentacion : String(this.formSecundario.value.presentacion).toUpperCase(),
      precio : this.formSecundario.value.precio
    });

    this.formSecundario.reset();

  }

  _search( text:string ){

    this.dataSource = new MatTableDataSource( this.listProduct );
    this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Producto', text ) );
    this.dataSource.paginator = this.paginator;
  }

  deletePresentacion( $event ){

    this.initialize();
    if( !$event ) return;

    const body = {
      idproducto : this.idProduct
    }

    this.sProduct.deleteProduct( body ).subscribe( (response : any ) => {
      
      if( response.message === 'exito' ){
        this.delete = true;
        this.message = '';
        this.refresh();
        this._changeExito();
      }

      this.spinner.hide();

    }, ( error )=> this._exeception( error ) );

  }


}