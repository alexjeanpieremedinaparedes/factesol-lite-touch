import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { ModalUnidadmedidaComponent } from '../modal-unidadmedida/modal-unidadmedida.component';

@Component({
  selector: 'app-modal-nuevoproducto',
  templateUrl: './modal-nuevoproducto.component.html',
  styles: [],
})
export class ModalNuevoproductoComponent implements OnInit {
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void { }

  // modal Unidad Medida
  unidMedida() {
    this.dialog.open(ModalUnidadmedidaComponent);
  }

  guardar: boolean = false;
  mensaje: string = "Se registro un nuevo Producto correctamente";
  // modal de  guradar
  modalguardar() {
    this.guardar = !this.guardar;
    this.Settimeout_msexito();
  }

  Settimeout_msexito() {
    setTimeout(() => {
      this.dialog.closeAll();
      this.guardar = false;
    }, 2000);
  }
}
