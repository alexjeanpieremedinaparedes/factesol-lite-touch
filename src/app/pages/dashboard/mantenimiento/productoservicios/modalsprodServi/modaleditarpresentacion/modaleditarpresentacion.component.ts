import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ValidatorsService } from 'src/app/services/validators.service';
import { ProductoserviciosComponent } from '../../productoservicios.component';

@Component({
  selector: 'app-modaleditarpresentacion',
  templateUrl: './modaleditarpresentacion.component.html',
  styles: [
  ]
})
export class ModaleditarpresentacionComponent implements OnInit {

  form : FormGroup;
  constructor(  private fb : FormBuilder ,
                private svalidator : ValidatorsService,
                public dialogRef  : MatDialogRef<ProductoserviciosComponent>,
                @Inject( MAT_DIALOG_DATA ) public data : any ) {
                  
                  this.create_form();
                  this.loadData( data.presentacion, data.precio );
                }

  ngOnInit(): void {}

  get presentacionInvalid(){
    return this.svalidator.control_invalid('presentacion', this.form);
  }

  get precioInvalid(){
    return this.svalidator.control_invalid('precio', this.form);
  }

  create_form(){
    this.form = this.fb.group({
      presentacion : [ '', Validators.required],
      precio       : [ '', Validators.required]
    });
  }

  loadData( presentacion : string, precio: number ){
    this.form.patchValue({
      presentacion : presentacion,
      precio       : precio
    })
  }

  Update(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data(this.form);
    }

    this.dialogRef.close({ 
      presentacion : this.form.value.presentacion,
      precio       : this.form.value.precio
    })

  }

}
