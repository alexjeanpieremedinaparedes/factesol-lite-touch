import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductsService } from 'src/app/services/products.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { ModalUnidadmedidaComponent } from '../modal-unidadmedida/modal-unidadmedida.component';

@Component({
  selector: 'app-modal-nuevaunidmedida',
  templateUrl: './modal-nuevaunidmedida.component.html',
  styles: [
  ]
})
export class ModalNuevaunidmedidaComponent implements OnInit {

  operation : string;
  success: boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;

  form:FormGroup;
  constructor(  private sproduc   : ProductsService,
                private spinner   : NgxSpinnerService,
                private fb        : FormBuilder,
                private svalidato : ValidatorsService,
                public dialogRef  : MatDialogRef<ModalUnidadmedidaComponent>,
                @Inject( MAT_DIALOG_DATA ) public data : any ) {
    this.operation = this.data.operation;
    this.create_form();
    this.loadForm();
  }

  ngOnInit(): void {}

  get nameInvalid(){
    return this.svalidato.control_invalid('name', this.form);
  }

  get ApodoInvalid(){
    return this.svalidato.control_invalid('apodo', this.form);
  }

  create_form(){

    const operation = this.operation.toUpperCase();
    this.form = this.fb.group({
      name : [ '', [ Validators.required, Validators.minLength(2) ] ],
      apodo: [ operation !== 'UNIDAD DE MEDIDA' ? '-' : '', [ Validators.required, Validators.minLength(1) ] ]
    });

  }

  loadForm(){

    const operation = this.operation.toUpperCase();
    this.form.patchValue({
      name  : this.data.name,
      apodo : operation !== 'UNIDAD DE MEDIDA' ? '-' : this.data.apodo
    });

  }

  Save(){

    if( this.form.invalid ){
      return this.svalidato.Empty_data(this.form);
    }

    if( this.form.value.name.trim() === '' ){
      this.form.patchValue({ name : '' })
      return this.svalidato.Empty_data(this.form);
    }

    if( this.form.value.apodo.trim() === '' ){
      this.form.patchValue({ apodo : '' })
      return this.svalidato.Empty_data(this.form);
    }

    this.spinner.show();
    const operation = this.operation.toUpperCase();
    switch( operation ){

      case 'UNIDAD DE MEDIDA':
        this.SaveUnidadMedida();
        break;
      case 'CATEGORIA':
        this.SaveCategoria();
        break;
      case 'SUB-CATEGORIA':
        this.SaveSubCategoria();
        break;

    }

  }

  SaveUnidadMedida(){

    const body = {
      idunidadmedida: String(this.data.id),
      unidadmedida: this.form.value.name,
      abreviatura: this.form.value.apodo,
      editar: Boolean(this.data.editar)
    };

    this.sproduc.SaveUnidadMedida( body ).subscribe( (response : any ) => {
      if( response.message === 'exito' ) this.dialogRef.close( { exito:true } );
      this.spinner.hide();
    }, ( error )=> this._exeception( error ) )

  }

  SaveCategoria(){

    const body = {
      idcategoria: String(this.data.id),
      categoria: this.form.value.name,
      editar: Boolean(this.data.editar)
    };

    this.sproduc.SaveCategoria( body ).subscribe( (response : any ) =>{
      if( response.message === 'exito' ) this.dialogRef.close( { exito:true } );
      this.spinner.hide();
    }, ( error ) => this._exeception( error ) );

  }

  SaveSubCategoria(){

    const body = {
      idsubcategoria: String(this.data.id),
      idcategoria: this.data.idCategoria,
      subcategoria: this.form.value.name,
      editar: Boolean(this.data.editar)
    };

    this.sproduc.SaveSubCategoria( body ).subscribe( (response : any ) =>{
      if( response.message === 'exito' ) this.dialogRef.close( { exito:true } );
      this.spinner.hide();
    }, ( error ) => this._exeception( error ) );

  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

}