import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { ModalNuevaunidmedidaComponent } from '../modal-nuevaunidmedida/modal-nuevaunidmedida.component';
import { ProductsService } from 'src/app/services/products.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductoserviciosComponent } from '../../productoservicios.component';
import { FunctionsService } from '../../../../../../services/functions.service';

@Component({
  selector: 'app-modal-unidadmedida',
  templateUrl: './modal-unidadmedida.component.html',
  styles: [
  ]
})
export class ModalUnidadmedidaComponent implements OnInit {

  success   : boolean;
  cnn_expi  : boolean;
  error     : boolean;
  message   : string;

  list      : any[] = [];
  listAlt   : any[] = []

  guardar: boolean = false;

  constructor ( public dialog     : MatDialog,
                private sProduct  : ProductsService,
                private spinner   : NgxSpinnerService,
                private sfunction : FunctionsService,
                public dialogRef  : MatDialogRef<ProductoserviciosComponent>,
                @Inject( MAT_DIALOG_DATA ) public data : any ) {
                  this._listGeneral();
                }

  ngOnInit(): void {}

  _listGeneral(){

    const operation = String(this.data.operation).toUpperCase();
    switch( operation ){
      case 'UNIDAD DE MEDIDA':
        this.listunidadMedida();
        break;
      case 'CATEGORIA':
        this.listCategoria();
        break;
      case 'SUB-CATEGORIA':
        this.listSubCategoria();
        break;
    }

  }

  listunidadMedida(){

    this.initialize();
    this.sProduct.listUnidadMedida().subscribe( response =>{
      
      response.forEach( el => {
        const body = {
          id    : el.Clave,
          name  : el.NombreUnidadMedida,
          apodo : el.Abreviatura
        };

        this.list.push( body );
      });
      this.listAlt = this.list;
      this.spinner.hide();

    }, (error) => this._exeception( error ) );

  }

  listCategoria(){
    this.initialize();

    this.sProduct.lisCategoria().subscribe( response => {
      response.forEach( el => {
        const body = {
          id    : el.IDCategoria,
          name  : el.NombreCategoria,
          apodo : ''
        };

        this.list.push( body );
      });
      this.listAlt = this.list;
      this.spinner.hide();

    }, (error) => this._exeception( error ) );

  }

  listSubCategoria(){

    this.initialize();
    this.sProduct.listSubCategoria().subscribe( response  => {
      response.filter( x=>x.IDCategoria === this.data.idCategoria ).forEach( el => {
        const body = {
          id    : el.IDSubcategoria,
          name  : el.NombreSubcategoria,
          apodo : ''
        };
        
        this.list.push( body );
      });
      this.listAlt = this.list;
      this.spinner.hide();
      
    }, ( error ) => this._exeception( error ) );

  }

  initialize() {
    this.spinner.show();
    this.list = [];
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _exeception( error:any ){
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  useToItem( name:string, id:number ){

    this.dialogRef.close( { name : name, id : id } )
  }

  irSaveOperation( name : string = null, apodo : string = null, id : string = "0", editar : boolean = false ) {

    this.dialog.open(ModalNuevaunidmedidaComponent, {

      data : {

        operation   : this.data.operation,
        idCategoria : this.data.idCategoria,
        name        : name,
        apodo       : apodo,
        id          : id,
        editar      : editar
        
      }

    }).afterClosed().subscribe( ( response )=>{

      if(response.exito){
        this._listGeneral()
      }
    });
  }

  _search( text:string ){

    this.list = this.listAlt;
    this.list = this.sfunction._search( this.list, 'name', text );
    
  }

}