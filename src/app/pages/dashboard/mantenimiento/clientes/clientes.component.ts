import { Component, ElementRef, Inject, OnInit, Optional, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ClientService } from 'src/app/services/client.service';
import { FunctionsService } from 'src/app/services/functions.service';
import { ValidatorsService } from 'src/app/services/validators.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { VentarapidaComponent } from '../../operacion/ventarapida/ventarapida.component';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html'
})
export class ClientesComponent implements OnInit {

  @ViewChild('correoEInput') correoEInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  delete: boolean;
  idClient: number;
  success: boolean;
  cnn_expi: boolean;
  error: boolean;
  message: string;
  newClient: boolean;

  columns = [];
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource([]);
  listClient: [] = [];
  listDocumentos: [] = [];

  visible = true;
  selectable = true;
  removable = true;
  emailInvalid:boolean;
  countndoc : number;

  separatorKeysCodes: number[] = [ENTER, COMMA];
  correoEremitente = new FormControl();
  correoE: string[] = [];
  dataTheDB : boolean;

  form: FormGroup;

  constructor(  public dialog: MatDialog,
                private sclient: ClientService,
                private spinner: NgxSpinnerService,
                private sfunction: FunctionsService,
                private fb: FormBuilder,
                private svalidator: ValidatorsService,
                @Optional() public dialogRef  : MatDialogRef<VentarapidaComponent>,
                @Optional() @Inject( MAT_DIALOG_DATA ) public data : any ) {
                  
    this.dataTheDB = data === null ? true : data.dataTheDB;
    this.countndoc = 0;
    this.create_form();
    this.list_Client('');
    this.listDocumento();
  }

  ngOnInit(): void { }

  nuevoclient(){
    this.countndoc = 0;
    this.newClient = !this.newClient;
    this._clear();
  }

  get tipoDocumentoInvalid() {
    return this.svalidator.control_invalid('idtipodocumento', this.form);
  }

  get nDocumentoInvalid() {
    return this.svalidator.control_invalid('numerodocumento', this.form);
  }

  get clienteInvalid() {
    return this.svalidator.control_invalid('cliente', this.form);
  }

  get direccionInvalid() {
    return this.svalidator.control_invalid('direccion', this.form);
  }

  get refDireccionInvalid() {
    return this.svalidator.control_invalid('referencia', this.form);
  }

  get telefonoInvalid() {
    return this.svalidator.control_invalid('telefono', this.form);
  }

  create_form() {
    this.form = this.fb.group({
      idcliente       : [ 0 ],
      cliente         : [ '', [ Validators.required, Validators.minLength(3) ] ],
      numerodocumento : [ '', [ Validators.required] ],
      telefono        : [ '' ],
      direccion       : [ '' ],
      referencia      : [ '' ],
      idtipodocumento : [ '' ],
      editar          : [ false ]
    });
  }

  listDocumento() {
    this.initialize();
    this.spinner.show();
    this.sclient.listDocumento().subscribe(response => {

      this.listDocumentos = response;
      this.spinner.hide();

    }, (error) => this._exeception(error))
  }

  list_Client( text:string ) {

    this.initializeTable();
    this.sclient.listClient().subscribe(response => {
      if (response.length > 0) {

        const columns = response ?? [];
        const keys = this.sfunction.KeyCabecera(columns[0]);
        this.columns = keys.columns;
        this.displayedColumns = keys.displayedColumns;
        this.dataSource = new MatTableDataSource(response);
        this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Nombrecompleto', text ) );
        this.listClient = response;
        this.dataSource.paginator = this.paginator;
        this.paginator._intl.itemsPerPageLabel = 'items por pagina';
      }

      this.spinner.hide();

    }, (error) => this._exeception(error))

  }

  SaveClient() {

    if (this.form.invalid) {
      return this.svalidator.Empty_data(this.form);
    }

    const ndocumento = String(this.form.value.numerodocumento).length;

    if( this.form.value.idtipodocumento === '2' && ndocumento !== 11  ){
      this.error = true;
      this.message = 'ruc invalido';
      return;
    }

    if( this.form.value.idtipodocumento === '1' && ndocumento !== 8  ){
      this.error = true;
      this.message = 'dni invalido';
      return;
    }

    let correos = [];
    this.initialize();
    this.spinner.show();

    this.correoE.forEach(( el, i ) =>{

      correos.push({

        idcorreo  : 0,
        idcliente : this.form.value.idcliente,
        correo    : el

      })

    });

    const body = {
      ... this.form.value,
      correos : correos
    };

    body.telefono = body.telefono === null ? '' : String(body.telefono);
    body.numerodocumento = String(body.numerodocumento);
    this.sclient.SaveClient( body ).subscribe( (response : any ) =>{

      if( response.message === 'exito' ){

        this.success = true;
        this._changeExito();
        this._clear();
        this.refresh();
        if( body.editar ) this.newClient = false;

      }

      this.spinner.hide();

    }, ( error ) => this._exeception( error ) );

  }

  deleteClient( $event ){
    
    this.initialize();
    this.spinner.show();
    if( !$event ) return;
    const body = {
      idcliente : this.idClient
    }

    this.sclient.deleteClient( body ).subscribe( (response : any) =>{

      if( response.message === 'exito' ){
        this.success = true;
        this.message = 'Registro eliminado con exito';
        this.refresh();
        this._changeExito();
      }

      this.spinner.hide();

    }, (error)=> this._exeception( error ) );

  }

  _clear(){

    this.form.reset({

      idcliente       : 0,
      cliente         : '',
      numerodocumento : '',
      telefono        : '',
      direccion       : '',
      referencia      : '',
      idtipodocumento : '',
      editar          : false

    });

    this.countndoc = 0;
    this.correoE = [];

  }

  _changeExito() {

    setTimeout(() => {
      this.success = false;
      this.message = null;
    }, 2000);

  }

  refresh() {
    let search = (<HTMLInputElement>document.getElementById('search')).value;

    this.sclient.listClient().subscribe( response => {
      if(response.length > 0){

        this.dataSource = new MatTableDataSource<Element>(response);
        this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Nombrecompleto', search ) );
        this.dataSource.paginator = this.paginator;
        this.listClient = response;

      }

    }, (error)=>{
      this._exeception( error )
    })
  }

  initializeTable() {
    this.spinner.show();
    this.dataSource = null;
    this.columns = [];
    this.displayedColumns = [];
    this.initialize();
  }

  initialize() {
    this.success = false;
    this.cnn_expi = false;
    this.error = false;
    this.message = null;
  }

  _exeception(error: any) {
    console.log(error);
    this.error = true;
    const cnn_expi = error.error === 'Unauthorized';
    this.cnn_expi = cnn_expi;
    this.message = cnn_expi ? 'conexion expirada, vuelva a iniciar sesion' : error.error.message ?? 'Sin conexion al servidor';
    this.spinner.hide();
  }

  enventOperation($event) {

    if ($event.titulo === 'Eliminar') {

      this.idClient = $event.id;
      this.message = '¿Está seguro de eliminar el Cliente?';
      this.delete = !this.delete;

    }
    else if ($event.titulo === 'Editar') {

      this.newClient = !this.newClient;
      this.idClient = $event.id;;
      this.listClient.filter((x: any) => x.ID === this.idClient).map((result: any) => {

        this.form.setValue({
          idcliente       : result.ID,
          cliente         : result.Nombrecompleto,
          numerodocumento : result.NumeroIdentificacion,
          telefono        : result.Telefono ?? '',
          direccion       : result.IDDireccion,
          referencia      : result.IDReferenciaDireccion,
          idtipodocumento : String(result.IDTipoDocumento),
          editar          : true
        });

        this.correoE = result.IDCorreos.map( x => x.Correo );
        this.countndoc = this.form.value.numerodocumento === null ? 0 : String( this.form.value.numerodocumento ).length;

      });
    }
  }

  // correo

  validate_email( $event ){
    let oldval = $event.target.value;
    const val = oldval.slice(0,$event.target.selectionStart) + $event.key + oldval.slice($event.target.selectionEnd);

    this.emailInvalid = false;
    const pattern = '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$';
    if( !val.match( pattern ) ) this.emailInvalid = true;
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    this.emailInvalid = false;
    const pattern = '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$';
    if( !value.match( pattern ) ){
      this.emailInvalid = true;
      return;
    }

    if ((value || '').trim()) this.correoE.push(value.trim());
    if (input) input.value = '';
    this.correoEremitente.setValue(null);
  }

  remove(correoE: string): void {
    
    const index = this.correoE.indexOf(correoE);
    if (index >= 0) {
      this.correoE.splice(index, 1);
    }
  }

  countNDocumento(){
    this.countndoc = this.form.value.numerodocumento === null ? 0 : String( this.form.value.numerodocumento ).length;
  }

  validateKey(event, limite ) {
  
    if( String(this.form.value.numerodocumento).length < 8 )this.form.patchValue({ idtipodocumento : '1' })
    else if( String(this.form.value.numerodocumento).length > 8 ) this.form.patchValue({ idtipodocumento : '2' })
    if(!this.svalidator.validateKey( event, limite ))return false;
    return true;

  }

  _search( text:string ){

    this.dataSource = new MatTableDataSource( this.listClient );
    this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'Nombrecompleto', text ) );

    if( this.dataSource.filteredData.length === 0 ){

      this.dataSource = new MatTableDataSource( this.listClient );
      this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'NumeroIdentificacion', text ) );

    }
    
    this.dataSource.paginator = this.paginator;
  }

  _filterSale( text:string ){

    this.dataSource = new MatTableDataSource( this.listClient );
    this.dataSource = new MatTableDataSource(  this.sfunction._search( this.dataSource.data, 'NumeroIdentificacion', text ) );
    this.dataSource.paginator = this.paginator;
  }

  _Agregar( $event ){
    if( $event ){
      
      const client = this.listClient.filter( ( x:any )=>x.ID === $event )[0];
      this.dialogRef.close( client )

    }

  }

  changeTipoDocumento(){

    const ndocumento = String(this.form.value.numerodocumento).length;
    if( ndocumento <= 8 )this.form.patchValue({ idtipodocumento : '1' })
    else if( ndocumento > 8 ) this.form.patchValue({ idtipodocumento : '2' })

  }

  addClientSale( cliente : string ){

    const validate = this.dataSource.data.length === 1;
    if( validate ){
      
      cliente = cliente.toUpperCase();
      const id = this.dataSource.data.filter( (x : any )=>x.Nombrecompleto.includes( cliente ) || x.NumeroIdentificacion.includes( cliente ) )[0].ID ?? 0
      this._Agregar( id );
    }
  }

}