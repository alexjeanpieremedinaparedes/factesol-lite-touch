import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ClientesComponent } from './mantenimiento/clientes/clientes.component';
import { ProductoserviciosComponent } from './mantenimiento/productoservicios/productoservicios.component';
import { NotacreditoComponent } from './operacion/notacredito/notacredito.component';
import { NotadebitoComponent } from './operacion/notadebito/notadebito.component';
import { VentanormalComponent } from './operacion/ventanormal/ventanormal.component';
import { VentarapidaComponent } from './operacion/ventarapida/ventarapida.component';
import { RepadministrativoComponent } from './reportes/repadministrativo/repadministrativo.component';
import { RepcontableComponent } from './reportes/repcontable/repcontable.component';
import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { InicioComponent } from './inicio/inicio.component';
import { VentanormaltouchComponent } from './operacion/ventanormaltouch/ventanormaltouch.component';



const routes: Routes = [
    { path: '', component: DashboardComponent,
        children: [
            // opetraciones
            { path: 'ventarapida', component: VentarapidaComponent, canActivate: [ AuthGuard ], data : { title : 'Venta Rápida' } },
            { path: 'ventanormal', component: VentanormalComponent, canActivate: [ AuthGuard ], data : { title : 'Venta Normal' } },
            { path: 'notacredito', component: NotacreditoComponent, canActivate: [ AuthGuard ], data : { title : 'Nota Crédito' } },
            { path: 'notadebito', component: NotadebitoComponent, canActivate: [ AuthGuard ], data : { title : 'Nota Débito' } },
            // mantenimiento
            { path: 'productoservicios', component: ProductoserviciosComponent, canActivate: [ AuthGuard ], data : { title : 'Productos/Servicios' } },
            { path: 'clientes', component: ClientesComponent, canActivate: [ AuthGuard ], data : { title : 'Clientes' } },
            // reportes
            { path: 'reportesadministrativos', component: RepadministrativoComponent, canActivate: [ AuthGuard ], data : { title : 'Reporte Admin' } },
            { path: 'reportecontable', component: RepcontableComponent, canActivate: [ AuthGuard ], data : { title : 'Reporte Contable' } },
            // ventanormal touch
            { path: 'ventanormaltouch', component: VentanormaltouchComponent},    


            {path:'bienvenido', component:InicioComponent},
            {path: '', redirectTo: '/inicio' , pathMatch: 'full'}
            
            
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class dashboardRoutingModule { }
