import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DemoMaterialModule } from 'src/app/material-module';

import { RepcontableComponent } from './reportes/repcontable/repcontable.component';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { OverlayModule } from '@angular/cdk/overlay';
import { MantenimientoModule } from './mantenimiento/mantenimiento.module';
import { OperacionModule } from './operacion/operacion.module';
import {
  ModalVerDetalle,
  RepadministrativoComponent,
} from './reportes/repadministrativo/repadministrativo.component';
import { VercomprobanteComponent } from './reportes/repadministrativo/vercomprobante/vercomprobante.component';
import { ModalbuscarComponent } from './reportes/repadministrativo/modalbuscar/modalbuscar.component';
import { ObtenerDocumentosMasivosComponent } from './reportes/repcontable/obtener-documentos-masivos/obtener-documentos-masivos.component';
import { InicioComponent } from './inicio/inicio.component';


@NgModule({
  declarations: [
    // reportes
    RepadministrativoComponent,
    ModalVerDetalle,
    RepcontableComponent,
    DashboardComponent,
    VercomprobanteComponent,
     ModalbuscarComponent,
     ObtenerDocumentosMasivosComponent,
     InicioComponent, 
   
    
  ],

  exports: [
    // reportes
    RepadministrativoComponent,
    ModalVerDetalle,
    RepcontableComponent,
    DashboardComponent,
  ],

  imports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    DemoMaterialModule,
    SharedModule,
    ComponentsModule,
    OverlayModule,
    MantenimientoModule,
    OperacionModule,
  ],
})
export class DashboardModule {}
