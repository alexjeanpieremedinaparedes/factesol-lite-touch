import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from 'src/app/auth/login/login.component';


const routes: Routes = [
  { path: 'inicio' , component: LoginComponent, data : { title : 'Factesolweb' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class inicioRoutingModule {}
