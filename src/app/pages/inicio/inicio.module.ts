import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DemoMaterialModule } from 'src/app/material-module';
import { LoginComponent} from 'src/app/auth/login/login.component';
import { NavbarComponent } from 'src/app/shared/navbar/navbar.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/components/components.module';


@NgModule({
  declarations: [
    LoginComponent,
    NavbarComponent],

  exports:[
    LoginComponent,
    NavbarComponent
  ],

  imports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    NgxSpinnerModule,
    DemoMaterialModule,
    ComponentsModule,
    SharedModule,
    ReactiveFormsModule
  ]

})
export class InicioModule { }
