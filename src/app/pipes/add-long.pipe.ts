import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addLong'
})
export class AddLongPipe implements PipeTransform {

  transform( text: string, long : number, relleno : string ): string {
    return this.pad( String(text), long, relleno );
  }

  private pad(input, length, padding) { 
    var str = input + "";
    return (length <= str.length) ? str : this.pad( padding + str, length, padding);
  }

}
