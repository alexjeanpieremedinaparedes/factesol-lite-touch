import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddLongPipe } from './add-long.pipe';
import { DecimalsPipe } from './decimals.pipe';
import { SearchPipe } from './search.pipe';
import { SafeUrlPipe } from './safe-url.pipe';



@NgModule({

  declarations: [
    AddLongPipe,
    DecimalsPipe,
    SearchPipe,
    SafeUrlPipe
  ],
  imports: [
    CommonModule
  ],
  exports : [
    AddLongPipe,
    DecimalsPipe,
    SearchPipe,
    SafeUrlPipe
  ]

})
export class PipesModule { }
