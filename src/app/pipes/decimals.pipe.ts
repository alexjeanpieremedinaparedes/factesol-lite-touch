import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimals'
})
export class DecimalsPipe implements PipeTransform {

  transform( value: number , decimals : number ): string {

    const number = this.redondear(value, decimals );
    return number;
  }

  redondear(numero, digitos){

    const base = Math.pow(10, digitos);
    const entero = Math.round(numero * base);
    return (entero / base).toFixed( digitos );
  }

}
