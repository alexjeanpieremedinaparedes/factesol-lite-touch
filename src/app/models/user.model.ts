export class mdUser{

    ruc         : string;
    usuario     : string;
    password    : string;    
}


export class mdUseradd {

    idusuario:     string;
    usuario:       string;
    password:      string;
    nombre:        string;
    apellido:      string;
    dni:           string;
    acceso:        boolean;
    admin:         boolean;
    reimprimir:    boolean;
    cambiarprecio: boolean;
    editar:        boolean;

}
