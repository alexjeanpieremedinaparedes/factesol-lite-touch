
export class mdClient {
    idcliente:       string;
    cliente:         string;
    numerodocumento: string;
    telefono:        string;
    direccion:       string;
    referencia:      string;
    idtipodocumento: number;
    editar:          boolean;
    correo:          Correo[];
}

export class Correo {

    idcorreo    :   string;
    idcliente   :   string;
    correo      :   string;

}