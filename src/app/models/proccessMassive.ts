export class IProcesoMasivo{
  
    init       : boolean;
    completed  : boolean;
    porcentual : number;
    i          : number;
    time       : string;
  
    anulados   : boolean;
    pdf        : boolean;
    xml        : boolean;
    cdr        : boolean;
    filedownload : any;
    cantTotalcpe : number;

    anio       : string;
    mes        : string;
  
    constructor(){
      
      this.init = false;
      this.completed = true;
      this.porcentual = 0;
      this.i = 0;
      this.cantTotalcpe = 0;
  
      this.anulados = false;
      this.pdf = false;
      this.xml = false;
      this.cdr = false;
      this.filedownload = null;

      this.anio = null;
      this.mes = null;
      this.time = '';
    }
  
  }