import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-headertitle',
  templateUrl: './headertitle.component.html',
  styleUrls: ['./headertitle.component.css']
})
export class HeadertitleComponent implements OnInit {

@Input() titleheader:string = null;
@Input() titleparrafo:string = null;



  constructor() { }

  ngOnInit(): void {
  }

}
