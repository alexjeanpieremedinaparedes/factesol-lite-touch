import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IProcesoMasivo } from 'src/app/models/proccessMassive';
import { ObtenerDocumentosMasivosComponent } from 'src/app/pages/dashboard/reportes/repcontable/obtener-documentos-masivos/obtener-documentos-masivos.component';
import { UpdatedatadowloandfilesService } from 'src/app/services/updatedatadowloandfiles.service';

@Component({
  selector: 'app--downloadmassive',
  templateUrl: './download-massive.component.html',
  styleUrls: ['./download-massive.component.css']
})
export class DownloadMassiveComponent implements OnInit {

  minimize : boolean;
  IProcess = new IProcesoMasivo();
  constructor(
    private sProcess: UpdatedatadowloandfilesService,
    private modal: MatDialog) { }

  ngOnInit(): void {
    this.sProcess.proccess$.subscribe(process => this.IProcess = process);
  }

  // obtener documntos  masivos
  obtenerDocumentMasivo(inprocess: boolean = false) {

    let data = {
      anio: this.IProcess.anio,
      mes: this.IProcess.mes,
      totalcpe: this.IProcess.cantTotalcpe,
      IProcess: this.IProcess,
      inprocess: inprocess
    };

    this.modal.open(ObtenerDocumentosMasivosComponent, {
      data: data,
      disableClose : true
    }).afterClosed().subscribe(result => {
      
      if (result.closeall) {
        this._resetProcessMassive();
      }
    })

  }

  _resetProcessMassive() {
    this.IProcess.init = false;
    this.IProcess.completed = true;
    this.IProcess.porcentual = 0;
    this.IProcess.i = 0;
    this.IProcess.cantTotalcpe = 0;
    this.IProcess.time = '';

    this.IProcess.anulados = true;
    this.IProcess.pdf = false;
    this.IProcess.xml = false;
    this.IProcess.cdr = false;
    this.IProcess.filedownload = null;

    this.IProcess.anio = null;
    this.IProcess.mes = null;
  }

}
