import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-modalsuccess',
  templateUrl: './modalsuccess.component.html',
  styles: [
  ]
})
export class ModalsuccessComponent implements OnInit {

@Input() success:boolean = false;  // cambios para modal principal
@Input() message:string = null; // cambio en el emnsaje del modal
@Input() rutaimg:string = null; // cambios en la  imagen del modal
@Output() Send  = new EventEmitter<boolean>();

 modalguardar(){
  this.success = !this.success;
  this.Send_emiter();
}

  constructor() { }
  ngOnInit(): void {
  }

  Send_emiter(){
    this.Send.emit( this.success);
  }

}
