import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mserror',
  templateUrl: './mserror.component.html'
})
export class MserrorComponent implements OnInit {

  @Input() error    : boolean;
  @Input() message  : string;
  constructor() { }

  ngOnInit(): void {
  }

}
