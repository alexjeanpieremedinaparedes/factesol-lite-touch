import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-modal-error',
  templateUrl: './modal-error.component.html',
  styleUrls: ['./modal-error.component.css']
})
export class ModalErrorComponent implements OnInit {

  @Input() cnn_expi: boolean;
  @Input() error   : boolean;
  @Input() message : string;
  @Output() send = new EventEmitter<boolean>();
  
  constructor( private auth:AuthService ) { }

  ngOnInit(): void {}

  sendEvent(){
    this.send.emit(this.error);
    if( this.cnn_expi ) this.auth.logout();
  }

}
