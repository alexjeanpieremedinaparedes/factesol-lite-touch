import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModaldeleteComponent } from './modaldelete/modaldelete.component';
import { ModalsuccessComponent } from './modalsuccess/modalsuccess.component';
import { HeadertitleComponent } from './headertitle/headertitle.component';
import { MserrorComponent } from './mserror/mserror.component';
import { CabecerafechaComponent } from './cabecerafecha/cabecerafecha.component';
import { DemoMaterialModule } from '../material-module';
import { ModalErrorComponent } from './modal-error/modal-error.component';
import { AlertComponent } from './alert/alert.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DownloadMassiveComponent } from './download-massive/download-massive.component';

@NgModule({
  declarations: [
    ModaldeleteComponent,
    ModalsuccessComponent,
    HeadertitleComponent,
    MserrorComponent,
    ModalErrorComponent,
    CabecerafechaComponent,
    AlertComponent,
    DownloadMassiveComponent

  ],
  exports:[
    ModaldeleteComponent,
    ModalsuccessComponent,
    HeadertitleComponent,
    MserrorComponent,
    ModalErrorComponent,
    CabecerafechaComponent,
    AlertComponent,
    DownloadMassiveComponent
  ],
  imports: [
    CommonModule,
    DemoMaterialModule,
    ReactiveFormsModule
  ]
})
export class ComponentsModule { }
