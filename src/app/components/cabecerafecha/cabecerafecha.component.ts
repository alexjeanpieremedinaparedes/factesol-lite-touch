import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FunctionsService } from 'src/app/services/functions.service';
import { ValidatorsService } from 'src/app/services/validators.service';

@Component({
  selector: 'app-cabecerafecha',
  templateUrl: './cabecerafecha.component.html',
  styleUrls: ['./cabecerafecha.component.css']
})
export class CabecerafechaComponent implements OnInit {

  minDate:any;
  datos:any = {};
  changedate : boolean;

  @Output() senDateStart = new EventEmitter<string>();

  form : FormGroup;
  constructor(  private fb : FormBuilder,
                private svalidator : ValidatorsService,
                private sfuntion    : FunctionsService ) {

    const configuration = JSON.parse(JSON.parse(localStorage.getItem('_token_login')).configuration);
    configuration.filter( x=>x.Operacion === 'cambiarfecha' ).map( data => {

      const valor = data.valor === 'SI' ? true : false;
      this.minDate = valor ? 0 : new Date();
      this.changedate = valor;
    })
    
    this.datos = fb.group({

      datestart : new FormControl( new Date(), { validators : Validators.required, updateOn : 'blur' } ),
      datefin   : new FormControl( new Date() , { validators : Validators.required, updateOn : 'blur' } )
    }, { updateOn : 'change'});

    this.datos.get('datestart').valueChanges.subscribe( data => {
      this.senDateStart.emit( data );
    });

  }

  ngOnInit(): void { this.sfuntion.startTime(); }
}
