export interface IPermissions{

    ventanormal         : boolean,
    cliente             : boolean,
    productoservicio    : boolean,
    usuario             : boolean,
    notadebito          : boolean,
    notacredito         : boolean,
    ventarapida         : boolean,
    repadministrativo   : boolean,
    repcontable         : boolean,
    impresion           : boolean,
    venta               : boolean,
    facturacionsunat    : boolean,

    //cabecera de los componentes\
    operacion           : boolean;
    mantenimiento       : boolean;
    reportes            : boolean;
  
}