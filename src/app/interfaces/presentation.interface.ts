
export interface IPresentation {
    ID              : number;
    Producto        : string;
    Presentacion    : string;
    Precio          : number;
    CodProducto     : number;
    VentaRap        : boolean;
    QuantitySale    : number;
    Abreviatura     : string;
    Afectacion      : string;
}
