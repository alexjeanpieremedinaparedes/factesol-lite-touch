import { ComponentFixture } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { LoginComponent } from './login.component';

const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
const spinnerSpy  = jasmine.createSpyObj('NgxSpinnerService', ['hide']);
const validatorServiceSpy = jasmine.createSpyObj('ValidatorsService', ['Empty_data']);
const functionsServicesSpy = jasmine.createSpyObj('FunctionsService', ['Empty_data']);
const authServiceSpy = jasmine.createSpyObj('AuthService', ['login'])

describe('Prueba aislada de componente inicio de sesion', () => {
  let component: LoginComponent;

  beforeEach(async () => {
    component = new LoginComponent( spinnerSpy, new FormBuilder(), validatorServiceSpy, authServiceSpy,routerSpy, functionsServicesSpy );
  });

  function updateForm( ruc : string , user : string, password : string ){
    component.form.controls['ruc'].setValue(ruc);
    component.form.controls['usuario'].setValue(user);
    component.form.controls['password'].setValue(password);
  }

  it('componente creado correctamente', ()=>{
    expect(component).toBeTruthy();
  });

  it('estado inicial de componente', ()=>{
    expect(component.form).toBeDefined();
    expect(component.error).toBeFalsy();
    expect(component.message).toBeUndefined();
  });

  it('formulario login correcto', ()=>{

    component.login();
    updateForm(validUser.ruc, validUser.usuario, validUser.password);
    expect(component.form.invalid).toBeFalsy();

  });

  it('formulario login invalido', ()=>{

    component.login();
    updateForm(blankUser.ruc, blankUser.usuario, blankUser.password);
    expect(component.form.invalid).toBeTruthy();
  });

  it('la ruta pricipal deber ser <</bienvenido>>', ()=>{

    component.navigateRute();
    const rutaPrincipal = routerSpy.navigateByUrl.calls.first().args[0];
    expect(rutaPrincipal).toBe('/bienvenido')

  });

  it('El valor del formulario debe actualizarse desde el momento en que cambia la entrada', ()=>{

    updateForm( validUser.ruc, validUser.usuario, validUser.password );
    expect(component.form.value).toEqual(validUser);
  });

});

describe('Prueba superficial del componente login', ()=>{

  let fixture : ComponentFixture<LoginComponent>;
})


export const validUser = {
  ruc: '20602026418',
  usuario: 'admin',
  password: 'prueba'
};

export const blankUser = {

  ruc: '',
  usuario: '',
  password: ''
};