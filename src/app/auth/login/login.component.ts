import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from 'src/app/services/auth.service';
import { FunctionsService } from 'src/app/services/functions.service';
import { ValidatorsService } from 'src/app/services/validators.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  form    : FormGroup;
  error   : boolean;
  message : string;

  constructor(  public spinner      : NgxSpinnerService,
                private fb          : FormBuilder,
                private svalidator  : ValidatorsService,
                private auth        : AuthService,
                private router      : Router,
                public  sfn         : FunctionsService
                 ) {

    this.create_form();
  }

  ngOnInit(): void {

    const userlogueado = localStorage.getItem('_token_login')
    if( userlogueado )this.navigateRute();

  }

  get rucNovalido(){
    return this.svalidator.control_invalid( "ruc", this.form );
  }

  get userNovalido(){
    return this.svalidator.control_invalid("usuario", this.form);
  }

  get passNovadio(){
    return this.svalidator.control_invalid("password", this.form);
  }

  create_form(){

    this.form = this.fb.group({
      ruc       : [ '', Validators.required ],
      usuario   : [ '', Validators.required ],
      password  : [ '', Validators.required ]
    });

  }

  public dropdownHandler(element) {
    let single = element.getElementsByTagName("ul")[0];
    single.classList.toggle("hidden");
  }

  login(){

    if( this.form.invalid ){
      return this.svalidator.Empty_data(this.form);
    }

    this.initialize();
    const body = {
      ... this.form.value
    };

    this.auth.login( body ).subscribe( (response : any ) =>{

      
      if( response.message === "exito" ){

        const result = response.result;
        const user = result.usuario;
        const otros = result.otros;

        const list = { 
          token           : result.token,
          user            : user.Usuario,
          Nombre          : user.NombreTrabajador,
          Apellidos       : user.ApellidoTrabajador,
          dni             : user.DNI,
          ruc             : body.ruc,
          direccion       : otros.direccion,
          direccionanexo  : otros.direccionanexo,
          telefonoempresa : otros.telefonoempresa,
          correoempresa   : otros.correoempresa,
          empresa         : otros.razonsocial,
          logo            : otros.logo,
          datecertificado : otros.fechacertificado,
          numeroCuenta    : otros.NumeroCuenta ?? '',
          padmin          : user.EsAdmin,
          preimprimir     : user.Reimprimir,
          changePrecio    : user.CambiarPrecio,
          clave           : user.Clave,
          permissions     : JSON.stringify(result.permisos),
          configuration   : JSON.stringify(result.configuracion)
        };

        localStorage.setItem('_token_login', JSON.stringify(list))
        this.navigateRute();
        this.spinner.hide();

      }
      else this.auth.logout();

    }, ( error )=>{
      this.auth.logout();
      this.error = true;
      this.message = error.error.message ?? "Sin conexion al servidor";
      this.spinner.hide();
    });
  }


  validateruc( $event:any, limite:number ){
    
    let inputValue = (document.getElementById('rucSelect') as HTMLInputElement).value;
    const select = document.getSelection().toString() === inputValue;
    if( select ){
      (document.getElementById('rucSelect') as HTMLInputElement).value = '';
    }
    return this.svalidator.validateKey($event, limite);

  }

  navigateRute(){
    this.router.navigateByUrl('/bienvenido')
  }

  initialize(){
    this.spinner.show();
    this.error = false;
    this.message = null;
  }

}