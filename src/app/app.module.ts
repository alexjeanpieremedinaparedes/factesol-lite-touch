import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { DemoMaterialModule } from './material-module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule, registerLocaleData } from '@angular/common';
import LocaleEsPe from '@angular/common/locales/es-PE.js';


import { ConfiguracionesModule } from './pages/configuraciones/configuraciones.module'; // modulo de  configuraciones
import { DashboardModule } from './pages/dashboard/dashboard.module'; // modulo de dasboard /operaciones, mantenimiento y reportes
import { InicioModule } from './pages/inicio/inicio.module'; // modulo de inicio
import { BuscadorComponent } from './components/buscador/buscador.component';
import { NopagesfoundComponent } from './nopagesfound/nopagesfound.component'; //pagina no encontrada
import { MAT_DATE_LOCALE } from '@angular/material/core'; // Calendario En español
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ComponentsModule } from './components/components.module';

registerLocaleData(LocaleEsPe);

@NgModule({
  declarations: [
    AppComponent,
    BuscadorComponent,
    NopagesfoundComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    NgxSpinnerModule,
    // modulos de auth

    // modulo de Configuraciones
    ConfiguracionesModule,
    // &&modulos de sashboard
    DashboardModule,
    // modulo de  inicio
    InicioModule,

    SharedModule,
    HttpClientModule,
    ComponentsModule

  ],

  providers: [
    { 
      provide: MAT_DATE_LOCALE,
      useValue: 'es-PE'
    },
    {
      provide: LOCALE_ID,
      useValue: 'es-PE'
    }
  ], // modulo nesesario para cambiar el idioma detucalendario
  bootstrap: [AppComponent]
})

export class AppModule { }
