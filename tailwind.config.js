const newLocal = require('@tailwindcss/custom-forms');
module.exports = {
    purge: ['./src/**/*.{html,ts}'],
    darkMode: false, // or 'media' or 'class'
    theme: {

        fontSize: {
            'xs': '.75rem',
            'al': '.820rem',
            'sm': '.875rem',
            'tiny': '.875rem',
            'base': '1rem',
            'lg': '1.125rem',
            'xl': '1.25rem',
            '2xl': '1.5rem',
            '3xl': '1.875rem',
            '4xl': '2.25rem',
            '5xl': '3rem',
            '6xl': '4rem',
            '7xl': '5rem',
        },


        extend: {

            fontWeight: {
                hairline: 100,
                'extra-light': 100,
                thin: 200,
                light: 300,
                normal: 400,
                medium: 500,
                grande: 580,
                semibold: 600,
                masgrande: 605,
                bold: 700,
                extrabold: 800,
                'extra-bold': 800,
                black: 900,
            },

            width: {
                '1/7': '14.2857143%',
                '2/7': '28.5714286%',
                '3/7': '42.8571429%',
                '4/7': '57.1428571%',
                '5/7': '71.4285714%',
                '6/7': '85.7142857%',
                '49':  '11rem',
                '51':  '12rem',
                '40':  '17.5rem',
                '49':  '12rem',
                '82':  '29rem',
                '97':  '21rem',
                '98':  '29rem',
                '99':  '34.875rem',
                '99.4':'40.625',
                '100': '41.813rem',
                '110': '42rem',
                '120': '45rem',
                '130': '48rem',
                '140': '50rem'
              },


              height: {
                ce:  '500px',
                mp:  '600px',
                sm:  '8px',
                md:  '16px',
                lg:  '24px',
                xl:  '48px',
              '49':  '11rem',
              '51':  '12rem',
              '53':  '13.6rem',
              '62':  '15.2rem',
              '82':  '29rem',
              '83':  '23rem',
              '84':  '35.5rem',
              '85':  '38rem',
              '88':  '40.5rem',
              '90':  '42rem',
              '93':  '49.949rem',
              '95':  '50rem',
              '97':  '33rem',
              '98':  '54rem',
              '100': '60rem',
              '110': '75rem'
              
             },
    


            colors: {
                'facted': {
                    '50': '#fef4f4',
                    '100': '#fde9e9',
                    '200': '#f9c8c8',
                    '300': '#f6a7a7',
                    '400': '#ef6666',
                    '500': '#e82424',
                    '600': '#d12020',
                    '700': '#ae1b1b',
                    '800': '#8b1616',
                    '900': '#721212',
                    '1000': '#F5F5F5',
                    '3000': '#33985A',
                    '4000': '#036A96',
                },

                'azul':{
                    '800':'#263238',
                    '850':'#263238'
                },

                'rojo':{
                    '500':'#EA3E3E',
                },

                'plomo':{
                    '100':'#FAFAFA',
                    '300':'#969696',
                    '320':'#CFCFCF',
                    '350':'#ADAFAF',
                    '400':'#E3E3E3',
                    '405':'#7D7D7D',
                    '410':'#606772',
                    '420':'#7C7C7C',
                    '430':'#E6E6E6',
                    '435':'#5A5A5A',
                    '440':'#F2F2F2',
                    '450':'#7E818B',
                    '470':'#ABABAB',
                    '480':'#CECECE',
                    '500':'#DADADA',
                    '800':'#2F3447',
                    '900':'#7E8389'
                },

                'shark': {
                    '50': '#f4f4f4',
                    '100': '#e9e9e9',
                    '200': '#c8c8c8',
                    '300': '#a7a7a7',
                    '400': '#646464',
                    '500': '#242424',
                    '600': '#202020',
                    '700': '#1b1b1b',
                    '720': '#828282',
                    '800': '#161616',
                    '900': '#121212'
                },

                'oscuro':{
                    '500':'#525E64',
                    '800':'#2F3447',
                    '900':'#4A4A4A'
                },
               

            },
            fontFamily: {
                Roboto: ['Roboto'],
                Poppins: ['Poppins'],
                Manrope: ['Manrope']
              }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [require("@tailwindcss/custom-forms"),
        newLocal
    ],
};